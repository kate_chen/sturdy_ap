/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
//#define USE_HSI    // 20190830 十方_kate 新增   // false:外部8M震盪器    true:內部震盪器

//unmark define to enable prescan commands.
//#define USE_PRESCAN 	

#define AC_START_Pin GPIO_PIN_2
#define AC_START_GPIO_Port GPIOE
#define MCU_ACO_01_Pin GPIO_PIN_3
#define MCU_ACO_01_GPIO_Port GPIOE
#define MCU_ACO_02_Pin GPIO_PIN_4
#define MCU_ACO_02_GPIO_Port GPIOE
#define MCU_ACO_03_Pin GPIO_PIN_5
#define MCU_ACO_03_GPIO_Port GPIOE
#define MCU_ACO_04_Pin GPIO_PIN_6
#define MCU_ACO_04_GPIO_Port GPIOE
#define WATER_QUAILITY_Pin GPIO_PIN_0     // 20190916 十方新增
#define WATER_QUAILITY_GPIO_Port GPIOA    // 20190916 十方新增
#define PT100_NSS_Pin GPIO_PIN_4
#define PT100_NSS_GPIO_Port GPIOA
#define PT100_SCK_Pin GPIO_PIN_5
#define PT100_SCK_GPIO_Port GPIOA
#define PT100_MISO_Pin GPIO_PIN_6
#define PT100_MISO_GPIO_Port GPIOA
#define PT100_MOSI_Pin GPIO_PIN_7
#define PT100_MOSI_GPIO_Port GPIOA
#define WATER_LEVEL_DE1_Pin GPIO_PIN_2
#define WATER_LEVEL_DE1_GPIO_Port GPIOB
#define WATER_LEVEL_DE2_Pin GPIO_PIN_9
#define WATER_LEVEL_DE2_GPIO_Port GPIOE
#define WATER_LEVEL_DE3_Pin GPIO_PIN_12
#define WATER_LEVEL_DE3_GPIO_Port GPIOE
#define EEPROM_SCL_Pin GPIO_PIN_10
#define EEPROM_SCL_GPIO_Port GPIOB
#define EEPROM_SDA_Pin GPIO_PIN_11
#define EEPROM_SDA_GPIO_Port GPIOB
#define KTYPE_NSS_Pin GPIO_PIN_12
#define KTYPE_NSS_GPIO_Port GPIOB
#define KTYPE_SCK_Pin GPIO_PIN_13
#define KTYPE_SCK_GPIO_Port GPIOB
#define KTYPE_MISO_Pin GPIO_PIN_14
#define KTYPE_MISO_GPIO_Port GPIOB
#define KTYPE_MOSI_Pin GPIO_PIN_15
#define KTYPE_MOSI_GPIO_Port GPIOB
#define MCU_DO_01_Pin GPIO_PIN_10
#define MCU_DO_01_GPIO_Port GPIOD
#define MCU_DO_02_Pin GPIO_PIN_11
#define MCU_DO_02_GPIO_Port GPIOD
#define MCU_DO_03_Pin GPIO_PIN_12
#define MCU_DO_03_GPIO_Port GPIOD
#define MCU_DO_04_Pin GPIO_PIN_13
#define MCU_DO_04_GPIO_Port GPIOD
#define MCU_DO_05_Pin GPIO_PIN_14
#define MCU_DO_05_GPIO_Port GPIOD
#define MCU_DO_06_Pin GPIO_PIN_15
#define MCU_DO_06_GPIO_Port GPIOD
#define MCU_DO_07_Pin GPIO_PIN_6
#define MCU_DO_07_GPIO_Port GPIOC
#define MCU_DO_08_Pin GPIO_PIN_7
#define MCU_DO_08_GPIO_Port GPIOC
#define MCU_DO_16_Pin GPIO_PIN_8
#define MCU_DO_16_GPIO_Port GPIOC
#define MCU_DO_17_Pin GPIO_PIN_9
#define MCU_DO_17_GPIO_Port GPIOC
#define RS232_TX_Pin GPIO_PIN_9
#define RS232_TX_GPIO_Port GPIOA
#define RS232_RX_Pin GPIO_PIN_10
#define RS232_RX_GPIO_Port GPIOA
#define RS232_CTS_Pin GPIO_PIN_11
#define RS232_CTS_GPIO_Port GPIOA
#define RS232_RTS_Pin GPIO_PIN_12
#define RS232_RTS_GPIO_Port GPIOA
#define MCU_DO_18_Pin GPIO_PIN_10
#define MCU_DO_18_GPIO_Port GPIOC
#define MCU_DO_19_Pin GPIO_PIN_12
#define MCU_DO_19_GPIO_Port GPIOC
#define MCU_DO_20_Pin GPIO_PIN_1
#define MCU_DO_20_GPIO_Port GPIOD
#define DEBUG_TX_Pin GPIO_PIN_5
#define DEBUG_TX_GPIO_Port GPIOD
#define DEBUG_RX_Pin GPIO_PIN_6
#define DEBUG_RX_GPIO_Port GPIOD

#define MCU_DI_01_Pin GPIO_PIN_0
#define MCU_DI_01_GPIO_Port GPIOB
#define MCU_DI_02_Pin GPIO_PIN_1
#define MCU_DI_02_GPIO_Port GPIOB
#define MCU_DI_03_Pin GPIO_PIN_7
#define MCU_DI_03_GPIO_Port GPIOE
#define MCU_DI_04_Pin GPIO_PIN_8
#define MCU_DI_04_GPIO_Port GPIOE
#define MCU_DI_06_Pin GPIO_PIN_10
#define MCU_DI_06_GPIO_Port GPIOE
#define MCU_DI_07_Pin GPIO_PIN_11
#define MCU_DI_07_GPIO_Port GPIOE
#define MCU_DI_08_Pin GPIO_PIN_13
#define MCU_DI_08_GPIO_Port GPIOE
#define MCU_ACI_01_Pin GPIO_PIN_9
#define MCU_ACI_01_GPIO_Port GPIOB
#define MCU_ACI_02_Pin GPIO_PIN_0
#define MCU_ACI_02_GPIO_Port GPIOE
#define BUZZER_CONTROL_Pin GPIO_PIN_1
#define BUZZER_CONTROL_GPIO_Port GPIOE
#define PRESSURE_I2C_OE_Pin GPIO_PIN_5
#define PRESSURE_I2C_OE_GPIO_Port GPIOB
#define SENSOR_SCL_Pin GPIO_PIN_6
#define SENSOR_SCL_GPIO_Port GPIOB
#define SENSOR_SDA_Pin GPIO_PIN_7
#define SENSOR_SDA_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */




/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
