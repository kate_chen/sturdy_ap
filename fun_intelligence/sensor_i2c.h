

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SENSOR_I2C_H
#define __SENSOR_I2C_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup SENSOR_I2C_Exported_Functions
  * @{
  */

/** @addtogroup SENSOR_I2C_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
void SENSOR_I2C_Init(void);
/**
  * @}
  */

/** @addtogroup SENSOR_I2C_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
HAL_StatusTypeDef SENSOR_I2C_check_ready(uint8_t DevAddress, uint32_t Trials, uint32_t Timeout);
HAL_StatusTypeDef SENSOR_I2C_mem_write(uint8_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size);
HAL_StatusTypeDef SENSOR_I2C_no_check_mem_write(uint8_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size);
HAL_StatusTypeDef SENSOR_I2C_mem_read(uint8_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size);
HAL_StatusTypeDef SENSOR_I2C_write(uint8_t DevAddress, uint8_t *pData, uint16_t Size);
HAL_StatusTypeDef SENSOR_I2C_read(uint8_t DevAddress, uint8_t *pData, uint16_t Size);
/**
  * @}
  */

/** @addtogroup SENSOR_I2C_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __SENSOR_I2C_H */



