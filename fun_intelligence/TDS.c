

/* Includes ------------------------------------------------------------------*/
#include "TDS.h"
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc;
TIM_HandleTypeDef htim2;

/* Variable used to get converted counter */
__IO uint8_t TDSConvCount = 0;
/* Variable used to get converted value */
uint16_t TDSConvBuff[TDS_CONVERTED_BUFFER_SIZE];
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/
/**
  * @brief TDS ADC Initialization Function
  * @param None
  * @retval None
  */
static void TDS_ADC_Init(void)
{

  /* USER CODE BEGIN ADC_Init 0 */

  /* USER CODE END ADC_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC_Init 1 */

  /* USER CODE END ADC_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  hadc.Init.ContinuousConvMode = DISABLE;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T2_TRGO;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
  hadc.Init.DMAContinuousRequests = DISABLE;
  hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }
  /* ###  Start calibration ############################################ */
  if (HAL_ADCEx_Calibration_Start(&hadc) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure for the selected ADC regular channel to be converted. 
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  sConfig.SamplingTime = ADC_SAMPLETIME_28CYCLES_5;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC_Init 2 */

  /* USER CODE END ADC_Init 2 */

}

/**
  * @brief TDS TIM Initialization Function - trigger every 10 ms
  * @param None
  * @retval None
  */
static void TDS_TIM_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_SlaveConfigTypeDef sSlaveConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 1599;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 99;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sSlaveConfig.SlaveMode = TIM_SLAVEMODE_DISABLE;
  sSlaveConfig.InputTrigger = TIM_TS_ITR0;
  if (HAL_TIM_SlaveConfigSynchronization(&htim2, &sSlaveConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TDS ADC Initialization Function
  * @param None
  * @retval None
  */
void TDS_Init(void)
{
	TDS_ADC_Init();
	TDS_TIM_Init();

}

HAL_StatusTypeDef TDS_ConvStart(void)
{
	if(TDSConvCount)
	{
    return HAL_BUSY;
  }
	
  /*##-- Start the conversion process and enable interrupt ##################*/
  if (HAL_ADC_Start_IT(&hadc) != HAL_OK)
  {
    /* Start Conversation Error */
    return HAL_ERROR;
  }

  /*##-- TIM counter enable ################################################*/
  if (HAL_TIM_Base_Start(&htim2) != HAL_OK)
  {
    /* Counter Enable Error */
    return HAL_ERROR;
  }
  
  TDSConvCount = 0;
  memset((uint16_t *)TDSConvBuff,0,sizeof(TDSConvBuff));
  return HAL_OK;
}

float getTDSValue(void)
{
	float averageVoltage = 0,tdsValue = 0,temperature = 25;
	uint32_t tmp = 0;
	__IO uint8_t count =0;

	for(count =0;count <TDS_CONVERTED_BUFFER_SIZE;count++)
	{
		//printf("[%d]",TDSConvBuff[count]);
		tmp += TDSConvBuff[count];
	}

	// read the analog value more stable by the median filtering algorithm, and convert to voltage value
	averageVoltage = (tmp /TDS_CONVERTED_BUFFER_SIZE)  * (float)TDS_VREF / 1024.0; 
	
	//temperature compensation formula: 
	//fFinalResult(25^C) = fFinalResult(current)/(1.0+0.02*(fTP-25.0));
	float compensationCoefficient=1.0+0.02*(temperature-25.0); 
	
	//temperature compensation
	float compensationVolatge=averageVoltage/compensationCoefficient; 
	
	//convert voltage value to tds value
	#ifdef TDS_UNIT_US_CM
	tdsValue=(133.42*compensationVolatge*compensationVolatge*compensationVolatge - 
		255.86*compensationVolatge*compensationVolatge + 857.39*compensationVolatge); 

	//printf("\nAvg:%d-%.2f",tmp,averageVoltage);
	//printf("\n%.2f uS/cm\n",tdsValue);
	#else
	tdsValue=(133.42*compensationVolatge*compensationVolatge*compensationVolatge - 
		255.86*compensationVolatge*compensationVolatge + 857.39*compensationVolatge)*0.5; 

	//printf("\nAvg:%d-%.2f",tmp,averageVoltage);
	//printf("\n%.2f ppm\n",tdsValue);
	#endif
	return tdsValue;
}


/**
  * @brief  Conversion complete callback in non blocking mode 
  * @param pData Pointer to data buffer.
  * @retval None
  */
__weak void TDS_ConvCpltCallback(uint16_t* pData)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(pData);

  /* NOTE : This function should not be modified. When the callback is needed,
            function HAL_ADC_ConvCpltCallback must be implemented in the user file.
   */
}

/**
  * @brief  Conversion complete callback in non blocking mode
  * @param  AdcHandle : AdcHandle handle
  * @note   This example shows a simple way to report end of conversion, and
  *         you can add your own implementation.
  * @retval None
  */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *AdcHandle)
{
  
  if(TDSConvCount < TDS_CONVERTED_BUFFER_SIZE)
 	{
	  /* Get the converted value of regular channel */
	  TDSConvBuff[TDSConvCount++] = HAL_ADC_GetValue(AdcHandle);
 	}
  
  if(TDSConvCount >= TDS_CONVERTED_BUFFER_SIZE)
 	{
	  HAL_TIM_Base_Stop(&htim2);
	  HAL_ADC_Stop_IT(&hadc);
	  
	  TDS_ConvCpltCallback(TDSConvBuff);
 	}
}


