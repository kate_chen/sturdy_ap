

/* Includes ------------------------------------------------------------------*/
#include "sensor.h"
#include "main.h"
#include "TDS.h" 
#include "pt100.h"
#include "ktype.h"
#include "sensor_i2c.h"
#include "AT24CM02.h"
#include "SM1131.h"
#include "TMP75.h"
#include "SSCDANT100PA2A5.h"
#include "uart_hmi.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
#define UPDATE_NONE 			0x00000000
#define UPDATE_CJ1					 0x00000001
#define UPDATE_K1					 0x00000002
#define UPDATE_CJ2 			0x00000004
#define UPDATE_K2 			0x00000008
#define UPDATE_CJ3					 0x00000010
#define UPDATE_K3					 0x00000020
#define UPDATE_CJ4 			0x00000040
#define UPDATE_K4 			0x00000080
#define UPDATE_LAST_KTYPE 			0x00000100
#define UPDATE_MASK_KTYPE 			0x000000FF

#define UPDATE_PT1 			0x00000200
#define UPDATE_PT2 			0x00000400
#define UPDATE_PT3 			0x00000800
#define UPDATE_WQ					 0x00001000
#define UPDATE_AIR					 0x00002000
#define UPDATE_PRESSURE					 0x00004000
#define UPDATE_TDS					 0x00008000

#define UPDATE_ENABLE 	0x80000000
#define UPDATE_MASK_ITEM 			0x7FFFFFFF

#define UPDATE_DEFAULT_LIST 			(UPDATE_PT1 | UPDATE_PT2 | UPDATE_PT3 | UPDATE_K1 | UPDATE_K2 | UPDATE_K3 | UPDATE_K4 | UPDATE_AIR | UPDATE_PRESSURE |UPDATE_TDS)

#define UPDATE_CNT_MAX 			99

/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private constants ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim_sensor;

__IO uint32_t uwUpdateItem = UPDATE_NONE;    
__IO uint8_t hUpdateCounter = 0;    

uint32_t uwUpdateList = UPDATE_DEFAULT_LIST;    

//Updated sensor data.
int PT1Temperature = 0;
int PT2Temperature = 0;
int PT3Temperature = 0;
int K1Temperature = 0;
int K2Temperature = 0;
int K3Temperature = 0;
int K4Temperature = 0;
int P1Pressure = 0;
int P2Pressure = 0;
int WaterQuality = 0;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern __IO uint8_t TDSConvCount;     
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/
/**
  * @brief TIM16 Initialization Function
  * @param None
  * @retval None
  */
static void SENSOR_TIM_Init(void)
{

  /* USER CODE BEGIN TIM16_Init 0 */

  /* USER CODE END TIM16_Init 0 */

  /* USER CODE BEGIN TIM16_Init 1 */

  /* USER CODE END TIM16_Init 1 */
  htim_sensor.Instance = TIM16;
  htim_sensor.Init.Prescaler = 1599;
  htim_sensor.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim_sensor.Init.Period = 99;
  htim_sensor.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim_sensor.Init.RepetitionCounter = 0;
  htim_sensor.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim_sensor) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM16_Init 2 */

  /* USER CODE END TIM16_Init 2 */

}



/**
  * @brief SENSOR Initialization Function
  * @param None
  * @retval None
  */
void SENSOR_Init(void)
{
	//initial sensor
	TDS_Init();
  PT100_Init();
	KTYPE_Init();
	AT24CM02_Init();
	SENSOR_I2C_Init();
	if(TMP75_Init() != HAL_OK)
		  printf("\nTMP75 ERR\n");
	if(SSCDANT100PA2A5_Init() != HAL_OK)
		  printf("\nSSCDANT100PA2A5 ERR\n");
	if(SM1131_Init() != HAL_OK)
		  printf("\nSM1131 ERR\n");

	printf("int=%d,u=%d\n",sizeof(int),sizeof(unsigned));

	SENSOR_TIM_Init();

 if (HAL_TIM_Base_Start_IT(&htim_sensor) != HAL_OK)       
 {
   /* Starting Error */
   Error_Handler();
 }
}

/**
  * @brief SENSOR update Function
  * @param None
  * @retval None
  */
void SENSOR_update(void)
{
	if(uwUpdateItem & UPDATE_ENABLE)
	{
		uwUpdateItem &= UPDATE_MASK_ITEM;

		if(uwUpdateItem & UPDATE_CJ1)
		{
		  printf("\n[GET CJ1]");
			uwUpdateItem &= (~UPDATE_CJ1);
			return;
		}
		
		if(uwUpdateItem & UPDATE_K1)
		{
		  if(KTYPE_check_k1() == HAL_OK)
	  	{
				int16_t data;
				HAL_StatusTypeDef ret = KTYPE_get_temp(&data);
				
				switch(ret)
				{
					case HAL_OK:
						uwUpdateItem &= (~UPDATE_K1);
						K1Temperature = data;
						printf("[K1=%d]\n",K1Temperature);
						break;
					case HAL_TIMEOUT:
						uwUpdateItem &= (~UPDATE_K1);
						printf("[K1 NC]\n");
						break;
					default:
					  printf("[K1]");
						break;
				}
	  	}
		  else
	  	{
	  		if(KTYPE_config_k1() == HAL_OK)
  			{
  				KTYPE_start_convertion();
				  printf("[set K1]\n");
  			}
	  		else
				  printf("[ERR K1]\n");
	  	}
			return;
		}
		
		if(uwUpdateItem & UPDATE_CJ2)
		{
		  printf("\n[GET CJ2]");
			uwUpdateItem &= (~UPDATE_CJ2);
			return;
		}
		
		if(uwUpdateItem & UPDATE_K2)
		{
		  if(KTYPE_check_k2() == HAL_OK)
	  	{
				int16_t data;
				HAL_StatusTypeDef ret = KTYPE_get_temp(&data);
				
				switch(ret)
				{
					case HAL_OK:
						uwUpdateItem &= (~UPDATE_K2);
						K2Temperature = data;
						printf("[K2=%d]\n",K2Temperature);
						break;
					case HAL_TIMEOUT:
						uwUpdateItem &= (~UPDATE_K2);
						printf("[K2 NC]\n");
						break;
					default:
					  printf("[K2]");
						break;
				}
	  	}
		  else
	  	{
	  		if(KTYPE_config_k2() == HAL_OK)
  			{
  				KTYPE_start_convertion();
				  printf("[set K2]\n");
  			}
	  		else
				  printf("[ERR K2]\n");
	  	}
			return;
		}
		
		if(uwUpdateItem & UPDATE_CJ3)
		{
		  printf("\n[GET CJ3]");
			uwUpdateItem &= (~UPDATE_CJ3);
			return;
		}
		
		if(uwUpdateItem & UPDATE_K3)
		{
		  if(KTYPE_check_k3() == HAL_OK)
	  	{
				int16_t data;
				HAL_StatusTypeDef ret = KTYPE_get_temp(&data);
				
				switch(ret)
				{
					case HAL_OK:
						uwUpdateItem &= (~UPDATE_K3);
						K3Temperature = data;
						printf("[K3=%d]\n",K3Temperature);
						break;
					case HAL_TIMEOUT:
						uwUpdateItem &= (~UPDATE_K3);
						printf("[K3 NC]\n");
						break;
					default:
					  printf("[K3]");
						break;
				}
	  	}
		  else
	  	{
	  		if(KTYPE_config_k3() == HAL_OK)
  			{
  				KTYPE_start_convertion();
				  printf("[set K3]\n");
  			}
	  		else
				  printf("[ERR K3]\n");
	  	}
			return;
		}
		
		if(uwUpdateItem & UPDATE_CJ4)
		{
		  printf("\n[GET CJ4]");
			uwUpdateItem &= (~UPDATE_CJ4);
			return;
		}
		
		if(uwUpdateItem & UPDATE_K4)
		{
		  if(KTYPE_check_k4() == HAL_OK)
	  	{
				int16_t data;
				HAL_StatusTypeDef ret = KTYPE_get_temp(&data);
				
				switch(ret)
				{
					case HAL_OK:
						uwUpdateItem &= (~UPDATE_K4);
						K4Temperature = data;
						printf("[K4=%d]\n",K4Temperature);
						break;
					case HAL_TIMEOUT:
						uwUpdateItem &= (~UPDATE_K4);
						printf("[K4 NC]\n");
						break;
					default:
					  printf("[K4]");
						break;
				}
	  	}
		  else
	  	{
	  		if(KTYPE_config_k4() == HAL_OK)
  			{
  				KTYPE_start_convertion();
				  printf("[set K4]\n");
  			}
	  		else
				  printf("[ERR K4]\n");
	  	}
			return;
		}
		
		if(uwUpdateItem & UPDATE_LAST_KTYPE)
		{
		  printf("[GET LastKtype]");
			uwUpdateItem &= (~UPDATE_LAST_KTYPE);
			return;
		}
		
		if(uwUpdateItem & UPDATE_PT1)
		{
		  if(PT100_check_pt1() == HAL_OK)
	  	{
				int16_t data;
				
			  if(PT100_get_temp(&data) == HAL_OK)
		  	{
					uwUpdateItem &= (~UPDATE_PT1);
					PT1Temperature = data;
					printf("[PT1=%d]\n",PT1Temperature);
		  	}
			  else
				  printf("[PT1]\n");
	  	}
		  else
	  	{
	  		if(PT100_config_pt1() == HAL_OK)
  			{
  				PT100_start_convertion();
				  printf("[set PT1]\n");
  			}
	  		else
				  printf("[ERR PT1]\n");
	  	}
			return;
		}
		
		if(uwUpdateItem & UPDATE_PT2)
		{
		  if(PT100_check_pt2() == HAL_OK)
	  	{
				int16_t data;
				
			  if(PT100_get_temp(&data) == HAL_OK)
		  	{
					uwUpdateItem &= (~UPDATE_PT2);
					PT2Temperature = data;
					printf("[P2=%d]\n",PT2Temperature);
		  	}
			  else
				  printf("[PT2]\n");
	  	}
		  else
	  	{
	  		if(PT100_config_pt2() == HAL_OK)
  			{
  				PT100_start_convertion();
				  printf("[set PT2]\n");
  			}
	  		else
				  printf("[ERR PT2]\n");
	  	}
			return;
		}
		
		if(uwUpdateItem & UPDATE_PT3)
		{
		  if(PT100_check_pt3() == HAL_OK)
	  	{
				int16_t data;
				
			  if(PT100_get_temp(&data) == HAL_OK)
		  	{
					uwUpdateItem &= (~UPDATE_PT3);
					PT3Temperature = data;
					printf("[PT3=%d]\n",PT3Temperature);
		  	}
			  else
				  printf("[PT3]\n");
	  	}
		  else
	  	{
	  		if(PT100_config_pt3() == HAL_OK)
  			{
  				PT100_start_convertion();
				  printf("[set PT3]\n");
  			}
	  		else
				  printf("[ERR PT3]\n");
	  	}
			return;
		}

		if(uwUpdateItem & UPDATE_WQ)
		{
			if(TDSConvCount >= TDS_CONVERTED_BUFFER_SIZE)
			{
				WaterQuality = (int)getTDSValue();
				TDSConvCount = 0;
			  printf("[WQ=%d]\n",WaterQuality);
			}
			else
			  printf("[GET WQ]\n");
			uwUpdateItem &= (~UPDATE_WQ);
			return;
		}
		
		if(uwUpdateItem & UPDATE_AIR)
		{
			int16_t air;
			
			if(SM1131_get_pressure(&air) == HAL_OK)
			{
				P1Pressure = air;
			  printf("[AIR=%d]\n",P1Pressure);
			}
			else
			  printf("[GET AIR]\n");
			uwUpdateItem &= (~UPDATE_AIR);
			return;
		}
		
		if(uwUpdateItem & UPDATE_PRESSURE)
		{
			int16_t pressure;
			
			if(SSCDANT100PA2A5_get_pressure(&pressure) == HAL_OK)
			{
				P2Pressure = pressure;
			  printf("[P2=%d]\n",P2Pressure);
			}
			else
			  printf("[GET P2]\n");
			uwUpdateItem &= (~UPDATE_PRESSURE);
			return;
		}
		
		if(uwUpdateItem & UPDATE_TDS)
		{
		  if(TDS_ConvStart() == HAL_OK)
	  	{
			  printf("[GET WQ]\n");
	  	}
		  else
			  printf("[ERR WQ]\n");
			uwUpdateItem &= (~UPDATE_TDS);
			return;
		}
		
	}
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @param  htim : TIM handle
  * @retval None
  */
void SENSOR_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance != TIM16)
		return;

	switch(hUpdateCounter) 
	{
		case 0:
			if(uwUpdateItem)
				printf("E0\n");
			
			hUpdateCounter++;
			uwUpdateItem = uwUpdateList | UPDATE_ENABLE;
			break;
		case UPDATE_CNT_MAX:
			if(uwUpdateItem)
				printf("E1\n");
			
			hUpdateCounter = 0;
			uwUpdateItem |= UPDATE_ENABLE;
			break;
		default:
			hUpdateCounter++;
			if(uwUpdateItem)
				uwUpdateItem |= UPDATE_ENABLE;
			break;
	}
}

void TDS_ConvCpltCallback(uint16_t* pData)
{
	UNUSED(pData);
 	uwUpdateList |= UPDATE_WQ;
}


