

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SSCDANT100PA2A5_H
#define __SSCDANT100PA2A5_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
#define SSCDANT100PA2A5_UNIT_M_BAR			

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup SSCDANT100PA2A5_Exported_Functions
  * @{
  */

/** @addtogroup SSCDANT100PA2A5_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
HAL_StatusTypeDef SSCDANT100PA2A5_Init(void);
/**
  * @}
  */

/** @addtogroup SSCDANT100PA2A5_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
HAL_StatusTypeDef SSCDANT100PA2A5_start_FMR(void);
HAL_StatusTypeDef SSCDANT100PA2A5_start_PMR(void);
HAL_StatusTypeDef SSCDANT100PA2A5_get_pressure(int16_t  * p_pressure);
HAL_StatusTypeDef SSCDANT100PA2A5_get_temperature(int16_t  * p_temperature);
/**
  * @}
  */

/** @addtogroup SSCDANT100PA2A5_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __SSCDANT100PA2A5_H */



