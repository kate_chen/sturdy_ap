

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PT100_H
#define __PT100_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
//#define PT100_IOUT_500UA		

/* Exported types ------------------------------------------------------------*/
enum {
	TRANSFER_WAIT,
	TRANSFER_COMPLETE,
	TRANSFER_ERROR
};
/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/


/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup PT100_Exported_Functions
  * @{
  */

/** @addtogroup PT100_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
void PT100_Init(void);
/**
  * @}
  */

/** @addtogroup PT100_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
HAL_StatusTypeDef PT100_config_pt1(void);
HAL_StatusTypeDef PT100_config_pt2(void);
HAL_StatusTypeDef PT100_config_pt3(void);
HAL_StatusTypeDef PT100_check_pt1(void);
HAL_StatusTypeDef PT100_check_pt2(void);
HAL_StatusTypeDef PT100_check_pt3(void);
HAL_StatusTypeDef PT100_start_convertion(void);
HAL_StatusTypeDef PT100_read_data(int16_t *p_data);
HAL_StatusTypeDef PT100_read_config(void);
HAL_StatusTypeDef PT100_get_temp(int16_t *p_data);

/**
  * @}
  */

/** @addtogroup PT100_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __PT100_H */



