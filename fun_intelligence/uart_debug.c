
/* Includes ------------------------------------------------------------------*/
#include "uart_debug.h"
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* UART handler declaration */
UART_HandleTypeDef huart_debug;

/* Buffer used for reception */
uint8_t aRXBufferA[DEBUGBUFFERSIZE];
uint8_t aRXBufferB[DEBUGBUFFERSIZE];
__IO uint32_t uwNbReceivedChars = 0;
__IO uint32_t uwBufferReadyIndication = 0;
uint8_t *pBufferReadyForUser;
uint8_t *pBufferReadyForReception;

uint32_t tickstart;
__IO uint32_t uwError = 0;
__IO uint32_t uwReceiveError = 0;
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the EVAL_COM1 and Loop until the end of transmission */
  HAL_UART_Transmit(&huart_debug, (uint8_t *)&ch, 1, 10); 

  return ch;
}


/**
  * @brief UART_DEBUG print system time Function
  * @param p_log pointer of log to print
  * @retval None
  */
extern uint32_t CurrentLoopTimeTicks;
void UART_DEBUG_PrintSysTime(void)
{
	tickstart = HAL_GetTick();
	CurrentLoopTimeTicks = HAL_GetTick();
//	printf("\r[%d]",tickstart);
}

/**
  * @brief UART_DEBUG start to receive command Function
  * @param p_log pointer of log to print
  * @retval None
  */
void UART_DEBUG_ReceiveCMD(void)
{
  uwReceiveError = HAL_UART_Receive_IT(&huart_debug,pBufferReadyForReception,1);
}

/**
  * @brief UART_DEBUG Initialization Function
  * @param None
  * @retval None
  */
void UART_DEBUG_Init(void)
{
  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  #ifdef DEBUG_USART1
  huart_debug.Instance = USART1;
  #else
  huart_debug.Instance = USART2;
  #endif
  huart_debug.Init.BaudRate = 115200;
  huart_debug.Init.WordLength = UART_WORDLENGTH_8B;
  huart_debug.Init.StopBits = UART_STOPBITS_1;
  huart_debug.Init.Parity = UART_PARITY_NONE;
  huart_debug.Init.Mode = UART_MODE_TX_RX;
  huart_debug.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart_debug.Init.OverSampling = UART_OVERSAMPLING_16;
  huart_debug.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart_debug.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_RXOVERRUNDISABLE_INIT|UART_ADVFEATURE_DMADISABLEONERROR_INIT;
  huart_debug.AdvancedInit.OverrunDisable = UART_ADVFEATURE_OVERRUN_DISABLE;
  huart_debug.AdvancedInit.DMADisableonRxError = UART_ADVFEATURE_DMA_DISABLEONRXERROR;
  if (HAL_UART_Init(&huart_debug) != HAL_OK)
  {
    Error_Handler();
  }

  /* Initializes Buffer swap mechanism :
     2 physical buffers aRXBufferA and aRXBufferB (RX_BUFFER_SIZE length)
     Any data received will be stored in active buffer : the number max of 
     data received is RX_BUFFER_SIZE */
  pBufferReadyForReception = aRXBufferA;
  pBufferReadyForUser      = aRXBufferB;
  uwNbReceivedChars = 0;
  uwBufferReadyIndication = 0;

  if(HAL_UART_Receive_IT(&huart_debug,pBufferReadyForReception,1) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */
}

/**
  * @brief  Rx Transfer completed callback
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report end of DMA Rx transfer, and 
  *         you can add your own implementation.
  * @retval None
  */
void UART_DEBUG_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{

	if(UartHandle->Instance != huart_debug.Instance)
		return;
  uint8_t ReceiveData = pBufferReadyForReception[uwNbReceivedChars++];
  
  //check if debug command is ready
  if ((uwNbReceivedChars == DEBUGBUFFERSIZE) || (ReceiveData == '\r'))
  {
    uint8_t *ptemp;
    
    //ReadyBuffer is processed & have valid data to process. 
    if(uwNbReceivedChars > 1)
    {
      /* Swap buffers for next bytes to be received */
      ptemp = pBufferReadyForUser;
      pBufferReadyForUser = pBufferReadyForReception;
      pBufferReadyForReception = ptemp;

      /* Set Buffer swap indication */
      uwBufferReadyIndication = uwNbReceivedChars;
      uwNbReceivedChars = 0;
    }
    //Discard this command.
    else
    {
      uwNbReceivedChars = 0;
      UART_DEBUG_ReceiveCMD();
    }
  }
  //Receive next byte.
  else
    uwReceiveError = HAL_UART_Receive_IT(&huart_debug,&pBufferReadyForReception[uwNbReceivedChars],1);
  
  //TX send back received data.
  HAL_UART_Transmit(&huart_debug, (uint8_t *)&ReceiveData, 1, 5); 
}

/**
  * @brief  UART error callbacks
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report transfer error, and you can
  *         add your own implementation.
  * @retval None
  */
void UART_DEBUG_ErrorCallback(UART_HandleTypeDef *UartHandle)
{
	if(UartHandle->Instance != huart_debug.Instance)
		return;
  /* Transfer error in reception/transmission process */
  if(UartHandle->ErrorCode)
  {
    uwError=UartHandle->ErrorCode;
    Error_Handler();
  }
	
}

