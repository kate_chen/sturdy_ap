

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AT24CM02_H
#define __AT24CM02_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup AT24CM02_Exported_Functions
  * @{
  */

/** @addtogroup AT24CM02_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
void AT24CM02_Init(void);
/**
  * @}
  */

/** @addtogroup AT24CM02_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
HAL_StatusTypeDef AT24CM02_write_page_0(uint16_t MemAddress, uint8_t *pData, uint16_t Size);
HAL_StatusTypeDef AT24CM02_no_check_write_page_0(uint16_t MemAddress, uint8_t *pData, uint16_t Size);
HAL_StatusTypeDef AT24CM02_read_page_0(uint16_t MemAddress, uint8_t *pData, uint16_t Size);
/**
  * @}
  */

/** @addtogroup AT24CM02_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __AT24CM02_H */



