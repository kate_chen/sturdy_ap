

/* Includes ------------------------------------------------------------------*/
#include "TMP75.h"
#include "main.h"
#include "sensor_i2c.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private constants ---------------------------------------------------------*/
#define TMP75_GENERAL_CALL_ADDRESS					0x00
#define TMP75_GENERAL_CALL_RESET					0x06

#define TMP75_DEV_ADDRESS					0x90

#define TMP75_TEMP_REG					0x00
#define TMP75_CONFIG_REG					0x01
#define TMP75_T_LOW_REG					0x02
#define TMP75_T_HIGH_REG					0x03

//CONFIG register bits
#define TMP75_CONFIG_REG_SD     			 (1 << 0)
#define TMP75_CONFIG_REG_TM      (1 << 1)
#define TMP75_CONFIG_REG_POL      (1 << 2)
#define TMP75_CONFIG_REG_F(x)      (((x) & 0x03) << 3)
#define TMP75_CONFIG_REG_R(x)      (((x) & 0x03) << 5)
#define TMP75_CONFIG_REG_OS						(1 << 7)

//read/write register size
#define TMP75_SIZE_READ_TEMP					2
#define TMP75_SIZE_CONFIG			1
#define TMP75_SIZE_T_LOW				2
#define TMP75_SIZE_T_HIGH				2

//data offset of serial read registers from TMP75_CMD_REG(0x2E)
#define TMP75_INDEX_TEMP_H					0
#define TMP75_INDEX_TEMP_L					1

#define TMP75_MASK_CONFIG_R      0x60

#define TMP75_RESOLUTION_9BIT					0
#define TMP75_RESOLUTION_10BIT					1
#define TMP75_RESOLUTION_11BIT					2
#define TMP75_RESOLUTION_12BIT					3

#define TMP75_TEMP_COEFF			10
#define TMP75_RESOLUTION_9_COEFF			2
#define TMP75_RESOLUTION_10_COEFF			4
#define TMP75_RESOLUTION_11_COEFF			8
#define TMP75_RESOLUTION_12_COEFF			16

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* Variable used to store TMP75 temperature, pressure, status data */
uint8_t ubTMP75Config;
uint8_t ubTMP75Data[TMP75_SIZE_READ_TEMP];



/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

/**
  * @brief TMP75 ADC Initialization Function
  * @param None
  * @retval None
  */
HAL_StatusTypeDef TMP75_Init(void)
{
	HAL_StatusTypeDef ret;

	//ret = SENSOR_I2C_mem_write(TMP75_GENERAL_CALL_ADDRESS,TMP75_GENERAL_CALL_RESET,I2C_MEMADD_SIZE_8BIT,&ubTMP75Config,0);
	
	//////HAL_Delay(500);
	//if(ret != HAL_OK)
 //   return ret;

	ret = SENSOR_I2C_check_ready(TMP75_DEV_ADDRESS,1000,1000);
	if(ret != HAL_OK)
    return ret;

	//read seral number register 0 &1
	ret = TMP75_read_config();
	
	if(ret != HAL_OK)
    return ret;

  return ret;
}

HAL_StatusTypeDef TMP75_read_config(void)
{
	HAL_StatusTypeDef ret;
	
	ret = SENSOR_I2C_mem_read(TMP75_DEV_ADDRESS,TMP75_CONFIG_REG,I2C_MEMADD_SIZE_8BIT,&ubTMP75Config,TMP75_SIZE_CONFIG);
	
	printf("\nTMP75 CONFIG %d-%02X\n",ret ,ubTMP75Config);
  return ret;
}

HAL_StatusTypeDef TMP75_config_resolution(uint8_t resolution)
{
	HAL_StatusTypeDef ret;

	ubTMP75Config &= ~TMP75_MASK_CONFIG_R;
	ubTMP75Config |= TMP75_CONFIG_REG_R(resolution);
	
	ret = SENSOR_I2C_mem_write(TMP75_DEV_ADDRESS,TMP75_CONFIG_REG,I2C_MEMADD_SIZE_8BIT,&ubTMP75Config,TMP75_SIZE_CONFIG);
	
	printf("TMP75 R %d-%02X,\n",ret,ubTMP75Config);
  return ret;
}

HAL_StatusTypeDef TMP75_read_data(void)
{
	HAL_StatusTypeDef ret;
	
	ret = SENSOR_I2C_mem_read(TMP75_DEV_ADDRESS,TMP75_TEMP_REG,I2C_MEMADD_SIZE_8BIT,ubTMP75Data,TMP75_SIZE_READ_TEMP);
	
	printf("TMP75 C %02X-%02X,%02X,\n",ubTMP75Config,ubTMP75Data[0],ubTMP75Data[1]);
  return ret;
}

HAL_StatusTypeDef TMP75_get_temperature(int16_t  * p_temperature)
{
	HAL_StatusTypeDef ret;
	uint32_t tmp;
	
	ret = SENSOR_I2C_mem_read(TMP75_DEV_ADDRESS,TMP75_TEMP_REG,I2C_MEMADD_SIZE_8BIT,ubTMP75Data,TMP75_SIZE_READ_TEMP);
	
	printf("TMP75 %d C %02X-%02X,%02X,\n",ret,ubTMP75Config,ubTMP75Data[0],ubTMP75Data[1]);

	if(ret != HAL_OK)
    return ret;
	
	tmp = (((uint32_t)ubTMP75Data[TMP75_INDEX_TEMP_H] << 8) | ubTMP75Data[TMP75_INDEX_TEMP_L]) >> 4;
	printf("TMP75 tmp-%X,",tmp);

	tmp = (uint32_t)((tmp * TMP75_TEMP_COEFF) / TMP75_RESOLUTION_12_COEFF);
	printf("%X\n",tmp);

		*p_temperature = (int16_t)tmp;
	
  return HAL_OK;
}


/**
  * @brief  Fan feedback update complete callback in non blocking mode 
  * @param pData Pointer to data buffer.
  * @retval None
  */
__weak void TMP75_CpltCallback(uint8_t * pData)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(pData);

  /* NOTE : This function should not be modified. When the callback is needed,
            function HAL_ADC_ConvCpltCallback must be implemented in the user file.
   */
}


