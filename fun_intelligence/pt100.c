

/* Includes ------------------------------------------------------------------*/
#include "pt100.h"
#include "main.h"
#include "ad7124.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
//temperature transform coefficients
//ADC resolution(24 bit) coefficients : 2^23
#define PT100_TEMP_COEFF 0x800000
//reference resistor coefficients : 5.11 k�[
#define PT100_TEMP_R_REF 5110
#define PT100_TEMP_I_REF 0.0005
#define PT100_TEMP_I_AIN 0.00025
#ifdef PT100_IOUT_500UA
//gain coefficients : 16
#define PT100_TEMP_GAIN 16
#else
//gain coefficients : 32
#define PT100_TEMP_GAIN 32
#endif
//RTD resistor 0 �J coefficients : 100 �[
#define PT100_TEMP_R_0_DEGREE 100.0
//RTD resistor change coefficients : 
#define PT100_TEMP_R_CHANGE 0.385

#define PT100_CHANNEL_ENABLE 0x8000
#define PT100_ADC_CTRL_STANDBY   0x0488
#define PT100_ADC_CTRL_CONTINUOUS_CONVERT 0x0480
#define PT100_ADC_CTRL_SINGLE_CONVERT 0x0484

//sinc4 filter,POST_FILTER_011, Full power mode 50 SPS
#define PT100_FILTER_CALIB 0x060180
//sinc4 filter,POST_FILTER_011, Full power mode 320 SPS
#define PT100_FILTER_CONFIG 0x06003C

#define PT100_PT1_CH_DISABLE 0x1043
#define PT100_PT2_CH_DISABLE 0x20C7
#define PT100_PT3_CH_DISABLE 0x314B

#if PT100_IOUT_500UA
#define PT100_CH_CONFIG_CALIB 0x0864

#define PT100_PT1_IO_CTRL_1 0x2410
#define PT100_PT1_CONFIG 0x09E4
//#define PT100_PT1_CONFIG 0x0804

#define PT100_PT2_IO_CTRL_1 0x2454
#define PT100_PT2_CONFIG 0x09E4

#define PT100_PT3_IO_CTRL_1 0x2498
#define PT100_PT3_CONFIG 0x09E4

#else	//250 uA
#define PT100_CH_CONFIG_CALIB 0x0865

#define PT100_PT1_IO_CTRL_1 0x1B10
#define PT100_PT1_CONFIG 0x09E5
//#define PT100_PT1_CONFIG 

#define PT100_PT2_IO_CTRL_1 0x1B54
#define PT100_PT2_CONFIG 0x09E5

#define PT100_PT3_IO_CTRL_1 0x1B98
#define PT100_PT3_CONFIG 0x09E5
#endif

/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* SPI handler declaration */
SPI_HandleTypeDef hpt100;
//DMA_HandleTypeDef hdma_pt100_rx;
//DMA_HandleTypeDef hdma_pt100_tx;

// struct ad7124_dev dev_pt100;                    /* A new driver instance */
 struct ad7124_dev hdev_pt100; /* A driver handle to pass around */

//__IO uint32_t uwError = 0;
__IO uint32_t wTransferState = TRANSFER_COMPLETE;

struct ad7124_st_reg pt100_regs[AD7124_REG_NO] = {
	{0x00, 0x00,   1, 2}, /* AD7124_Status */
	{0x01, PT100_ADC_CTRL_STANDBY, 2, 1}, /* AD7124_ADC_Control */
	{0x02, 0x0000, 3, 2}, /* AD7124_Data */
	{0x03, PT100_PT1_IO_CTRL_1, 3, 1}, /* AD7124_IOCon1 */
	{0x04, 0x0000, 2, 1}, /* AD7124_IOCon2 */
	{0x05, 0x00,   1, 2}, /* AD7124_ID */
	{0x06, 0x0000, 3, 2}, /* AD7124_Error */
	{0x07, 0x0040, 3, 1}, /* AD7124_Error_En */
	{0x08, 0x00,   1, 2}, /* AD7124_Mclk_Count */
	{0x09, 0x0001, 2, 1}, /* AD7124_Channel_0 */
	{0x0A, PT100_PT1_CH_DISABLE, 2, 1}, /* AD7124_Channel_1 */
	{0x0B, PT100_PT2_CH_DISABLE, 2, 1}, /* AD7124_Channel_2 */
	{0x0C, PT100_PT3_CH_DISABLE, 2, 1}, /* AD7124_Channel_3 */
	{0x0D, 0x0001, 2, 1}, /* AD7124_Channel_4 */
	{0x0E, 0x0001, 2, 1}, /* AD7124_Channel_5 */
	{0x0F, 0x0001, 2, 1}, /* AD7124_Channel_6 */
	{0x10, 0x0001, 2, 1}, /* AD7124_Channel_7 */
	{0x11, 0x0001, 2, 1}, /* AD7124_Channel_8 */
	{0x12, 0x0001, 2, 1}, /* AD7124_Channel_9 */
	{0x13, 0x0001, 2, 1}, /* AD7124_Channel_10 */
	{0x14, 0x0001, 2, 1}, /* AD7124_Channel_11 */
	{0x15, 0x0001, 2, 1}, /* AD7124_Channel_12 */
	{0x16, 0x0001, 2, 1}, /* AD7124_Channel_13 */
	{0x17, 0x0001, 2, 1}, /* AD7124_Channel_14 */
	{0x18, 0x0001, 2, 1}, /* AD7124_Channel_15 */
	{0x19, PT100_CH_CONFIG_CALIB, 2, 1}, /* AD7124_Config_0 */
	{0x1A, PT100_PT1_CONFIG, 2, 1}, /* AD7124_Config_1 */
	{0x1B, PT100_PT2_CONFIG, 2, 1}, /* AD7124_Config_2 */
	{0x1C, PT100_PT3_CONFIG, 2, 1}, /* AD7124_Config_3 */
	{0x1D, 0x0860, 2, 1}, /* AD7124_Config_4 */
	{0x1E, 0x0860, 2, 1}, /* AD7124_Config_5 */
	{0x1F, 0x0860, 2, 1}, /* AD7124_Config_6 */
	{0x20, 0x0860, 2, 1}, /* AD7124_Config_7 */
	{0x21, PT100_FILTER_CALIB, 3, 1}, /* AD7124_Filter_0 */
	{0x22, PT100_FILTER_CONFIG, 3, 1}, /* AD7124_Filter_1 */
	{0x23, PT100_FILTER_CONFIG, 3, 1}, /* AD7124_Filter_2 */
	{0x24, PT100_FILTER_CONFIG, 3, 1}, /* AD7124_Filter_3 */
	{0x25, 0x060180, 3, 1}, /* AD7124_Filter_4 */
	{0x26, 0x060180, 3, 1}, /* AD7124_Filter_5 */
	{0x27, 0x060180, 3, 1}, /* AD7124_Filter_6 */
	{0x28, 0x060180, 3, 1}, /* AD7124_Filter_7 */
	{0x29, 0x800000, 3, 1}, /* AD7124_Offset_0 */
	{0x2A, 0x800000, 3, 1}, /* AD7124_Offset_1 */
	{0x2B, 0x800000, 3, 1}, /* AD7124_Offset_2 */
	{0x2C, 0x800000, 3, 1}, /* AD7124_Offset_3 */
	{0x2D, 0x800000, 3, 1}, /* AD7124_Offset_4 */
	{0x2E, 0x800000, 3, 1}, /* AD7124_Offset_5 */
	{0x2F, 0x800000, 3, 1}, /* AD7124_Offset_6 */
	{0x30, 0x800000, 3, 1}, /* AD7124_Offset_7 */
	{0x31, 0x5558CC, 3, 1}, /* AD7124_Gain_0 */
	{0x32, 0x5558CC, 3, 1}, /* AD7124_Gain_1 */
	{0x33, 0x5558CC, 3, 1}, /* AD7124_Gain_2 */
	{0x34, 0x5558CC, 3, 1}, /* AD7124_Gain_3 */
	{0x35, 0x5558CC, 3, 1}, /* AD7124_Gain_4 */
	{0x36, 0x5558CC, 3, 1}, /* AD7124_Gain_5 */
	{0x37, 0x5558CC, 3, 1}, /* AD7124_Gain_6 */
	{0x38, 0x5558CC, 3, 1}, /* AD7124_Gain_7 */
};

//uint32_t pt100
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
void PT100_Init(void)
{
	struct ad7124_init_param pt100_init;
	int32_t ret;

  hpt100.Instance = SPI1;
  hpt100.Init.Mode = SPI_MODE_MASTER;
  hpt100.Init.Direction = SPI_DIRECTION_2LINES;
  hpt100.Init.DataSize = SPI_DATASIZE_8BIT;
  hpt100.Init.CLKPolarity = SPI_POLARITY_LOW;
  hpt100.Init.CLKPhase = SPI_PHASE_1EDGE;
  hpt100.Init.NSS = SPI_NSS_HARD_OUTPUT;
  hpt100.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hpt100.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hpt100.Init.TIMode = SPI_TIMODE_DISABLE;
  hpt100.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hpt100.Init.CRCPolynomial = 7;
  hpt100.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hpt100.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;

	pt100_init.regs = pt100_regs;
	pt100_init.spi_rdy_poll_cnt = 10;
	
	hdev_pt100.spi_desc = &hpt100;
	
	ret = ad7124_setup(&hdev_pt100,pt100_init);
	if(ret < 0)
    Error_Handler();

	ret = ad7124_read_register(&hdev_pt100, &pt100_regs[AD7124_ID]);
	if(ret < 0)
    Error_Handler();

	printf("PT100 ID %X\n",pt100_regs[AD7124_ID].value);
}

HAL_StatusTypeDef PT100_config_pt1(void)
{
	int32_t ret;

	pt100_regs[AD7124_IOCon1].value= PT100_PT1_IO_CTRL_1;
	pt100_regs[AD7124_Channel_1].value = (PT100_PT1_CH_DISABLE | PT100_CHANNEL_ENABLE);
	pt100_regs[AD7124_Channel_2].value = 0x0001;
	pt100_regs[AD7124_Channel_3].value = 0x0001;

	ret = ad7124_write_register(&hdev_pt100, pt100_regs[AD7124_IOCon1]);
	if(ret < 0)
		return HAL_ERROR;
	
	ret = ad7124_write_register(&hdev_pt100, pt100_regs[AD7124_Channel_2]);
	if(ret < 0)
		return HAL_ERROR;

	ret = ad7124_write_register(&hdev_pt100, pt100_regs[AD7124_Channel_3]);
	if(ret < 0)
		return HAL_ERROR;

	ret = ad7124_write_register(&hdev_pt100, pt100_regs[AD7124_Channel_1]);
	if(ret < 0)
		return HAL_ERROR;

	return HAL_OK;
}

HAL_StatusTypeDef PT100_config_pt2(void)
{
	int32_t ret;

	pt100_regs[AD7124_IOCon1].value= PT100_PT2_IO_CTRL_1;
	pt100_regs[AD7124_Channel_1].value = 0x0001;
	pt100_regs[AD7124_Channel_2].value = (PT100_PT2_CH_DISABLE | PT100_CHANNEL_ENABLE);
	pt100_regs[AD7124_Channel_3].value = 0x0001;

	ret = ad7124_write_register(&hdev_pt100, pt100_regs[AD7124_Channel_3]);
	if(ret < 0)
		return HAL_ERROR;

	ret = ad7124_write_register(&hdev_pt100, pt100_regs[AD7124_Channel_1]);
	if(ret < 0)
		return HAL_ERROR;

	ret = ad7124_write_register(&hdev_pt100, pt100_regs[AD7124_Channel_2]);
	if(ret < 0)
		return HAL_ERROR;

	ret = ad7124_write_register(&hdev_pt100, pt100_regs[AD7124_IOCon1]);
	if(ret < 0)
		return HAL_ERROR;
	
	return HAL_OK;
}

HAL_StatusTypeDef PT100_config_pt3(void)
{
	int32_t ret;

	pt100_regs[AD7124_IOCon1].value= PT100_PT3_IO_CTRL_1;
	pt100_regs[AD7124_Channel_1].value = 0x0001;
	pt100_regs[AD7124_Channel_2].value = 0x0001;
	pt100_regs[AD7124_Channel_3].value = (PT100_PT3_CH_DISABLE | PT100_CHANNEL_ENABLE);

	ret = ad7124_write_register(&hdev_pt100, pt100_regs[AD7124_Channel_1]);
	if(ret < 0)
		return HAL_ERROR;

	ret = ad7124_write_register(&hdev_pt100, pt100_regs[AD7124_Channel_2]);
	if(ret < 0)
		return HAL_ERROR;

	ret = ad7124_write_register(&hdev_pt100, pt100_regs[AD7124_Channel_3]);
	if(ret < 0)
		return HAL_ERROR;

	ret = ad7124_write_register(&hdev_pt100, pt100_regs[AD7124_IOCon1]);
	if(ret < 0)
		return HAL_ERROR;
	
	return HAL_OK;
}

HAL_StatusTypeDef PT100_start_convertion(void)
{
	int32_t ret;
	
	ret = ad7124_read_register(&hdev_pt100,&pt100_regs[AD7124_ADC_Control]);
	if(ret < 0)
		return HAL_ERROR;
	//printf("\nCTRL %X",pt100_regs[AD7124_ADC_Control].value);
	if(pt100_regs[AD7124_ADC_Control].value != PT100_ADC_CTRL_STANDBY)
		return HAL_BUSY;
	
	pt100_regs[AD7124_ADC_Control].value= PT100_ADC_CTRL_SINGLE_CONVERT;
	ret = ad7124_write_register(&hdev_pt100, pt100_regs[AD7124_ADC_Control]);
	if(ret < 0)
		return HAL_ERROR;
	
	return HAL_OK;
}

HAL_StatusTypeDef PT100_read_data(int16_t *p_data)
{
	int32_t ret;
	
	ret = ad7124_read_register(&hdev_pt100,&pt100_regs[AD7124_ADC_Control]);
	if(ret < 0)
		return HAL_ERROR;
	
	if(pt100_regs[AD7124_ADC_Control].value != PT100_ADC_CTRL_STANDBY)
		return HAL_BUSY;

	ret = ad7124_read_register(&hdev_pt100,&pt100_regs[AD7124_Status]);
	if(ret < 0)
		return HAL_ERROR;
	//printf("\nStatus %X",pt100_regs[AD7124_Status].value);

	uint32_t tmp;
	
	ret = ad7124_read_data(&hdev_pt100,(int32_t*)&tmp);
	if(ret < 0)
		return HAL_ERROR;

	pt100_regs[AD7124_ADC_Control].value= PT100_ADC_CTRL_SINGLE_CONVERT;
	ret = ad7124_write_register(&hdev_pt100, pt100_regs[AD7124_ADC_Control]);
	if(ret < 0)
		return HAL_ERROR;

	switch(AD7124_STATUS_REG_CH_ACTIVE(pt100_regs[AD7124_Status].value))
	{
		case 1:
			if(pt100_regs[AD7124_Channel_1].value & PT100_CHANNEL_ENABLE)
				break;
			else
				return HAL_BUSY;
		case 2:
			if(pt100_regs[AD7124_Channel_2].value & PT100_CHANNEL_ENABLE)
				break;
			else
				return HAL_BUSY;
			break;
		case 3:
			if(pt100_regs[AD7124_Channel_3].value & PT100_CHANNEL_ENABLE)
				break;
			else
				return HAL_BUSY;
			break;
		default :
			return HAL_BUSY;
	}


		float R_RTD = ((float)tmp / PT100_TEMP_COEFF)  -1;

		R_RTD = R_RTD * (PT100_TEMP_I_REF * PT100_TEMP_R_REF) / PT100_TEMP_GAIN / PT100_TEMP_I_AIN;
		//printf("\nPT%d,R_RTD %.4f",AD7124_STATUS_REG_CH_ACTIVE(pt100_regs[AD7124_Status].value),R_RTD);

		//temp = (R_RTD-100)/R_CHANGE
		R_RTD = (R_RTD - PT100_TEMP_R_0_DEGREE) / PT100_TEMP_R_CHANGE;
		//printf(",temp %.4f\n",R_RTD);
		*p_data = (int16_t)(R_RTD *10);
	
	return HAL_OK;
}

HAL_StatusTypeDef PT100_get_temp(int16_t *p_data)
{
	int32_t ret;
	
	ret = ad7124_read_register(&hdev_pt100,&pt100_regs[AD7124_Status]);
	if(ret < 0)
		return HAL_ERROR;
	//printf("\nStatus %X",pt100_regs[AD7124_Status].value);

	uint32_t tmp;
	
	ret = ad7124_read_data(&hdev_pt100,(int32_t*)&tmp);
	if(ret < 0)
		return HAL_ERROR;

	PT100_start_convertion();

	switch(AD7124_STATUS_REG_CH_ACTIVE(pt100_regs[AD7124_Status].value))
	{
		case 1:
			if(pt100_regs[AD7124_Channel_1].value & PT100_CHANNEL_ENABLE)
				break;
			else
				return HAL_BUSY;
		case 2:
			if(pt100_regs[AD7124_Channel_2].value & PT100_CHANNEL_ENABLE)
				break;
			else
				return HAL_BUSY;
		case 3:
			if(pt100_regs[AD7124_Channel_3].value & PT100_CHANNEL_ENABLE)
				break;
			else
				return HAL_BUSY;
		default :
			return HAL_BUSY;
	}


		float R_RTD = ((float)tmp / PT100_TEMP_COEFF)  -1;

		R_RTD = R_RTD * (PT100_TEMP_I_REF * PT100_TEMP_R_REF) / PT100_TEMP_GAIN / PT100_TEMP_I_AIN;
		//printf("\nPT%d,R_RTD %.4f",AD7124_STATUS_REG_CH_ACTIVE(pt100_regs[AD7124_Status].value),R_RTD);

		//temp = (R_RTD-100)/R_CHANGE
		R_RTD = (R_RTD - PT100_TEMP_R_0_DEGREE) / PT100_TEMP_R_CHANGE;
		//printf(",temp %.4f\n",R_RTD);
		*p_data = (int16_t)(R_RTD *10);
	
	return HAL_OK;
}

HAL_StatusTypeDef PT100_check_pt1(void)
{
	int32_t ret;

	ret = ad7124_read_register(&hdev_pt100,&pt100_regs[AD7124_Channel_1]);
	if(ret < 0)
		return HAL_ERROR;

	if(pt100_regs[AD7124_Channel_1].value & PT100_CHANNEL_ENABLE)
		return HAL_OK;

	return HAL_ERROR;
}

HAL_StatusTypeDef PT100_check_pt2(void)
{
	int32_t ret;

	ret = ad7124_read_register(&hdev_pt100,&pt100_regs[AD7124_Channel_2]);
	if(ret < 0)
		return HAL_ERROR;

	if(pt100_regs[AD7124_Channel_2].value & PT100_CHANNEL_ENABLE)
		return HAL_OK;

	return HAL_ERROR;
}

HAL_StatusTypeDef PT100_check_pt3(void)
{
	int32_t ret;

	ret = ad7124_read_register(&hdev_pt100,&pt100_regs[AD7124_Channel_3]);
	if(ret < 0)
		return HAL_ERROR;

	if(pt100_regs[AD7124_Channel_3].value & PT100_CHANNEL_ENABLE)
		return HAL_OK;

	return HAL_ERROR;
}

HAL_StatusTypeDef PT100_read_config(void)
{
	int32_t ret;

	ret = ad7124_read_register(&hdev_pt100,&pt100_regs[AD7124_Status]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\npt100 STATUS %X",pt100_regs[AD7124_Status].value);
	
	ret = ad7124_read_register(&hdev_pt100,&pt100_regs[AD7124_ADC_Control]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\npt100 CTRL %X",pt100_regs[AD7124_ADC_Control].value);
	
	ret = ad7124_read_register(&hdev_pt100,&pt100_regs[AD7124_IOCon1]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\npt100 IOCon1 %X",pt100_regs[AD7124_IOCon1].value);
	
	ret = ad7124_read_register(&hdev_pt100,&pt100_regs[AD7124_Channel_1]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\npt100 CH1 %X",pt100_regs[AD7124_Channel_1].value);
	
	ret = ad7124_read_register(&hdev_pt100,&pt100_regs[AD7124_Channel_2]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\npt100 CH2 %X",pt100_regs[AD7124_Channel_2].value);
	
	ret = ad7124_read_register(&hdev_pt100,&pt100_regs[AD7124_Channel_3]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\npt100 CH3 %X",pt100_regs[AD7124_Channel_3].value);
	
	ret = ad7124_read_register(&hdev_pt100,&pt100_regs[AD7124_Config_1]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\npt100 CFG1 %X",pt100_regs[AD7124_Config_1].value);
	
	ret = ad7124_read_register(&hdev_pt100,&pt100_regs[AD7124_Config_2]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\npt100 CFG2 %X",pt100_regs[AD7124_Config_2].value);
	
	ret = ad7124_read_register(&hdev_pt100,&pt100_regs[AD7124_Config_3]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\npt100 CFG3 %X\n",pt100_regs[AD7124_Config_3].value);
	
	
	return HAL_OK;
}


