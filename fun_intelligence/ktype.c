

/* Includes ------------------------------------------------------------------*/
#include <math.h>
#include "ktype.h"
#include "main.h"
#include "RTD.h"
#include "Thermocouple.h"
#include "ad7124.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
#define KTYPE_CHANNEL_ENABLE 0x8000

#define KTYPE_ADC_CTRL_STANDBY   0x0588
#define KTYPE_ADC_CTRL_CONTINUOUS_CONVERT 0x0580
#define KTYPE_ADC_CTRL_SINGLE_CONVERT 0x0584

#define KTYPE_IOCON1_KTYPE1 0x0501
#define KTYPE_IOCON1_KTYPE2 0x0503
#define KTYPE_IOCON1_KTYPE3 0x0505
#define KTYPE_IOCON1_KTYPE4 0x0507

#define KTYPE_IOCON2_KTYPE1 0x0001
#define KTYPE_IOCON2_KTYPE2 0x0004
#define KTYPE_IOCON2_KTYPE3 0x0010
#define KTYPE_IOCON2_KTYPE4 0x0040

#define KTYPE_CH_CJ1_DISABLE 0x0028
#define KTYPE_CH_CJ2_DISABLE 0x0068
#define KTYPE_CH_CJ3_DISABLE 0x00A8
#define KTYPE_CH_CJ4_DISABLE 0x00E8
//#define KTYPE_CH_KTYPE1_DISABLE 0x100F
#define KTYPE_CH_KTYPE1_DISABLE 0x1013
#define KTYPE_CH_KTYPE2_DISABLE 0x1053
#define KTYPE_CH_KTYPE3_DISABLE 0x1093
#define KTYPE_CH_KTYPE4_DISABLE 0x10D3

#define KTYPE_COFIG_RTD 0x09E8
#define KTYPE_COFIG_THERMOCOUPLE 0x09F5

//sinc3 filter,POST_FILTER_011, Full power mode 240 SPS
#define KTYPE_FILTER_RTD 0x460050
#define KTYPE_FILTER_THERMOCOUPLE 0x460050


//ADC resolution(24 bit) coefficients : 2^23
#define KTYPE_TEMP_COEFF 0x800000
//RTD reference resistor coefficients : 1.6 k�[
#define KTYPE_TEMP_RTD_R_REF 1600
#define KTYPE_TEMP_RTD_I_REF 0.75    //mA
#define KTYPE_TEMP_RTD_I_AIN 0.75    //mA
//RTD gain coefficients : 1
#define KTYPE_TEMP_RTD_GAIN 1
//TH gain coefficients : 32
#define KTYPE_TEMP_KTYPE_GAIN 32
#define KTYPE_TEMP_KTYPE_V_REF 2500.0     //mV
//TH resistor 0 �J coefficients : 100 �[
#define KTYPE_TEMP_V_400_DEGREE 100.0
//TH resistor change coefficients : 
#define KTYPE_TEMP_R_CHANGE 0.385

#define R5      1600.0  //ohmi
#define I_EXT   0.75    //mA

#define VREF_EXT    (R5*I_EXT)  //mV
#define VREF_INT     2500.0     //mV

#define _2_23     8388608.0

#define GAIN_RTD       1  //GAIN1
#define GAIN_TH        32  //GAIN32

#define TC_OFFSET_VOLTAGE    0.00    // mV compensation for system offset

#define POLY_CALC(retVal, inVal, coeff_array) \
{ \
    float expVal = 1.0f; \
    const float* coeff = coeff_array; \
    retVal = 0.0f; \
    while(*coeff != 1.0f)\
    { \
        retVal += *coeff * expVal; \
        expVal *= inVal; \
        coeff++; \
    }\
}


/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* SPI handler declaration */
SPI_HandleTypeDef hktype;
//DMA_HandleTypeDef hdma_ktype_rx;
//DMA_HandleTypeDef hdma_ktype_tx;

//__IO uint32_t uwError = 0;
//__IO uint32_t wKtypeTransferState = TRANSFER_COMPLETE;
float ktype_mv_data[8];

//TH tempeature variable
int32_t adcValue0[4], adcValue1[4];
//float rRtdValue[4], temp0[4], temp1[4];
float temp0[4], temp1[4];
float cj_Voltage[4], th_Voltage_read[4], th_Voltage[4];

// struct ad7124_dev dev_ktype;                    /* A new driver instance */
 struct ad7124_dev hdev_ktype; /* A driver handle to pass around */

struct ad7124_st_reg ktype_regs[AD7124_REG_NO] = {
	{0x00, 0x00,   1, 2}, /* AD7124_Status */
	{0x01, KTYPE_ADC_CTRL_STANDBY, 2, 1}, /* AD7124_ADC_Control */
	{0x02, 0x0000, 3, 2}, /* AD7124_Data */
//	{0x03, 0x0500, 3, 1}, /* AD7124_IOCon1 */
	{0x03, KTYPE_IOCON1_KTYPE2, 3, 1}, /* AD7124_IOCon1 */
	{0x04, 0x0000, 2, 1}, /* AD7124_IOCon2 */
	{0x05, 0x00,   1, 2}, /* AD7124_ID */
	{0x06, 0x0000, 3, 2}, /* AD7124_Error */
	{0x07, 0x0040, 3, 1}, /* AD7124_Error_En */
	{0x08, 0x00,   1, 2}, /* AD7124_Mclk_Count */
	{0x09, KTYPE_CH_CJ1_DISABLE, 2, 1}, /* AD7124_Channel_0 */
	{0x0A, KTYPE_CH_CJ2_DISABLE, 2, 1}, /* AD7124_Channel_1 */
	{0x0B, KTYPE_CH_CJ3_DISABLE, 2, 1}, /* AD7124_Channel_2 */
	{0x0C, KTYPE_CH_CJ4_DISABLE, 2, 1}, /* AD7124_Channel_3 */
	{0x0D, KTYPE_CH_KTYPE1_DISABLE, 2, 1}, /* AD7124_Channel_4 */
	{0x0E, KTYPE_CH_KTYPE2_DISABLE, 2, 1}, /* AD7124_Channel_5 */
	{0x0F, KTYPE_CH_KTYPE3_DISABLE, 2, 1}, /* AD7124_Channel_6 */
	{0x10, KTYPE_CH_KTYPE4_DISABLE, 2, 1}, /* AD7124_Channel_7 */
	{0x11, KTYPE_CH_CJ1_DISABLE, 2, 1}, /* AD7124_Channel_8 */
	{0x12, KTYPE_CH_CJ2_DISABLE, 2, 1}, /* AD7124_Channel_9 */
	{0x13, KTYPE_CH_CJ3_DISABLE, 2, 1}, /* AD7124_Channel_10 */
	{0x14, KTYPE_CH_CJ4_DISABLE, 2, 1}, /* AD7124_Channel_11 */
	{0x15, KTYPE_CH_KTYPE1_DISABLE, 2, 1}, /* AD7124_Channel_12 */
	{0x16, KTYPE_CH_KTYPE2_DISABLE, 2, 1}, /* AD7124_Channel_13 */
	{0x17, KTYPE_CH_KTYPE3_DISABLE, 2, 1}, /* AD7124_Channel_14 */
	{0x18, KTYPE_CH_KTYPE4_DISABLE, 2, 1}, /* AD7124_Channel_15 */
	{0x19, KTYPE_COFIG_RTD, 2, 1}, /* AD7124_Config_0 */
	{0x1A, KTYPE_COFIG_THERMOCOUPLE, 2, 1}, /* AD7124_Config_1 */
	{0x1B, 0x0860, 2, 1}, /* AD7124_Config_2 */
	{0x1C, 0x0860, 2, 1}, /* AD7124_Config_3 */
	{0x1D, 0x0860, 2, 1}, /* AD7124_Config_4 */
	{0x1E, 0x0860, 2, 1}, /* AD7124_Config_5 */
	{0x1F, 0x0860, 2, 1}, /* AD7124_Config_6 */
	{0x20, 0x0860, 2, 1}, /* AD7124_Config_7 */
	{0x21, KTYPE_FILTER_RTD, 3, 1}, /* AD7124_Filter_0 */
	{0x22, KTYPE_FILTER_THERMOCOUPLE, 3, 1}, /* AD7124_Filter_1 */
	{0x23, 0x060180, 3, 1}, /* AD7124_Filter_2 */
	{0x24, 0x060180, 3, 1}, /* AD7124_Filter_3 */
	{0x25, 0x060180, 3, 1}, /* AD7124_Filter_4 */
	{0x26, 0x060180, 3, 1}, /* AD7124_Filter_5 */
	{0x27, 0x060180, 3, 1}, /* AD7124_Filter_6 */
	{0x28, 0x060180, 3, 1}, /* AD7124_Filter_7 */
	{0x29, 0x800000, 3, 1}, /* AD7124_Offset_0 */
	{0x2A, 0x800000, 3, 1}, /* AD7124_Offset_1 */
	{0x2B, 0x800000, 3, 1}, /* AD7124_Offset_2 */
	{0x2C, 0x800000, 3, 1}, /* AD7124_Offset_3 */
	{0x2D, 0x800000, 3, 1}, /* AD7124_Offset_4 */
	{0x2E, 0x800000, 3, 1}, /* AD7124_Offset_5 */
	{0x2F, 0x800000, 3, 1}, /* AD7124_Offset_6 */
	{0x30, 0x800000, 3, 1}, /* AD7124_Offset_7 */
	{0x31, 0x5558CC, 3, 1}, /* AD7124_Gain_0 */
	{0x32, 0x5558CC, 3, 1}, /* AD7124_Gain_1 */
	{0x33, 0x5558CC, 3, 1}, /* AD7124_Gain_2 */
	{0x34, 0x5558CC, 3, 1}, /* AD7124_Gain_3 */
	{0x35, 0x5558CC, 3, 1}, /* AD7124_Gain_4 */
	{0x36, 0x5558CC, 3, 1}, /* AD7124_Gain_5 */
	{0x37, 0x5558CC, 3, 1}, /* AD7124_Gain_6 */
	{0x38, 0x5558CC, 3, 1}, /* AD7124_Gain_7 */
};

//uint32_t ktype
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

static float KTYPE_data_to_resistance(int32_t data)
{

   float rRtd;

   rRtd = (R5*(data - _2_23))/(_2_23 *GAIN_RTD);

   return rRtd;
}


/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
void KTYPE_Init(void)
{
	struct ad7124_init_param ktype_init;
	int32_t ret;

  hktype.Instance = SPI2;
  hktype.Init.Mode = SPI_MODE_MASTER;
  hktype.Init.Direction = SPI_DIRECTION_2LINES;
  hktype.Init.DataSize = SPI_DATASIZE_8BIT;
  hktype.Init.CLKPolarity = SPI_POLARITY_LOW;
  hktype.Init.CLKPhase = SPI_PHASE_1EDGE;
  hktype.Init.NSS = SPI_NSS_SOFT;
  hktype.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hktype.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hktype.Init.TIMode = SPI_TIMODE_DISABLE;
  hktype.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hktype.Init.CRCPolynomial = 7;
  hktype.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hktype.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;

	ktype_init.regs = ktype_regs;
	ktype_init.spi_rdy_poll_cnt = 10;
	
	hdev_ktype.spi_desc = &hktype;
	
	ret = ad7124_setup(&hdev_ktype,ktype_init);
	if(ret < 0)
    Error_Handler();

	ret = ad7124_read_register(&hdev_ktype, &ktype_regs[AD7124_ID]);
	if(ret < 0)
    Error_Handler();

	printf("KTYPE ID %X\n",ktype_regs[AD7124_ID].value);
}

HAL_StatusTypeDef KTYPE_channel_enable(uint8_t channel)
{
	int32_t ret;
	uint8_t ch;

	for(ch=AD7124_Channel_0;ch<AD7124_Channel_8;ch++)
	{
		if(ch == channel)
		{
			ktype_regs[ch].value |= KTYPE_CHANNEL_ENABLE;
			ret = ad7124_write_register(&hdev_ktype, ktype_regs[ch]);
			if(ret < 0)
				return HAL_ERROR;
		}
		else if(ktype_regs[ch].value  | KTYPE_CHANNEL_ENABLE)
		{
			ktype_regs[ch].value &= (~(uint32_t)KTYPE_CHANNEL_ENABLE);
			ret = ad7124_write_register(&hdev_ktype, ktype_regs[ch]);
			if(ret < 0)
				return HAL_ERROR;
		}
	}
	return HAL_OK;
}

HAL_StatusTypeDef KTYPE_config_cj1(void)
{
	int32_t ret;

	ktype_regs[AD7124_IOCon1].value= KTYPE_IOCON1_KTYPE1;
	ret = ad7124_write_register(&hdev_ktype, ktype_regs[AD7124_IOCon1]);
	if(ret < 0)
		return HAL_ERROR;

	//HAL_Delay(1);

	ret = KTYPE_channel_enable(AD7124_Channel_0);
	if(ret < 0)
		return HAL_ERROR;

	return HAL_OK;
}

HAL_StatusTypeDef KTYPE_config_cj2(void)
{
	int32_t ret;

	ktype_regs[AD7124_IOCon1].value= KTYPE_IOCON1_KTYPE2;
	ret = ad7124_write_register(&hdev_ktype, ktype_regs[AD7124_IOCon1]);
	if(ret < 0)
		return HAL_ERROR;

	//HAL_Delay(1);

	ret = KTYPE_channel_enable(AD7124_Channel_1);
	if(ret < 0)
		return HAL_ERROR;

	return HAL_OK;
}

HAL_StatusTypeDef KTYPE_config_cj3(void)
{
	int32_t ret;

	ktype_regs[AD7124_IOCon1].value= KTYPE_IOCON1_KTYPE3;
	ret = ad7124_write_register(&hdev_ktype, ktype_regs[AD7124_IOCon1]);
	if(ret < 0)
		return HAL_ERROR;

	//HAL_Delay(1);

	ret = KTYPE_channel_enable(AD7124_Channel_2);
	if(ret < 0)
		return HAL_ERROR;

	return HAL_OK;
}

HAL_StatusTypeDef KTYPE_config_cj4(void)
{
	int32_t ret;

	ktype_regs[AD7124_IOCon1].value= KTYPE_IOCON1_KTYPE4;
	ret = ad7124_write_register(&hdev_ktype, ktype_regs[AD7124_IOCon1]);
	if(ret < 0)
		return HAL_ERROR;

	//HAL_Delay(1);

	ret = KTYPE_channel_enable(AD7124_Channel_3);
	if(ret < 0)
		return HAL_ERROR;

	return HAL_OK;
}

HAL_StatusTypeDef KTYPE_config_k1(void)
{
	int32_t ret;

	ktype_regs[AD7124_IOCon1].value= KTYPE_IOCON1_KTYPE1;
	ret = ad7124_write_register(&hdev_ktype, ktype_regs[AD7124_IOCon1]);
	if(ret < 0)
		return HAL_ERROR;

	//HAL_Delay(1);

	ret = KTYPE_channel_enable(AD7124_Channel_4);
	if(ret < 0)
		return HAL_ERROR;

	return HAL_OK;
}

HAL_StatusTypeDef KTYPE_config_k2(void)
{
	int32_t ret;

	ktype_regs[AD7124_IOCon1].value= KTYPE_IOCON1_KTYPE2;
	ret = ad7124_write_register(&hdev_ktype, ktype_regs[AD7124_IOCon1]);
	if(ret < 0)
		return HAL_ERROR;

	//HAL_Delay(1);

	ret = KTYPE_channel_enable(AD7124_Channel_5);
	if(ret < 0)
		return HAL_ERROR;

	return HAL_OK;
}

HAL_StatusTypeDef KTYPE_config_k3(void)
{
	int32_t ret;

	ktype_regs[AD7124_IOCon1].value= KTYPE_IOCON1_KTYPE3;
	ret = ad7124_write_register(&hdev_ktype, ktype_regs[AD7124_IOCon1]);
	if(ret < 0)
		return HAL_ERROR;

	//HAL_Delay(1);

	ret = KTYPE_channel_enable(AD7124_Channel_6);
	if(ret < 0)
		return HAL_ERROR;

	return HAL_OK;
}

HAL_StatusTypeDef KTYPE_config_k4(void)
{
	int32_t ret;

	ktype_regs[AD7124_IOCon1].value= KTYPE_IOCON1_KTYPE4;
	ret = ad7124_write_register(&hdev_ktype, ktype_regs[AD7124_IOCon1]);
	if(ret < 0)
		return HAL_ERROR;

	//HAL_Delay(1);

	ret = KTYPE_channel_enable(AD7124_Channel_7);
	if(ret < 0)
		return HAL_ERROR;

	return HAL_OK;
}

HAL_StatusTypeDef KTYPE_start_convertion(void)
{
	int32_t ret;
	
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_ADC_Control]);
	if(ret < 0)
		return HAL_ERROR;
	//printf("\nCTRL %X",ktype_regs[AD7124_ADC_Control].value);
	if(ktype_regs[AD7124_ADC_Control].value != KTYPE_ADC_CTRL_STANDBY)
		return HAL_BUSY;
	
	ktype_regs[AD7124_ADC_Control].value= KTYPE_ADC_CTRL_SINGLE_CONVERT;
	ret = ad7124_write_register(&hdev_ktype, ktype_regs[AD7124_ADC_Control]);
	if(ret < 0)
		return HAL_ERROR;
	
	return HAL_OK;
}

void KTYPE_calc_rtd_temperature(uint8_t ch, float *temp)
 {
     float rRtd;

     rRtd = (R5*(adcValue0[ch] - _2_23))/(_2_23 *GAIN_RTD);

      if(rRtd > R0) {

        *temp = (-COEFF_A + sqrt(COEFF_A_A - COEFF_4B_R0*(R0 - rRtd)))/COEFF_2B;


      } else {

         POLY_CALC(*temp, rRtd/10.0, &cjPolyCoeff[0]);
     }

 }


HAL_StatusTypeDef KTYPE_calc_th_temperature(uint8_t ch, float cjTemp, float *buffer)
{
    float cjVoltage, thVoltage;
    const temp_range *thCoeff;

    thCoeff = &thPolyCoeff[TYPE_K];

    thVoltage = ((VREF_INT*(adcValue1[ch] - _2_23))/(_2_23*GAIN_TH)) + TC_OFFSET_VOLTAGE;

    th_Voltage_read[ch]= thVoltage;

      if(cjTemp < cjTempRange[TYPE_K][1]) {
         POLY_CALC(cjVoltage, cjTemp, thCoeff->neg_temp);
      } else {

        if(cjTemp <= cjTempRange[TYPE_K][2]){

          POLY_CALC(cjVoltage, cjTemp, thCoeff->pos_temp1);
          if(TYPE_K == TYPE_K){
             cjVoltage += COEFF_K_A0*exp(COEFF_K_A1*(cjTemp - COEFF_K_A2)*(cjTemp - COEFF_K_A2));
           }
        } else{
          POLY_CALC(cjVoltage, cjTemp, thCoeff->pos_temp2);
        }
      }
      cj_Voltage[ch] = cjVoltage;

      thVoltage += cjVoltage;

      th_Voltage[ch] = thVoltage;


      if(thVoltage < thVoltageRange[TYPE_K][0]) {
            //errFlag[ch] = ERR_UNDER_RANGE;
            return HAL_ERROR;
      } else{
            if(thVoltage < thVoltageRange[TYPE_K][1]) {
              POLY_CALC(*buffer, thVoltage, thCoeff->neg_voltage);
            } else {
                if(thVoltage <= thVoltageRange[TYPE_K][2]) {
                  POLY_CALC(*buffer, thVoltage, thCoeff->pos_voltage1);
                }else{

                   if((thVoltage <= thVoltageRange[TYPE_K][3]) && (thCoeff->pos_voltage2[0] != 1.0f)) {
                    POLY_CALC(*buffer, thVoltage, thCoeff->pos_voltage2);
                  }else{
                        if(thCoeff->pos_voltage3[0] != 1.0f){
                           if(thVoltage <= thVoltageRange[TYPE_K][4]){
                                 POLY_CALC(*buffer, thVoltage, thCoeff->pos_voltage3);
                           }
                           else{
                                 //errFlag[ch] = ERR_OVER_RANGE;
                                 return HAL_ERROR;
                           }
                        }
                        else{
                              //errFlag[ch] = ERR_OVER_RANGE;
                              return HAL_ERROR;
                        }
                  }
                }
             }
      }


	return HAL_OK;
}

HAL_StatusTypeDef KTYPE_get_temp(int16_t *p_data)
{
	int32_t ret;
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Status]);
	if(ret < 0)
		return HAL_ERROR;
	//printf("\nStatus %X",ktype_regs[AD7124_Status].value);

	int32_t tmp;
	
	ret = ad7124_read_data(&hdev_ktype,&tmp);
	if(ret < 0)
		return HAL_ERROR;

	KTYPE_start_convertion();

	
	
	uint8_t ch = AD7124_STATUS_REG_CH_ACTIVE(ktype_regs[AD7124_Status].value);
	//printf("\nCH%d -",ch);
	
	switch(ch)
	{
		case 0:
			if(ktype_regs[AD7124_Channel_0].value & KTYPE_CHANNEL_ENABLE)
				break;
			else
				return HAL_BUSY;
		case 1:
			if(ktype_regs[AD7124_Channel_1].value & KTYPE_CHANNEL_ENABLE)
				break;
			else
				return HAL_BUSY;
		case 2:
			if(ktype_regs[AD7124_Channel_2].value & KTYPE_CHANNEL_ENABLE)
				break;
			else
				return HAL_BUSY;
		case 3:
			if(ktype_regs[AD7124_Channel_3].value & KTYPE_CHANNEL_ENABLE)
				break;
			else
				return HAL_BUSY;
		case 4:
			if(ktype_regs[AD7124_Channel_4].value & KTYPE_CHANNEL_ENABLE)
				break;
			else
				return HAL_BUSY;
		case 5:
			if(ktype_regs[AD7124_Channel_5].value & KTYPE_CHANNEL_ENABLE)
				break;
			else
				return HAL_BUSY;
		case 6:
			if(ktype_regs[AD7124_Channel_6].value & KTYPE_CHANNEL_ENABLE)
				break;
			else
				return HAL_BUSY;
		case 7:
			if(ktype_regs[AD7124_Channel_7].value & KTYPE_CHANNEL_ENABLE)
				break;
			else
				return HAL_BUSY;
		default :
			return HAL_BUSY;
	}

	if(ch > 3)
	{
		ch -= 4;
				
		adcValue1[ch] = tmp;
		if(KTYPE_calc_th_temperature(ch, 25.0, &temp1[ch]) == HAL_OK)
		{
			printf("%.4fmV-%.4fmV,",th_Voltage_read[ch],th_Voltage[ch]);
			*p_data = (int16_t)(temp1[ch]*10);
		}
		else
		{
			//printf("%X\n",adcValue1[ch]);
			return HAL_TIMEOUT;
		}
	}
	else if(tmp != 0)
	{
		adcValue0[ch] = tmp;
		KTYPE_calc_rtd_temperature(ch,&temp0[ch]);
		printf("cj%d %.4f",ch,temp0[ch]);
		*p_data = (int16_t)(temp0[ch]*10);
	}
	else
			return HAL_TIMEOUT;


	return HAL_OK;
}

HAL_StatusTypeDef KTYPE_read_data(uint8_t *p_update_ch)
{
	int32_t ret;
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Status]);
	if(ret < 0)
		return HAL_ERROR;
	//printf("\nStatus %X",ktype_regs[AD7124_Status].value);

	int32_t tmp;
	
	ret = ad7124_read_data(&hdev_ktype,&tmp);
	if(ret < 0)
		return HAL_ERROR;

	KTYPE_start_convertion();

	
	
	*p_update_ch = AD7124_STATUS_REG_CH_ACTIVE(ktype_regs[AD7124_Status].value);
	printf("\nCH%d -",*p_update_ch);
	
	switch(*p_update_ch)
	{
		case 0:
		case 1:
		case 2:
		case 3:
			if(tmp != 0)
			{
				adcValue0[*p_update_ch] = tmp;
				KTYPE_calc_rtd_temperature(*p_update_ch,&temp0[*p_update_ch]);
				printf("cj %.4f",temp0[*p_update_ch]);
			}
			else
				return HAL_ERROR;
			break;
		case 4:
		case 5:
		case 6:
		case 7:
			{
				uint8_t ch = *p_update_ch - 4;
				
				adcValue1[ch] = tmp;
				if(KTYPE_calc_th_temperature(ch, 0.0, &temp1[ch]) == HAL_OK)
				{
					printf("cj %.4f,%.4fmV-%.4fmV",temp0[ch],th_Voltage_read[ch],th_Voltage[ch]);
					break;
				}
				printf("cj %.4f,%.4fmV-%.4fmV",temp0[ch],th_Voltage_read[ch],th_Voltage[ch]);
			}
		default:
				return HAL_BUSY;
	}

	return HAL_OK;
}

HAL_StatusTypeDef KTYPE_check_cj1(void)
{
	int32_t ret;

	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Channel_0]);
	if(ret < 0)
		return HAL_ERROR;

	if(ktype_regs[AD7124_Channel_0].value & KTYPE_CHANNEL_ENABLE)
		return HAL_OK;

	return HAL_ERROR;
}

HAL_StatusTypeDef KTYPE_check_cj2(void)
{
	int32_t ret;

	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Channel_1]);
	if(ret < 0)
		return HAL_ERROR;

	if(ktype_regs[AD7124_Channel_1].value & KTYPE_CHANNEL_ENABLE)
		return HAL_OK;

	return HAL_ERROR;
}

HAL_StatusTypeDef KTYPE_check_cj3(void)
{
	int32_t ret;

	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Channel_2]);
	if(ret < 0)
		return HAL_ERROR;

	if(ktype_regs[AD7124_Channel_2].value & KTYPE_CHANNEL_ENABLE)
		return HAL_OK;

	return HAL_ERROR;
}

HAL_StatusTypeDef KTYPE_check_cj4(void)
{
	int32_t ret;

	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Channel_3]);
	if(ret < 0)
		return HAL_ERROR;

	if(ktype_regs[AD7124_Channel_3].value & KTYPE_CHANNEL_ENABLE)
		return HAL_OK;

	return HAL_ERROR;
}

HAL_StatusTypeDef KTYPE_check_k1(void)
{
	int32_t ret;

	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Channel_4]);
	if(ret < 0)
		return HAL_ERROR;

	if(ktype_regs[AD7124_Channel_4].value & KTYPE_CHANNEL_ENABLE)
		return HAL_OK;

	return HAL_ERROR;
}

HAL_StatusTypeDef KTYPE_check_k2(void)
{
	int32_t ret;

	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Channel_5]);
	if(ret < 0)
		return HAL_ERROR;

	if(ktype_regs[AD7124_Channel_5].value & KTYPE_CHANNEL_ENABLE)
		return HAL_OK;

	return HAL_ERROR;
}

HAL_StatusTypeDef KTYPE_check_k3(void)
{
	int32_t ret;

	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Channel_6]);
	if(ret < 0)
		return HAL_ERROR;

	if(ktype_regs[AD7124_Channel_6].value & KTYPE_CHANNEL_ENABLE)
		return HAL_OK;

	return HAL_ERROR;
}

HAL_StatusTypeDef KTYPE_check_k4(void)
{
	int32_t ret;

	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Channel_7]);
	if(ret < 0)
		return HAL_ERROR;

	if(ktype_regs[AD7124_Channel_7].value & KTYPE_CHANNEL_ENABLE)
		return HAL_OK;

	return HAL_ERROR;
}

HAL_StatusTypeDef KTYPE_read_config(void)
{
	int32_t ret;

	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Status]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\nktype STATUS %X",ktype_regs[AD7124_Status].value);
	
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_ADC_Control]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\nktype CTRL %X",ktype_regs[AD7124_ADC_Control].value);
	
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_IOCon1]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\nktype IOCon1 %X",ktype_regs[AD7124_IOCon1].value);
	
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_IOCon2]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\nktype IOCon2 %X",ktype_regs[AD7124_IOCon2].value);
	
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Channel_0]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\nktype CH0 %X",ktype_regs[AD7124_Channel_0].value);
	
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Channel_1]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\nktype CH1 %X",ktype_regs[AD7124_Channel_1].value);
	
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Channel_2]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\nktype CH2 %X",ktype_regs[AD7124_Channel_2].value);
	
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Channel_3]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\nktype CH3 %X",ktype_regs[AD7124_Channel_3].value);
	
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Channel_4]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\nktype CH4 %X",ktype_regs[AD7124_Channel_4].value);
	
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Channel_5]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\nktype CH5 %X",ktype_regs[AD7124_Channel_5].value);
	
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Channel_6]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\nktype CH6 %X",ktype_regs[AD7124_Channel_6].value);
	
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Channel_7]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\nktype CH7 %X",ktype_regs[AD7124_Channel_7].value);
	
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Config_0]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\nktype CFG0 %X",ktype_regs[AD7124_Config_0].value);
	
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Config_1]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\nktype CFG1 %X",ktype_regs[AD7124_Config_1].value);
	
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Filter_0]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\nktype F0 %X",ktype_regs[AD7124_Filter_0].value);
	
	ret = ad7124_read_register(&hdev_ktype,&ktype_regs[AD7124_Filter_1]);
	if(ret < 0)
		return HAL_ERROR;
	printf("\nktype F1 %X",ktype_regs[AD7124_Filter_1].value);
	
	return HAL_OK;
}



