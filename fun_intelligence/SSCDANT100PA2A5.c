

/* Includes ------------------------------------------------------------------*/
#include "SSCDANT100PA2A5.h"
#include "main.h"
#include "sensor_i2c.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private constants ---------------------------------------------------------*/
#define SSCDANT100PA2A5_DEV_ADDRESS					0x50

#define SSCDANT100PA2A5_P_HIGH_REG					0x00
#define SSCDANT100PA2A5_P_LOW_REG					0x01
#define SSCDANT100PA2A5_T_HIGH_REG					0x02
#define SSCDANT100PA2A5_T_LOW_REG					0x03

//read/write register size
#define SSCDANT100PA2A5_SIZE_FMR					0
#define SSCDANT100PA2A5_SIZE_PMR					0
#define SSCDANT100PA2A5_SIZE_PRESSURE					2
#define SSCDANT100PA2A5_SIZE_TEMP_8_BIT			3
#define SSCDANT100PA2A5_SIZE_TEMP_11_BIT			4

//data offset of serial read registers from SSCDANT100PA2A5_CMD_REG(0x2E)
#define SSCDANT100PA2A5_INDEX_P_H					0
#define SSCDANT100PA2A5_INDEX_P_L					1
#define SSCDANT100PA2A5_INDEX_T_H					2
#define SSCDANT100PA2A5_INDEX_T_L					3


#define SSCDANT100PA2A5_MASK_STATUS      0xC0
#define SSCDANT100PA2A5_MASK_PRESSURE_H      0x3F

#define SSCDANT100PA2A5_STATUS_NORMAL									0x00
#define SSCDANT100PA2A5_STATUS_CMD												0x40
#define SSCDANT100PA2A5_STATUS_STALE											0x80
#define SSCDANT100PA2A5_STATUS_DIAGNOSTIC					0xC0

#define SSCDANT100PA2A5_OUTPUT_MAX_COEFF			0x3999
#define SSCDANT100PA2A5_OUTPUT_MIN_COEFF			0x0666
#define SSCDANT100PA2A5_P_MAX_COEFF			100  //psi
#define SSCDANT100PA2A5_P_MIN_COEFF			0	//psi
#define SSCDANT100PA2A5_KPA_COEFF			6.894757	//psi to kPa


/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* Variable used to store SSCDANT100PA2A5 pressure, temperature data */
uint8_t ubSSCDANT100PA2A5Data[SSCDANT100PA2A5_SIZE_TEMP_11_BIT];



/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

/**
  * @brief SSCDANT100PA2A5 ADC Initialization Function
  * @param None
  * @retval None
  */
HAL_StatusTypeDef SSCDANT100PA2A5_Init(void)
{
	HAL_StatusTypeDef ret = SSCDANT100PA2A5_start_PMR();

  return ret;
}

HAL_StatusTypeDef SSCDANT100PA2A5_start_FMR(void)
{
	HAL_StatusTypeDef ret = SENSOR_I2C_read(SSCDANT100PA2A5_DEV_ADDRESS,ubSSCDANT100PA2A5Data,SSCDANT100PA2A5_SIZE_FMR);

	printf("SSCDANT FMR %d\n",ret);
	if(ret != HAL_OK)
    return ret;

	HAL_Delay(5);
	
	ret = SENSOR_I2C_read(SSCDANT100PA2A5_DEV_ADDRESS,ubSSCDANT100PA2A5Data,SSCDANT100PA2A5_SIZE_PRESSURE);
	
	printf("SSCDANT %d-%02X,%02X,\n",ret,ubSSCDANT100PA2A5Data[0],ubSSCDANT100PA2A5Data[1]);
  return ret;
}

HAL_StatusTypeDef SSCDANT100PA2A5_start_PMR(void)
{
	HAL_StatusTypeDef ret = SENSOR_I2C_write(SSCDANT100PA2A5_DEV_ADDRESS,ubSSCDANT100PA2A5Data,SSCDANT100PA2A5_SIZE_PMR);
	
	printf("SSCDANT PMR %d\n",ret);
	if(ret != HAL_OK)
    return ret;
	HAL_Delay(3);
	
	ret = SENSOR_I2C_read(SSCDANT100PA2A5_DEV_ADDRESS,ubSSCDANT100PA2A5Data,SSCDANT100PA2A5_SIZE_PRESSURE);
	
	printf("SSCDANT %d-%02X,%02X,\n",ret,ubSSCDANT100PA2A5Data[0],ubSSCDANT100PA2A5Data[1]);
  return ret;
}

HAL_StatusTypeDef SSCDANT100PA2A5_get_pressure(int16_t  * p_pressure)
{
	HAL_StatusTypeDef ret;
	uint32_t tmp;
	float tmp_p;
	
	ret = SENSOR_I2C_read(SSCDANT100PA2A5_DEV_ADDRESS,ubSSCDANT100PA2A5Data,SSCDANT100PA2A5_SIZE_PRESSURE);
	
	//printf("SSCDANT %d-%02X,%02X,\n",ret,ubSSCDANT100PA2A5Data[0],ubSSCDANT100PA2A5Data[1]);

	if(ret != HAL_OK)
    return ret;

	//check status
	if(ubSSCDANT100PA2A5Data[SSCDANT100PA2A5_INDEX_P_H] & SSCDANT100PA2A5_MASK_STATUS)
		return HAL_ERROR;
	
	tmp = ((uint32_t)(ubSSCDANT100PA2A5Data[SSCDANT100PA2A5_INDEX_P_H] & SSCDANT100PA2A5_MASK_PRESSURE_H) << 8) | ubSSCDANT100PA2A5Data[SSCDANT100PA2A5_INDEX_P_L] ;
	//printf("SSCDANT tmp-%X,",tmp);

	tmp = (uint32_t)((tmp - SSCDANT100PA2A5_OUTPUT_MIN_COEFF) *  (SSCDANT100PA2A5_P_MAX_COEFF - SSCDANT100PA2A5_P_MIN_COEFF));

	tmp_p = (float)tmp / (SSCDANT100PA2A5_OUTPUT_MAX_COEFF - SSCDANT100PA2A5_OUTPUT_MIN_COEFF);
	//printf("%X,tmp_p:%04f\n",tmp,tmp_p);

	tmp_p += SSCDANT100PA2A5_P_MIN_COEFF;
	//printf(",%04f psi\n",tmp_p);

	#ifdef SSCDANT100PA2A5_UNIT_M_BAR
	tmp_p = tmp_p * SSCDANT100PA2A5_KPA_COEFF /10;
	//printf("\n%.4f mBar\n",tmp_p);
	#else
	tmp_p *= SSCDANT100PA2A5_KPA_COEFF;
	//printf("\n%.4f kpa\n",tmp_p);
	#endif
	*p_pressure = (int16_t)tmp_p;
	
  return HAL_OK;
}

HAL_StatusTypeDef SSCDANT100PA2A5_get_temperature(int16_t  * p_temperature)
{

  return HAL_OK;
}


/**
  * @brief  Fan feedback update complete callback in non blocking mode 
  * @param pData Pointer to data buffer.
  * @retval None
  */
__weak void SSCDANT100PA2A5_CpltCallback(uint8_t * pData)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(pData);

  /* NOTE : This function should not be modified. When the callback is needed,
            function HAL_ADC_ConvCpltCallback must be implemented in the user file.
   */
}


