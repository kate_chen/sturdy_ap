

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __KTYPE_H
#define __KTYPE_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "stm32f0xx_hal.h"
/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/


/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup KTYPE_Exported_Functions
  * @{
  */

/** @addtogroup KTYPE_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
void KTYPE_Init(void);
/**
  * @}
  */

/** @addtogroup KTYPE_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
HAL_StatusTypeDef KTYPE_config_k1(void);
HAL_StatusTypeDef KTYPE_config_k2(void);
HAL_StatusTypeDef KTYPE_config_k3(void);
HAL_StatusTypeDef KTYPE_config_k4(void);
HAL_StatusTypeDef KTYPE_config_cj1(void);
HAL_StatusTypeDef KTYPE_config_cj2(void);
HAL_StatusTypeDef KTYPE_config_cj3(void);
HAL_StatusTypeDef KTYPE_config_cj4(void);
HAL_StatusTypeDef KTYPE_start_convertion(void);
HAL_StatusTypeDef KTYPE_check_cj1(void);
HAL_StatusTypeDef KTYPE_check_cj2(void);
HAL_StatusTypeDef KTYPE_check_cj3(void);
HAL_StatusTypeDef KTYPE_check_cj4(void);
HAL_StatusTypeDef KTYPE_check_k1(void);
HAL_StatusTypeDef KTYPE_check_k2(void);
HAL_StatusTypeDef KTYPE_check_k3(void);
HAL_StatusTypeDef KTYPE_check_k4(void);
HAL_StatusTypeDef KTYPE_read_config(void);
HAL_StatusTypeDef KTYPE_read_data(uint8_t *p_update_ch);
HAL_StatusTypeDef KTYPE_get_temp(int16_t *p_data);

/**
  * @}
  */

/** @addtogroup KTYPE_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __KTYPE_H */



