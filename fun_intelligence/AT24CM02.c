

/* Includes ------------------------------------------------------------------*/
#include "AT24CM02.h"
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private constants ---------------------------------------------------------*/
//basic AT24CM02 device address
#define AT24CM02_DEV_ADDR_PAGE_0					0xA0
#define AT24CM02_DEV_ADDR_PAGE_256					0xA1
#define AT24CM02_DEV_ADDR_PAGE_512					0xA2
#define AT24CM02_DEV_ADDR_PAGE_768					0xA3


/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef heeprom;
DMA_HandleTypeDef hdma_eeprom_tx;
DMA_HandleTypeDef hdma_eeprom_rx;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

/**
  * @brief AT24CM02_I2C Initialization Function
  * @param None
  * @retval None
  */
void AT24CM02_Init(void)
{
  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  heeprom.Instance = I2C2;
  heeprom.Init.Timing = 0x0010061A;
  heeprom.Init.OwnAddress1 = 0;
  heeprom.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  heeprom.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  heeprom.Init.OwnAddress2 = 0;
  heeprom.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  heeprom.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  heeprom.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&heeprom) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&heeprom, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&heeprom, 0) != HAL_OK)
  {
    Error_Handler();
  }


  /* Check if the device is ready for a new operation */
  while (HAL_I2C_IsDeviceReady(&heeprom, AT24CM02_DEV_ADDR_PAGE_0, 300, 300) == HAL_TIMEOUT);

   /* Wait for the end of the transfer */
  while (HAL_I2C_GetState(&heeprom) != HAL_I2C_STATE_READY)
 	{
 	}
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

HAL_StatusTypeDef AT24CM02_write_page_0(uint16_t MemAddress, uint8_t *pData, uint16_t Size)
{

	if(HAL_I2C_GetState(&heeprom) != HAL_I2C_STATE_READY)
		return HAL_BUSY;

  /* Check if the device is ready for a new operation */
  while (HAL_I2C_IsDeviceReady(&heeprom, AT24CM02_DEV_ADDR_PAGE_0, 300, 300) == HAL_TIMEOUT);


	if(HAL_I2C_Mem_Write_DMA(&heeprom , AT24CM02_DEV_ADDR_PAGE_0, MemAddress, I2C_MEMADD_SIZE_16BIT, pData, Size) != HAL_OK)
	//if(HAL_I2C_Mem_Write(&heeprom , (uint16_t)DevAddress, MemAddress, MemAddSize, pData, Size,1000) != HAL_OK)
		return HAL_ERROR;

	  /* Wait for the end of the transfer */
  while (HAL_I2C_GetState(&heeprom) != HAL_I2C_STATE_READY)
  {
  }
	
	return HAL_OK;
}

HAL_StatusTypeDef AT24CM02_no_check_write_page_0(uint16_t MemAddress, uint8_t *pData, uint16_t Size)
{

	if(HAL_I2C_GetState(&heeprom) != HAL_I2C_STATE_READY)
		return HAL_BUSY;

	if(HAL_I2C_Mem_Write_DMA(&heeprom , AT24CM02_DEV_ADDR_PAGE_0, MemAddress, I2C_MEMADD_SIZE_16BIT, pData, Size) != HAL_OK)
	//if(HAL_I2C_Mem_Write(&heeprom , (uint16_t)DevAddress, MemAddress, MemAddSize, pData, Size,1000) != HAL_OK)
		return HAL_ERROR;
	
	  /* Wait for the end of the transfer */
  while (HAL_I2C_GetState(&heeprom) != HAL_I2C_STATE_READY)
  {
  }

	return HAL_OK;
}

HAL_StatusTypeDef AT24CM02_read_page_0(uint16_t MemAddress, uint8_t *pData, uint16_t Size)
{

	if(HAL_I2C_GetState(&heeprom) != HAL_I2C_STATE_READY)
		return HAL_BUSY;
	
	if(HAL_I2C_Mem_Read_DMA(&heeprom , AT24CM02_DEV_ADDR_PAGE_0, MemAddress, I2C_MEMADD_SIZE_16BIT, pData, Size) != HAL_OK)
		return HAL_ERROR;

	  /* Wait for the end of the transfer */
  while (HAL_I2C_GetState(&heeprom) != HAL_I2C_STATE_READY)
  {
  }

	return HAL_OK;
}

/**
  * @brief  Fan feedback update complete callback in non blocking mode 
  * @param pData Pointer to data buffer.
  * @retval None
  */
__weak void AT24CM02_CpltCallback(uint8_t * pData)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(pData);

  /* NOTE : This function should not be modified. When the callback is needed,
            function HAL_ADC_ConvCpltCallback must be implemented in the user file.
   */
}


