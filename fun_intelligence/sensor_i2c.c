

/* Includes ------------------------------------------------------------------*/
#include "sensor_i2c.h"
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private constants ---------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;
//DMA_HandleTypeDef hdma_i2c1_tx;
//DMA_HandleTypeDef hdma_i2c1_rx;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
void SENSOR_I2C_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x0010061A;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

HAL_StatusTypeDef SENSOR_I2C_check_ready(uint8_t DevAddress, uint32_t Trials, uint32_t Timeout)
{
	if(HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
		return HAL_BUSY;

  /* Check if the device is ready for a new operation */
  while (HAL_I2C_IsDeviceReady(&hi2c1, DevAddress, Trials, Timeout) == HAL_TIMEOUT);

   /* Wait for the end of the transfer */
  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
 	{
 	}
	return HAL_OK;
}

HAL_StatusTypeDef SENSOR_I2C_mem_write(uint8_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size)
{

	if(HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
		return HAL_BUSY;

  /* Check if the device is ready for a new operation */
  while (HAL_I2C_IsDeviceReady(&hi2c1, DevAddress, 300, 300) == HAL_TIMEOUT);

   /* Wait for the end of the transfer */
  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
 	{
 	}

	//if(HAL_I2C_Mem_Write_DMA(&hi2c1 , (uint16_t)DevAddress, MemAddress, MemAddSize, pData, Size) != HAL_OK)
	if(HAL_I2C_Mem_Write(&hi2c1 , (uint16_t)DevAddress, MemAddress, MemAddSize, pData, Size,3000) != HAL_OK)
		return HAL_ERROR;

	  /* Wait for the end of the transfer */
  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
  {
  }
	
	return HAL_OK;
}

HAL_StatusTypeDef SENSOR_I2C_no_check_mem_write(uint8_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size)
{

	if(HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
		return HAL_BUSY;

	//if(HAL_I2C_Mem_Write_DMA(&hi2c1 , (uint16_t)DevAddress, MemAddress, MemAddSize, pData, Size) != HAL_OK)
	if(HAL_I2C_Mem_Write(&hi2c1 , (uint16_t)DevAddress, MemAddress, MemAddSize, pData, Size,3000) != HAL_OK)
		return HAL_ERROR;
	
	  /* Wait for the end of the transfer */
  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
  {
  }

	return HAL_OK;
}

HAL_StatusTypeDef SENSOR_I2C_mem_read(uint8_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t *pData, uint16_t Size)
{

	if(HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
	{
		printf("SENSOR state %d\n",hi2c1.State);
		return HAL_BUSY;
	}

#if 1
	if(MemAddSize == I2C_MEMADD_SIZE_8BIT)
	{
		HAL_StatusTypeDef ret;
		uint8_t mem_addr = (uint8_t)MemAddress;

		ret = HAL_I2C_Master_Transmit(&hi2c1 , (uint16_t)DevAddress, &mem_addr , 1, 3000);
		if(ret != HAL_OK)
		{
			printf("SENSOR E %X-W mem %d\n",hi2c1.ErrorCode,ret);
			return HAL_ERROR;
		}

		ret = HAL_I2C_Master_Receive(&hi2c1 , (uint16_t)DevAddress, pData, Size, 3000);
		if(ret != HAL_OK)
		{
			printf("SENSOR %X-R %d\n",hi2c1.State,ret);
			return HAL_ERROR;
		}
	}
#else
	//HAL_StatusTypeDef ret = HAL_I2C_Mem_Read_DMA(&hi2c1 , (uint16_t)DevAddress, MemAddress, MemAddSize, pData, Size);
	HAL_StatusTypeDef ret = HAL_I2C_Mem_Read(&hi2c1 , (uint16_t)DevAddress, MemAddress, MemAddSize, pData, Size, 10000);
	if(ret != HAL_OK)
	{
		printf("SENSOR %X-read %d\n",hi2c1.State,ret);
		return HAL_ERROR;
	}
#endif
	  /* Wait for the end of the transfer */
  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
  {
  }

	return HAL_OK;
}

HAL_StatusTypeDef SENSOR_I2C_write(uint8_t DevAddress, uint8_t *pData, uint16_t Size)
{

	if(HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
	{
		printf("SENSOR state %d\n",hi2c1.State);
		return HAL_BUSY;
	}
	
#if 1
		HAL_StatusTypeDef ret = HAL_I2C_Master_Transmit(&hi2c1 , (uint16_t)DevAddress, pData, Size, 3000);
		if(ret != HAL_OK)
		{
			printf("SENSOR E %X-W %d\n",hi2c1.ErrorCode,ret);
			return HAL_ERROR;
		}
#else
	//HAL_StatusTypeDef ret = HAL_I2C_Mem_Read_DMA(&hi2c1 , (uint16_t)DevAddress, MemAddress, MemAddSize, pData, Size);
	HAL_StatusTypeDef ret = HAL_I2C_Mem_Read(&hi2c1 , (uint16_t)DevAddress, MemAddress, MemAddSize, pData, Size, 10000);
	if(ret != HAL_OK)
	{
		printf("SENSOR %X-read %d\n",hi2c1.State,ret);
		return HAL_ERROR;
	}
	  /* Wait for the end of the transfer */
  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
  {
  }
#endif

	return HAL_OK;
}

HAL_StatusTypeDef SENSOR_I2C_read(uint8_t DevAddress, uint8_t *pData, uint16_t Size)
{

	if(HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
	{
		printf("SENSOR state %d\n",hi2c1.State);
		return HAL_BUSY;
	}
	
#if 1
		HAL_StatusTypeDef ret = HAL_I2C_Master_Receive(&hi2c1 , (uint16_t)DevAddress, pData, Size, 3000);
		if(ret != HAL_OK)
		{
			printf("SENSOR E %X-R %d\n",hi2c1.ErrorCode,ret);
			return HAL_ERROR;
		}
#else
	//HAL_StatusTypeDef ret = HAL_I2C_Mem_Read_DMA(&hi2c1 , (uint16_t)DevAddress, MemAddress, MemAddSize, pData, Size);
	HAL_StatusTypeDef ret = HAL_I2C_Mem_Read(&hi2c1 , (uint16_t)DevAddress, MemAddress, MemAddSize, pData, Size, 10000);
	if(ret != HAL_OK)
	{
		printf("SENSOR %X-read %d\n",hi2c1.State,ret);
		return HAL_ERROR;
	}
	  /* Wait for the end of the transfer */
  while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
  {
  }
#endif

	return HAL_OK;
}

/**
  * @brief  Fan feedback update complete callback in non blocking mode 
  * @param pData Pointer to data buffer.
  * @retval None
  */
__weak void SENSOR_I2C_FB_UpdateCpltCallback(uint8_t * pData)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(pData);

  /* NOTE : This function should not be modified. When the callback is needed,
            function HAL_ADC_ConvCpltCallback must be implemented in the user file.
   */
}


