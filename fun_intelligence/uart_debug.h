

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __UART_DEBUG_H
#define __UART_DEBUG_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "stm32f0xx_hal.h"
/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/
#ifndef __UART_HMI_H
/* Define when using USART1 as Debug UART,don't define it when using UART_HMI.  */
//#define DEBUG_USART1
#endif

/* Size of Reception buffer */
#define DEBUGBUFFERSIZE                      32

/* Exported macro ------------------------------------------------------------*/
#ifdef __GNUC__
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup UART_DEBUG_Exported_Functions
  * @{
  */

/** @addtogroup UART_DEBUG_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
void UART_DEBUG_Init(void);
/**
  * @}
  */

/** @addtogroup UART_DEBUG_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
void UART_DEBUG_PrintSysTime(void);
void UART_DEBUG_ReceiveCMD(void);

void UART_DEBUG_RxCpltCallback(UART_HandleTypeDef *UartHandle);
void UART_DEBUG_ErrorCallback(UART_HandleTypeDef *UartHandle);
/**
  * @}
  */

/** @addtogroup UART_DEBUG_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __UART_DEBUG_H */

