

/* Includes ------------------------------------------------------------------*/
#include "SM1131.h"
#include "main.h"
#include "sensor_i2c.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/

/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private constants ---------------------------------------------------------*/
#define SM1131_DEV_ADDRESS					0xD8

#define SM1131_CMD_REG					0x22
#define SM1131_TEMP_REG					0x2E
#define SM1131_PRESSURE_REG					0x30
#define SM1131_STATUS_SYNC_REG					0x32
#define SM1131_STATUS_REG					0x36
#define SM1131_SER0_REG					0x50

//SYNC_STATUS register bits
#define SM1131_STATUS_SYNC_REG_IDLE     			 (1 << 0)
#define SM1131_STATUS_SYNC_REG_DSP_S_UP      (1 << 3)
#define SM1131_STATUS_SYNC_REG_DSP_T_UP      (1 << 4)
#define SM1131_STATUS_SYNC_REG_BS_FAIL						(1 << 7)
#define SM1131_STATUS_SYNC_REG_BC_FAIL      		(1 << 8)
#define SM1131_STATUS_SYNC_REG_DSP_SAT		      (1 << 10)
#define SM1131_STATUS_SYNC_REG_CRC_ERR      		(1 << 11)
#define SM1131_STATUS_SYNC_REG_DSP_S_MISS      (1 << 14)
#define SM1131_STATUS_SYNC_REG_DSP_T_MISS      (1 << 15)

//read/write register size
#define SM1131_SIZE_READ_PRESSURE					8
#define SM1131_SIZE_READ_SER_NUM				4
#define SM1131_SIZE_WRITE_STATUS				2

//data index of serial read registers from SM1131_CMD_REG(0x2E)
#define SM1131_INDEX_TEMP_L					0
#define SM1131_INDEX_TEMP_H					1
#define SM1131_INDEX_PRESSURE_L					2
#define SM1131_INDEX_PRESSURE_H					3
#define SM1131_INDEX_STATUS_SYNC_L					4
#define SM1131_INDEX_STATUS_SYNC_H					5
#define SM1131_INDEX_STATUS_L					6
#define SM1131_INDEX_STATUS_H					7

//pressure transform coefficients
#define SM1131_A0_COEFF			8800
#define SM1131_A1_COEFF			220

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* Variable used to store SM1131 temperature, pressure, status data */
uint8_t ubSM1131Data[SM1131_SIZE_READ_PRESSURE];



/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

/**
  * @brief SM1131 ADC Initialization Function
  * @param None
  * @retval None
  */
HAL_StatusTypeDef SM1131_Init(void)
{
	HAL_StatusTypeDef ret;

	ret = SENSOR_I2C_check_ready(SM1131_DEV_ADDRESS,1000,1000);
	if(ret != HAL_OK)
    return ret;

	//read seral number register 0 &1
	ret = SENSOR_I2C_mem_read(SM1131_DEV_ADDRESS,SM1131_SER0_REG,I2C_MEMADD_SIZE_8BIT,ubSM1131Data,SM1131_SIZE_READ_SER_NUM);
	
	printf("\nSM1131 SER-%02X%02X,%02X%02X\n",ubSM1131Data[3],ubSM1131Data[2],ubSM1131Data[1],ubSM1131Data[0]);
	if(ret != HAL_OK)
    return ret;

	//clear status register event type value
	ubSM1131Data[0] = 0xFF;
	ubSM1131Data[1] = 0xFF;
	ret = SENSOR_I2C_mem_write(SM1131_DEV_ADDRESS,SM1131_STATUS_REG,I2C_MEMADD_SIZE_8BIT,ubSM1131Data,SM1131_SIZE_WRITE_STATUS);
	
  return ret;
}

HAL_StatusTypeDef SM1131_read_data(void)
{
	HAL_StatusTypeDef ret;
	
	ret = SENSOR_I2C_mem_read(SM1131_DEV_ADDRESS,SM1131_TEMP_REG,I2C_MEMADD_SIZE_8BIT,ubSM1131Data,SM1131_SIZE_READ_PRESSURE);
	
	printf("SM1131 %d-%02X,%02X,%02X,%02X\n",ret,ubSM1131Data[2],ubSM1131Data[3],ubSM1131Data[4],ubSM1131Data[5]);
  return ret;
}

HAL_StatusTypeDef SM1131_get_pressure(int16_t  * p_pressure)
{
	HAL_StatusTypeDef ret;
	uint32_t tmp;
	
	ret = SENSOR_I2C_mem_read(SM1131_DEV_ADDRESS,SM1131_TEMP_REG,I2C_MEMADD_SIZE_8BIT,ubSM1131Data,SM1131_SIZE_READ_PRESSURE);
	
	//printf("SM1131 %d-%02X%02X/%02X%02X\n",ret,ubSM1131Data[5],ubSM1131Data[4],ubSM1131Data[7],ubSM1131Data[6]);

	if(ret != HAL_OK)
    return ret;
	
	//check sync status register
	tmp = ((uint32_t)ubSM1131Data[SM1131_INDEX_STATUS_SYNC_H] << 8) | ubSM1131Data[SM1131_INDEX_STATUS_SYNC_L];
	
	if(tmp & SM1131_STATUS_SYNC_REG_DSP_S_UP)
	{
		tmp = ((uint32_t)ubSM1131Data[SM1131_INDEX_PRESSURE_H] << 8) | ubSM1131Data[SM1131_INDEX_PRESSURE_L];
		
		//printf("SM1131 P %02X,%02X/%X\n",ubSM1131Data[2],ubSM1131Data[3],tmp);

		#ifdef SM1131_UNIT_M_BAR
		tmp = (tmp + SM1131_A0_COEFF) / SM1131_A1_COEFF /10;
		//printf("SM1131 %d mBar\n",tmp);
		#else
		tmp = (tmp + SM1131_A0_COEFF) / SM1131_A1_COEFF;
		//printf("SM1131 %d kPa\n",tmp);
		#endif
		*p_pressure = (int16_t)tmp;
	}
	else
	{
		printf("SM1131 NA\n");
	  return HAL_TIMEOUT;
	}
	
  return HAL_OK;
}


/**
  * @brief  Fan feedback update complete callback in non blocking mode 
  * @param pData Pointer to data buffer.
  * @retval None
  */
__weak void SM1131_CpltCallback(uint8_t * pData)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(pData);

  /* NOTE : This function should not be modified. When the callback is needed,
            function HAL_ADC_ConvCpltCallback must be implemented in the user file.
   */
}


