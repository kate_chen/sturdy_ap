

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __UART_HMI_H
#define __UART_HMI_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "stm32f0xx_hal.h"
/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/
//for HMI opcode decode
typedef enum
{
	HMI_OP_INFO_UNWRAPPED_121 = 0x00,
	HMI_OP_INFO_UNWRAPPED_134 ,
	HMI_OP_INFO_WRAPPED_121 ,
	HMI_OP_INFO_WRAPPED_134 ,
	HMI_OP_INFO_INSTRUMENT_121 ,
	HMI_OP_INFO_INSTRUMENT_134 ,
	HMI_OP_INFO_PRION ,
	HMI_OP_INFO_CUMTOM ,
	HMI_OP_INFO_FLASH ,
	HMI_OP_INFO_DRY ,

	HMI_OP_START = 0x1F ,
	HMI_OP_NOTIFY_STEPS ,
	HMI_OP_CONFIG_CUMTOM ,
	HMI_OP_INFO_LEAKAGE_TEST ,
	HMI_OP_INFO_HELIX_TEST ,
	HMI_OP_INFO_B_D_TEST ,

	HMI_OP_NOTIFY_STER_STEP = 0x30 ,
	HMI_OP_NOTIFY_STER_LOG ,
	HMI_OP_NOTIFY_STER_OK ,
	HMI_OP_NOTIFY_STER_ERR ,
	HMI_OP_NOTIFY_STER_FAIL ,
	HMI_OP_NOTIFY_LEAKAGE_STEP ,
	HMI_OP_NOTIFY_LEAKAGE_LOG ,
	HMI_OP_NOTIFY_LEAKAGE_OK ,
	HMI_OP_NOTIFY_LEAKAGE_FAIL ,
	
	HMI_OP_CONFIG_AUTO_ADD_W = 0x40 ,
	HMI_OP_CALIB_SENSOR ,
	HMI_OP_CALIB_ALTITUDER ,
	HMI_OP_ERR_WQ ,

	HMI_OP_STOP_EMERGENCY = 0x50 ,
	HMI_OP_ERR_002 ,
	HMI_OP_ERR_003 ,
	HMI_OP_NOTIFY_ERR ,
	HMI_OP_GET_ALL_ERR ,

	HMI_OP_OPEN_DOOR = 0x60 ,

	#ifdef USE_PRESCAN
	HMI_OP_PRESCAN = 0xD0,
	HMI_OP_DO01,
	HMI_OP_DO02,
	HMI_OP_DO03,
	HMI_OP_DO04,
	HMI_OP_DO05,
	HMI_OP_DO06,
	HMI_OP_DO07,
	HMI_OP_DO08,
	HMI_OP_DO09,
	HMI_OP_DO10,
	HMI_OP_DO11,
	HMI_OP_DO12,
	HMI_OP_DO13,
	HMI_OP_DO14,
	HMI_OP_DO15,
	HMI_OP_DO16,
	HMI_OP_RO01,
	HMI_OP_RO02,
	HMI_OP_RO03,
	HMI_OP_RO04,
	HMI_OP_RO05,
	HMI_OP_DO17,
	HMI_OP_ALL_ON,
	HMI_OP_ALL_OFF,
	#endif
	
	HMI_OP_INFO_INITIAL = 0xF0,
	HMI_OP_INFO_CB ,
	HMI_OP_INFO_UNLOCK ,
	HMI_OP_INFO_SYSTEM ,
}hmi_opcode_t;


/* Exported constants --------------------------------------------------------*/
//for HMI check if command has valid data size
#define HMI_SIZE_CMD_STOP_EMERGENCY	 			0x01
#define HMI_SIZE_CMD_OPEN_DOOR 			0x01
#define HMI_SIZE_CMD_START	 			0x02
#define HMI_SIZE_CMD_START_DATA	 			0x08
#define HMI_SIZE_NOTI_STEPS	 			0x15
#define HMI_SIZE_NOTI_STER_STEP	 			0x15
#define HMI_SIZE_NOTI_STER_LOG	 			0x11
#define HMI_SIZE_NOTI_STER_OK	 					0x05
#define HMI_SIZE_NOTI_STER_FAIL	 			0x05

#define HMI_SIZE_CMD_PRESCAN	 0x02
#define HMI_SIZE_CFG_PRESCAN	 0x01

#define HMI_DATA_NACK	 			0x00
#define HMI_DATA_ACK	 			0x01

#define HMI_DATA_OFF	 			0x00
#define HMI_DATA_ON	 			0x01
#define HMI_DATA_NONE	 			0x02

#define HMI_DATA_LOW	 			0x01
#define HMI_DATA_MIDDLE	 			0x01
#define HMI_DATA_HIGH	 			0x02


/* Size of Reception buffer */
#define HMIBUFFERSIZE                      256

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup UART_HMI_Exported_Functions
  * @{
  */

/** @addtogroup UART_HMI_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
void UART_HMI_Init(void);
/**
  * @}
  */

/** @addtogroup UART_HMI_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
void UART_HMI_TransmitData(void);
void UART_HMI_ReceiveCMD(void);
HAL_StatusTypeDef UART_HMI_send_one_data(uint8_t op_code, uint8_t data);
void UART_HMI_start_data(uint8_t sterlize_ind, uint8_t total_step, uint8_t err_031, uint8_t err_101, uint8_t err_102, uint8_t err_200, uint8_t err_600);
void UART_HMI_all_steps(uint8_t *p_steps);

void UART_HMI_sterlize_step(uint8_t step_ind, uint8_t *p_name, uint32_t countdown, uint32_t time, int16_t temp, int16_t pressure);
void UART_HMI_sterlize_log(uint8_t step_ind, uint8_t *p_name, uint32_t time, int16_t temp, int16_t pressure);
void UART_HMI_sterlize_ok(int16_t temp, int16_t pressure);
void UART_HMI_sterlize_fail(int16_t temp, int16_t pressure);

void UART_HMI_TxCpltCallback(UART_HandleTypeDef *UartHandle);
void UART_HMI_RxCpltCallback(UART_HandleTypeDef *UartHandle);
void UART_HMI_ErrorCallback(UART_HandleTypeDef *UartHandle);
/**
  * @}
  */

/** @addtogroup UART_HMI_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __UART_HMI_H */

