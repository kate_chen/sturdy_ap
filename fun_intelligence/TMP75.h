

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TMP75_H
#define __TMP75_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup TMP75_Exported_Functions
  * @{
  */

/** @addtogroup TMP75_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
HAL_StatusTypeDef TMP75_Init(void);
/**
  * @}
  */

/** @addtogroup TMP75_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
HAL_StatusTypeDef TMP75_read_config(void);
HAL_StatusTypeDef TMP75_config_resolution(uint8_t resolution);
HAL_StatusTypeDef TMP75_read_data(void);
HAL_StatusTypeDef TMP75_get_temperature(int16_t  * p_temperature);
/**
  * @}
  */

/** @addtogroup TMP75_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __TMP75_H */



