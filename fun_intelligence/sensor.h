

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SENSOR_H
#define __SENSOR_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/

/* Private constants ---------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup SENSOR_Exported_Functions
  * @{
  */

/** @addtogroup SENSOR_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
void SENSOR_Init(void);
/**
  * @}
  */

/** @addtogroup SENSOR_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
void SENSOR_update(void);
void SENSOR_PeriodElapsedCallback(TIM_HandleTypeDef *htim);
/**
  * @}
  */

/** @addtogroup SENSOR_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __SENSOR_H */



