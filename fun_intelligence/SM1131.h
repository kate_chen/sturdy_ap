

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SM1131_H
#define __SM1131_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
#define SM1131_UNIT_M_BAR	

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup SM1131_Exported_Functions
  * @{
  */

/** @addtogroup SM1131_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
HAL_StatusTypeDef SM1131_Init(void);
/**
  * @}
  */

/** @addtogroup SM1131_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
HAL_StatusTypeDef SM1131_read_data(void);
HAL_StatusTypeDef SM1131_get_pressure(int16_t  * p_pressure);
/**
  * @}
  */

/** @addtogroup SM1131_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __SM1131_H */



