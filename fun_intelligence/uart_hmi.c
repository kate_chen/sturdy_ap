
/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include "uart_hmi.h"
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* UART handler declaration */
UART_HandleTypeDef huart_hmi;
DMA_HandleTypeDef hdma_hmi_tx;

/* Buffer used for reception */
uint8_t aRXBufferA_HMI[HMIBUFFERSIZE];
uint8_t aRXBufferB_HMI[HMIBUFFERSIZE];
uint8_t aTXBuffer_HMI[HMIBUFFERSIZE];

__IO uint32_t uwNbTransmitCharsHMI = 0;
__IO uint32_t uwNbReceivedCharsHMI = 0;
__IO uint32_t uwBufferReadyIndicationHMI = 0;
uint8_t *pBufferReadyForCB;
uint8_t *pBufferReadyForHMI;

__IO uint32_t uwErrorHMI = HAL_OK;
__IO uint32_t uwReceiveErrorHMI = HAL_OK;
__IO uint32_t uwTransmitErrorHMI = HAL_OK;
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

/**
  * @brief UART_HMI print system time Function
  * @param p_log pointer of log to print
  * @retval None
  */
void UART_HMI_TransmitData(void)
{
	uwTransmitErrorHMI = HAL_UART_Transmit_DMA(&huart_hmi, aTXBuffer_HMI, uwNbTransmitCharsHMI);
}

/**
  * @brief UART_HMI start to receive command Function
  * @param p_log pointer of log to print
  * @retval None
  */
void UART_HMI_ReceiveCMD(void)
{
  uwReceiveErrorHMI = HAL_UART_Receive_IT(&huart_hmi,pBufferReadyForHMI,1);
}

/**
  * @brief UART_HMI Initialization Function
  * @param None
  * @retval None
  */
void UART_HMI_Init(void)
{
  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart_hmi.Instance = USART1;
  huart_hmi.Init.BaudRate = 115200;
  huart_hmi.Init.WordLength = UART_WORDLENGTH_8B;
  huart_hmi.Init.StopBits = UART_STOPBITS_1;
  huart_hmi.Init.Parity = UART_PARITY_NONE;
  huart_hmi.Init.Mode = UART_MODE_TX_RX;
  huart_hmi.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart_hmi.Init.OverSampling = UART_OVERSAMPLING_16;
  huart_hmi.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart_hmi.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_RXOVERRUNDISABLE_INIT|UART_ADVFEATURE_DMADISABLEONERROR_INIT;
  huart_hmi.AdvancedInit.OverrunDisable = UART_ADVFEATURE_OVERRUN_DISABLE;
  huart_hmi.AdvancedInit.DMADisableonRxError = UART_ADVFEATURE_DMA_DISABLEONRXERROR;
  if (HAL_UART_Init(&huart_hmi) != HAL_OK)
  {
    Error_Handler();
  }

  /* Initializes Buffer swap mechanism :
     2 physical buffers aRXBufferA_HMI and aRXBufferB_HMI (RX_BUFFER_SIZE length)
     Any data received will be stored in active buffer : the number max of 
     data received is RX_BUFFER_SIZE */
  pBufferReadyForHMI = aRXBufferA_HMI;
  pBufferReadyForCB      = aRXBufferB_HMI;
  uwNbReceivedCharsHMI = 0;
  uwBufferReadyIndicationHMI = 0;

  if(HAL_UART_Receive_IT(&huart_hmi,pBufferReadyForHMI,1) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */
}

HAL_StatusTypeDef UART_HMI_send_one_data(uint8_t op_code, uint8_t data)
{
	if(uwNbTransmitCharsHMI)
		return HAL_BUSY;
	aTXBuffer_HMI[1] = op_code;
	aTXBuffer_HMI[2] = data;
	aTXBuffer_HMI[0] = 0x02;
	uwNbTransmitCharsHMI = 0x03;
	UART_HMI_TransmitData();
	
	return HAL_OK;
}

void UART_HMI_start_data(uint8_t sterlize_ind, uint8_t total_step, uint8_t err_031, uint8_t err_101, uint8_t err_102, uint8_t err_200, uint8_t err_600)
{
	if(uwNbTransmitCharsHMI)
		return;
	
	aTXBuffer_HMI[1] = HMI_OP_START;
	aTXBuffer_HMI[2] = sterlize_ind;
	aTXBuffer_HMI[3] = total_step;
	aTXBuffer_HMI[4] = err_031;
	aTXBuffer_HMI[5] = err_101;
	aTXBuffer_HMI[6] = err_102;
	aTXBuffer_HMI[7] = err_200;
	aTXBuffer_HMI[8] = err_600;
	aTXBuffer_HMI[0] = HMI_SIZE_CMD_START_DATA;
	uwNbTransmitCharsHMI = HMI_SIZE_CMD_START_DATA+1;
	
	UART_HMI_TransmitData();
}

void UART_HMI_all_steps(uint8_t *p_steps)
{
	if(uwNbTransmitCharsHMI)
		return;
	
	aTXBuffer_HMI[1] = HMI_OP_NOTIFY_STEPS;
	memcpy(&aTXBuffer_HMI[2],p_steps,20);
	aTXBuffer_HMI[0] = HMI_SIZE_NOTI_STEPS;
	uwNbTransmitCharsHMI = HMI_SIZE_NOTI_STEPS+1;
	
	UART_HMI_TransmitData();
}

void UART_HMI_sterlize_step(uint8_t step_ind, uint8_t *p_name, uint32_t countdown, uint32_t time, int16_t temp, int16_t pressure)
{
	if(uwNbTransmitCharsHMI)
		return;
	
	aTXBuffer_HMI[1] = HMI_OP_NOTIFY_STER_STEP;
	aTXBuffer_HMI[2] = step_ind;
	memcpy(&aTXBuffer_HMI[3],p_name,7);
	memcpy(&aTXBuffer_HMI[10],&countdown,4);
	memcpy(&aTXBuffer_HMI[14],&time,4);
	memcpy(&aTXBuffer_HMI[18],&temp,2);
	memcpy(&aTXBuffer_HMI[20],&pressure,2);
	aTXBuffer_HMI[0] = HMI_SIZE_NOTI_STER_STEP;
	uwNbTransmitCharsHMI = HMI_SIZE_NOTI_STER_STEP+1;
	
	UART_HMI_TransmitData();
}

void UART_HMI_sterlize_log(uint8_t step_ind, uint8_t *p_name, uint32_t time, int16_t temp, int16_t pressure)
{
	if(uwNbTransmitCharsHMI)
		return;
	
	aTXBuffer_HMI[1] = HMI_OP_NOTIFY_STER_LOG;
	aTXBuffer_HMI[2] = step_ind;
	memcpy(&aTXBuffer_HMI[3],p_name,7);
	memcpy(&aTXBuffer_HMI[10],&time,4);
	memcpy(&aTXBuffer_HMI[14],&temp,2);
	memcpy(&aTXBuffer_HMI[16],&pressure,2);
	aTXBuffer_HMI[0] = HMI_SIZE_NOTI_STER_LOG;
	uwNbTransmitCharsHMI = HMI_SIZE_NOTI_STER_LOG+1;
	
	UART_HMI_TransmitData();
}

void UART_HMI_sterlize_ok(int16_t temp, int16_t pressure)
{
	if(uwNbTransmitCharsHMI)
		return;
	
	aTXBuffer_HMI[1] = HMI_OP_NOTIFY_STER_OK;
	memcpy(&aTXBuffer_HMI[2],&temp,2);
	memcpy(&aTXBuffer_HMI[4],&pressure,2);
	aTXBuffer_HMI[0] = HMI_SIZE_NOTI_STER_OK;
	uwNbTransmitCharsHMI = HMI_SIZE_NOTI_STER_OK+1;
	
	UART_HMI_TransmitData();
}

void UART_HMI_sterlize_fail(int16_t temp, int16_t pressure)
{
	if(uwNbTransmitCharsHMI)
		return;
	
	aTXBuffer_HMI[1] = HMI_OP_NOTIFY_STER_FAIL;
	memcpy(&aTXBuffer_HMI[2],&temp,2);
	memcpy(&aTXBuffer_HMI[4],&pressure,2);
	aTXBuffer_HMI[0] = HMI_SIZE_NOTI_STER_FAIL;
	uwNbTransmitCharsHMI = HMI_SIZE_NOTI_STER_FAIL+1;
	
	UART_HMI_TransmitData();
}


/**
  * @brief  Tx Transfer completed callback
  * @param  UartHandle: UART handle. 
  * @note   This example shows a simple way to report end of DMA Tx transfer, and 
  *         you can add your own implementation. 
  * @retval None
  */
void UART_HMI_TxCpltCallback(UART_HandleTypeDef *UartHandle)
{
	if(UartHandle->Instance != huart_hmi.Instance)
		return;
	uwNbTransmitCharsHMI = 0;
}

/**
  * @brief  Rx Transfer completed callback
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report end of DMA Rx transfer, and 
  *         you can add your own implementation.
  * @retval None
  */
void UART_HMI_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{

	if(UartHandle->Instance != huart_hmi.Instance)
		return;

  uint8_t ReceiveData = pBufferReadyForHMI[uwNbReceivedCharsHMI++];
  
  //check if debug command is ready
  if ((uwNbReceivedCharsHMI == HMIBUFFERSIZE) || (uwNbReceivedCharsHMI > pBufferReadyForHMI[0]))
  {
    uint8_t *ptemp;
    
    //ReadyBuffer is processed & have valid data to process. 
    if(pBufferReadyForHMI[0])
    {
      /* Swap buffers for next bytes to be received */
      ptemp = pBufferReadyForCB;
      pBufferReadyForCB = pBufferReadyForHMI;
      pBufferReadyForHMI = ptemp;

      /* Set Buffer swap indication */
      uwBufferReadyIndicationHMI = uwNbReceivedCharsHMI;
      uwNbReceivedCharsHMI = 0;
    }
    //Discard this command.
    else
    {
      uwNbReceivedCharsHMI = 0;
			UART_HMI_ReceiveCMD();
    }
  }
  //Receive next byte.
  else
    uwReceiveErrorHMI = HAL_UART_Receive_IT(&huart_hmi,&pBufferReadyForHMI[uwNbReceivedCharsHMI],1);
  
  //TX send back received data.
  //HAL_UART_Transmit(&huart_hmi, (uint8_t *)&ReceiveData, 1, 5); 
}

/**
  * @brief  UART error callbacks
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report transfer error, and you can
  *         add your own implementation.
  * @retval None
  */
void UART_HMI_ErrorCallback(UART_HandleTypeDef *UartHandle)
{
	if(UartHandle->Instance != huart_hmi.Instance)
		return;
  /* Transfer error in reception/transmission process */
  if(UartHandle->ErrorCode)
  {
    uwErrorHMI=UartHandle->ErrorCode;
    Error_Handler();
  }
	
}

