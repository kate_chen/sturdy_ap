

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TDS_H
#define __TDS_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include "stm32f0xx_hal.h"
/* Private includes ----------------------------------------------------------*/
/* Private constants ---------------------------------------------------------*/
#define TDS_UNIT_US_CM				


/* Size of Conversion buffer */
#define TDS_CONVERTED_BUFFER_SIZE 		10

/* Analog reference voltage(Volt) of the ADC */
#define TDS_VREF 5.0 

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup TDS_Exported_Functions
  * @{
  */

/** @addtogroup TDS_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
void TDS_Init(void);
/**
  * @}
  */

/** @addtogroup TDS_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
HAL_StatusTypeDef TDS_ConvStart(void);
float getTDSValue(void);
void TDS_ConvCpltCallback(uint16_t* pData);
/**
  * @}
  */

/** @addtogroup TDS_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __TDS_H */



