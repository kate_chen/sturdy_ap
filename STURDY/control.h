


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CONTROL_H
#define __CONTROL_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/
typedef struct {
 unsigned CtrlItem;
 unsigned WaitItem;
 int PT; // 溫度或壓力
 int time; //時間
 int time0; //預估的動作時間(秒)[2011/10/17] 新增
 int time1; //20120808 JASON KeepTime,壓力到達目標值,持續動作時間
} PROGRAM_DATA, *pPROGRAM_DATA;

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup CONTROL_Exported_Functions
  * @{
  */

/** @addtogroup CONTROL_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
/**
  * @}
  */

/** @addtogroup CONTROL_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
void StartProgram_Active(void);
void StartProgram_Stop(void);
void StartProgram_Loop(void);
void Pause_Start(int temperature_10,unsigned tt);
void Pause_Stop(void);
void Pause_Loop(void);
void IO_Control(void);
void ResetHeaterParam(void);
void ResetVacuumParam(void);
void ResetSteamParam(void);
void ResetSterilizeParam(void);
void ResetExhaustParam(void);
void ResetVacuumReleaseParam(void);
void ResetDryParam(void);
int ConfigProgram(void);
void RestoreGotoNextStep(void);
void GotoNextProgramStep(void);
void TEST_GotoNextStep(void);
void ResetProgramStatus(void);
int RunProgram(pPROGRAM_DATA *pProgram,int no);
int StopProgram(pPROGRAM_DATA *pProgram);
void WaitToNextControlStep(void);
void Leakage_Start(int pressure);
void Leakage_Stop(void);
void Leakage_Loop(void);
void CheckServiceTimes(void);
void CheckInnerTemperature(int idx);
void Check_ProgramTimePeriod(void);
void Check_bRunProgram(void);
int Check_R1(void);
int Check_SSR1(void);
int Check_SSR2(void);
int Check_Key(void);
void CheckRoomTemp(void);
void CheckAirPressure(void);
int Check_ForPowerOn(void);
//static void near ResetPressureRecord(int Pressure);
static void ResetPressureRecord(int Pressure);
//static int near Get_dPressure(int pressure);
static int Get_dPressure(int pressure);
void Check_IO_Status(void);
void Cmd_CheckErrorMode(void);
/**
  * @}
  */

/** @addtogroup CONTROL_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __CONTROL_H */


