

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __COMMAND_H
#define __COMMAND_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/
typedef struct {
char *msg;
int type;
void *data;
int max_size;
} INPUT;

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup COMMAND_Exported_Functions
  * @{
  */

/** @addtogroup COMMAND_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
/**
  * @}
  */

/** @addtogroup COMMAND_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
void InputShowMsg(void);
void InputShowAllMsg(INPUT *pInput,int n);
void ParamInput(int key);
void StartParamInput(INPUT *pInput,int n);
void Cmd_Ver(void);
void Cmd_Load(void);
void Cmd_Input(void);
void Cmd_Output(void);
void Cmd_DI(void);
void Cmd_DO1(void);
void Cmd_DO2(void);
void Cmd_DO3(void);
void Cmd_DO(void);
void Cmd_RO(void);
void Cmd_Door(void);
void Cmd_Water(void);
void Cmd_Heater(void);
void Cmd_Vacuum(void);
void Cmd_Vacuum_New(void);
void Cmd_ExhaustPressure(void);
void Cmd_ExhaustPressure1Shot(void);
void Cmd_SetExhaust(void);
void Cmd_SteamFixed(void);
void Cmd_PlaySound(void);
void Cmd_Steam(void);
void Cmd_Sterilize(void);
void Cmd_Dry(void);
void Cmd_AddSteam1Shot(void);
void Cmd_Pause(void);
void Cmd_VacuumRelease(void);
void Cmd_LeakageTest(void);
void Cmd_Test(void);
void ShowTc1Table(void);
void ShowTc2Table(void);
void TableInput(int key);
void Cmd_Tc1Table(void);
void Cmd_Tc2Table(void);
void ShowTc3Table(void);
void Cmd_Tc3Table(void);
void ShowPt1Table(void);
void Cmd_Pt1Table(void);
void ShowPt2Table(void);
void Cmd_Pt2Table(void);
void ShowPressureTable(void);
void Cmd_PressureTable(void);
void ShowAirTable(void);
void Cmd_AirTable(void);
//static void near ShowTC1Offset(void);
static void ShowTC1Offset(void);
void CalibrateTc1(int CaliTemperature);
void Cmd_CaliTc1(void);
void Cmd_Tc1Offset(void);
//static void near ShowTC2Offset(void);
static void ShowTC2Offset(void);
void CalibrateTc2(int CaliTemperature);
void Cmd_CaliTc2(void);
void Cmd_Tc2Offset(void);
void Cmd_Tc2Offset(void);
void CalibrateTc3(int CaliTemperature);
void Cmd_CaliTc3(void);
void Cmd_Tc3Offset(void);
//static void near ShowPT1Offset(void);
static void ShowPT1Offset(void);
void CalibratePt1(int CaliTemperature);
void Cmd_CaliPt1(void);
void Cmd_Pt1Offset(void);
//static void near ShowPT2Offset(void);
static void ShowPT2Offset(void);
void CalibratePt2(int CaliTemperature);
void Cmd_CaliPt2(void);
void Cmd_Pt2Offset(void);
//static void near ShowAirOffset(void);
static void ShowAirOffset(void);
void CalibrateAir(int CaliPressure);
void Cmd_CaliAir(void);
void CalibrateP1(int newP1);
void Cmd_PressureOffset(void);
void Cmd_ScanAi(void);
void Cmd_Start(void);
void Cmd_Auto(void);
void Cmd_Program(void);
void Cmd_StopProgram(void);
void Cmd_Led(void);
void Cmd_CJC(void);
void Cmd_AD(void);
void Cmd_Time(void);
static int IsLeapYear(int year);
void Cmd_Date(void);
void Cmd_Printer(void);
void Cmd_PowerOn(void);
void Cmd_Display(void);
void Cmd_NextStep(void);
void Cmd_Quit(void);
void Cmd_Ip(void);
void Cmd_Mask(void);
void Cmd_Gateway(void);
void Cmd_Table(void);
void Cmd_Arp(void);
void DoTimer(void);
int TestIO_1(int mode);
void DoTimerFunction(void);
void Cmd_SD(void);
void Cmd_Counter(void);
void Cmd_SerialNumber(void); 
void Cmd_ErrorCode(void); 
void Cmd_Language(void);
void DoCommand(char *Command);
void DoTcpCommand(int skt,char *Command);
void CommandInput(int key);
void DoHmiCommand(uint8_t *Command);
/**
  * @}
  */

/** @addtogroup COMMAND_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __COMMAND_H */
