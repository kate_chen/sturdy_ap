
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "ai.h"
#include "uart_debug.h"


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

int Read_AI_Hex(void)
{
	return 0;
}

int Read_AI_Hex_C(void)
{
	return 0;
}

static unsigned _ReadAi_10(void)
{
	return 0;
}

static unsigned _ReadAi_100(void)
{
	return 0;
}

void SetAiChannel(unsigned channel)
{
}

void ResetAiChannel(void)
{
}

unsigned _GetAI(int idx)
{
	return 0;
}

void StartReadAI(void)
{
}

void _ReadAI(void)
{
}

int GetTc1Temperature(void)
{
	return 0;
}

int GetTc2Temperature(void)
{
	return 0;
}

int GetTc3Temperature(void)
{
	return 0;
}

int GetPT1Temperature(void)
{
	return 0;
}

int GetPT2Temperature(void)
{
	return 0;
}

void PT_AI_Reset(void)
{
}

int GetRelativePressure(void)
{
	return 0;
}

int GetRelativePressure_10(void)
{
	return 0;
}

static void CjcHLH(void)
{
}

static void CjcLHL(void)
{
}

void SetCjcAverageNo(int no)
{
}

static int Tx100Averag(int newTx100)
{
	return 0;
}

void GetCjc(void)
{
}

long CALC_CJC_V1_mV(int cjc100)
{
	return 0;
}

long CALC_CJC_V2_mV(int cjc100)
{
	return 0;
}

long CALC_CJC_V3_mV(int cjc100)
{
	return 0;
}

int CvtmV2T(int mV)
{
	return 0;
}

int CvtmR2P(int mV)
{
	return 0;
}

int GetTempTC1(int ad,int cjc)
{
	return 0;
}

int GetTempTC2(int ad,int cjc)
{
	return 0;
}

int GetTempTC3(int ad,int cjc)
{
	return 0;
}

int GetTempPT1(int ad,int cjc)
{
	return 0;
}

int GetTempPT2(int ad,int cjc)
{
	return 0;
}

int GetAirPressure(int ad)
{
	return 0;
}

void TestTC2(void)
{
}




