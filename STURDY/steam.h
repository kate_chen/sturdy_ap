


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STEAM_H
#define __STEAM_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup STEAM_Exported_Functions
  * @{
  */

/** @addtogroup STEAM_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
/**
  * @}
  */

/** @addtogroup STEAM_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
void AddSteamOneShot_Start(void);
void AddSteamOneShot_Stop_1(void);
void AddSteamOneShot_Stop_0(void);  
void AddSteamOneShot_Loop(void);
void Steam_Start(int temperature);
void Steam_Stop(void); 
void ResetCurrentTemperatureRec(void);
void SaveCurrentInnerTemperature(void);
int GetdT(unsigned dt);
void Steam_Loop(void);
void Sterilize_Start(int Temperature,unsigned ttt);
void Sterilize_Stop(void);
void Sterilize_Loop(void);
void Temperature_balance_Start(int TTTA,unsigned ttt);
void Temperature_balance_Stop(void);
void Temperature_balance_Loop(void);
void Test_Stop(void);
void Test_Start(int targetT_10,unsigned t1 ,unsigned period);
void Test_Loop(void);
/**
  * @}
  */

/** @addtogroup STEAM_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __STEAM_H */




