
/* Includes ------------------------------------------------------------------*/
//#include "main.h"
#include "uart_debug.h"
#include "door.h"
#include "Prime.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

static int DoorMode=0;
void UnlockDoor_Start(void)
{
	// 啟動開門程序
	ControlItem |= CTRL_LOCK_DOOR;
	DoorMode=0;
//	_Puts("[Unlock Door:Start]");
}

void UnlockDoor_Stop(void)
{
	// 停止開門程序
	ControlItem |= CTRL_LOCK_DOOR;
	DoorMode=8;
//	_Puts("[Unlock Door:Stop]");
}

void LockDoor_Stop(void)
{
}

void LockDoor_Start(void)
{
}

void LockDoor_Loop(void)
{
}

void Do_UnlockDoor(void)
{
static uint32_t UnlockTime_1;
static uint32_t UnlockTime_2;
 // << 20190910V1 JASON 新增
    printf("[DoorMode=%d]\r\n",DoorMode);
	switch(DoorMode){ //X= 0 or 1 or 2......或是文字

		case 0:
      if(!DI_DoorClosed() && !DI_DoorLocked()){
        printf("Door unlocking\r\n");
        DoorMode = 1;
      }
      else{
        printf("Door is unlock\r\n");
        DoorMode = 5;
      }  
			break;
 		case 1:
      AC_Power_ON();
      DoorMode++;
			break;        
 		case 2:
      ElectricLock_ON();
      UnlockTime_1 = tickstart;
      DoorMode++;
			break;
 		case 3:
      if(DI_DoorClosed())
      {    
        UnlockTime_2 = tickstart;
        DoorMode = 4;
      }  
      
      if(tickstart - UnlockTime_1 >= 10000)
      {   
        DoorMode = 5;
      }      
			break;    
		case 4:
      if(tickstart - UnlockTime_2 >= 2000)
      {    
        DoorMode++;
      }    
			break;
 		case 5:
      ElectricLock_OFF();  
      printf("Door unlock\r\n");
      DoorMode++;
			ControlItem &= ~CTRL_LOCK_DOOR;
			break;            
		default:         //當所有選項都不成立,執行default內描述

			break;
	}
// >> 20190910V1 JASON 新增
}




