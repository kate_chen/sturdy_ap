


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MENU_H
#define __MENU_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Private includes ----------------------------------------------------------*/
/* Private defines -----------------------------------------------------------*/
#define PROGRAM_MSG_IDX_STANDBY		    	    0
#define PROGRAM_MSG_IDX_VACUUM_1	    	    1		
#define PROGRAM_MSG_IDX_STAEM_1		    	    2		
#define PROGRAM_MSG_IDX_VACUUM_2	    	    3			
#define PROGRAM_MSG_IDX_STAEM_2		    	    4
#define PROGRAM_MSG_IDX_VACUUM_3	    	    5	
#define PROGRAM_MSG_IDX_STAEM_3		    	    6
#define PROGRAM_MSG_IDX_VACUUM_4	    	    7	
#define PROGRAM_MSG_IDX_STAEM_4		    	    8
#define PROGRAM_MSG_IDX_VACUUM_5	    	    9	
#define PROGRAM_MSG_IDX_STAEM_5		    	    10
#define PROGRAM_MSG_IDX_STERILIZE	    	    11	
#define PROGRAM_MSG_IDX_EXHAUST		    	    12
#define PROGRAM_MSG_IDX_EXHAUSTAIR    	    13
#define PROGRAM_MSG_IDX_DRY				          14
#define PROGRAM_MSG_IDX_VACUUM_RELEASE	    15		
#define PROGRAM_MSG_IDX_PAUSE			          16		
#define PROGRAM_MSG_IDX_COMPLETE	    	    17	
#define PROGRAM_MSG_IDX_LEAKAGE			        18
#define PROGRAM_MSG_IDX_Temperature_balance	19
/*
以上定義對應到變數 char *ProgramMessage[] 定義的訊息。(menu.c)
*/
#define IDX_121_UNWRAPPED	    0
#define IDX_134_UNWRAPPED	    1
#define IDX_121_WRAPPED		    2
#define IDX_134_WRAPPED		    3
#define IDX_121_INSTRUMENT		4
#define IDX_134_INSTRUMENT		5
#define IDX_PRION		     	    6
#define IDX_FLASH		     	    7
#define IDX_DRY_ONLY	  	    8
#define IDX_CUSTOMIZATION	  	9
#define IDX_LEAKAGE		  	   10
#define IDX_B_AND_D     	   11
#define IDX_HELIX   	  	   12
#define IDX_LIQUID		   	   13



/* Exported types ------------------------------------------------------------*/
typedef struct {                                                                
	int x,y;                                                                      
	int w1,w2;                                                                    
	void *Value;                                                                  
	int DataType;                                                                 
	long MaxValue;//(代表 Msg 的項目，可用的 index 為 0~MaxValue-1)               
	long MinValue;//(char **Msg;)                                                 
	int NextHmiMode;                                                              
	void *pNextHmi;                                                               
	void (*NextFun)(void);                                                        
	void (*ChangeValueFun)(void *); //數值由變化時，馬上呼叫的副程式 [2010/10/08] 
	union {                                                                       
		unsigned char uc;                                                           
		int i16;                                                                    
		unsigned int u16;                                                           
		long L32;	                                                                  
		unsigned long uL32;	                                                        
		char tmpstr[12];                                                            
	} tmpdata;
} DATA_EDIT,*pDATA_EDIT;

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup MENU_Exported_Functions
  * @{
  */

/** @addtogroup MENU_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
/**
  * @}
  */

/** @addtogroup MENU_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
int StartProgramCheck(int mode);
int StartLeakage(int mode);
int StartHelix(int mode);
int StartBD(int mode);
int CheckPassword(void);
void ResetCodeInput(void);
int DistributorCheckPassword(void);
void DistributorResetInputCode(void);
int SaveNewServiceTimes(void);
void DistributorCopyOldId(void);
int DistributorCheckNewId(void);
void Calibrate_Tc1(void);
void Calibrate_Tc2(void);
void Calibrate_Tc3(void);
void Calibrate_P1(void);
void ToggleHeater(void *tmpdata);
void ToggleVacuumRelease(void *tmpdata);
void ToggleFan1(void *tmpdata);
void ToggleFan2(void *tmpdata);
void ToggleAddWater(void *tmpdata);
void ToggleVacuumSolenoid(void *tmpdata);
void ToggleExhaustSolenoid(void *tmpdata);
void ToggleWaterSensor(void *tmpdata);
void ToggleVacuumPump(void *tmpdata);
void ToggleDoorLockMotor(void *tmpdata);
void ToggleWaterPump(void *tmpdata);
void TogglePower220V(void *tmpdata);
void Page_Read_DO(void);
void SetFor_Program_135_Unwrapped(void);
void SetFor_135_Unwrapped(void);
void SetFor_Program_135_Wrapped(void);
void SetFor_135_Wrapped(void);
void SetFor_Program_126_Unwrapped(void);
void SetFor_126_Unwrapped(void);
void SetFor_Program_126_Wrapped(void);
void SetFor_126_Wrapped(void);
void SetFor_Program_121_Unwrapped(void);
void SetFor_121_Unwrapped(void);
void SetFor_Program_121_Wrapped(void);
void SetFor_121_Wrapped(void);
void SetFor_Program_Prion(void);
void SetFor_Prion(void);
void SetFor_Program_Liquid(void);
void SetFor_Liquid(void);
void SetFor_Program_DryOnly(void);
void SetFor_DryOnly(void);
void SetFor_Program_Customization(void);
void SetFor_Customization(void);
void SetFor_Program_Leakage(void);
void SetFor_Leakage(void);
void SetFor_Program_Helix(void);
void SetFor_Helix(void);
void SetFor_Program_Helix_134(void);
void SetFor_Helix_134(void);
void SetFor_Program_Helix_121(void);
void SetFor_Helix_121(void);
void SetFor_Program_BD(void);
void SetFor_BD(void);
void SetFor_Program_BD_134(void);
void SetFor_BD_134(void);
void SetFor_Program_BD_121(void);
void SetFor_BD_121(void);
void SetFor_Program(int ProgramIdx);
int DoData(pDATA_EDIT data,int key);
void GetCurrentTimeDate(void);
void SetNewTimeDate(void);
int SaveUnitToEeprom(void);
int Fun_Print(int mode);
void LiquidPreSet(void);
void StartDryOnly(void);
void ChangeCustomize(void);
void ShowTime(void);
void ShowCjc(void);
void UpdateScr_00(void);
void PowerOn(void);
void UserId(void);
void DoKeyEvent(void);
void AlarmInverse(void);
void CheckAlarm(void);  
int ShowAlarm(int idx);
void PlayAlarmSound(void);
void SetAlarmKeyAndTime(void);
void SetAlarmSpecialMode(void);
void SetAlarmSoundMode(void);
void StartAlarm(int err);
void Test_ShowComplete_End(void);
void CheckInnerPressure(void);
void Cmd_Alarm(void);
void UpdateDTTP(void);
void UpdateData(void);
void StartProgram(int idx);
int UpdateProgramPage(int mode,int idx);
void Program(void);
int GetDataString(pDATA_EDIT data,unsigned char *buf);
void DoHMI(void);
int _IsKey(void);
int _ReadKey(void);
/**
  * @}
  */

/** @addtogroup MENU_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __MENU_H */




