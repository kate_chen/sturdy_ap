
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "uart_debug.h"
#include "eep.h"


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

int EEP_SetParamIndex(VCOM_PARAM *pSavedParam,int idx)
{
	return 0;
}

void EEP_SetParamIndex_To_AD_QC(void)
{
}

void EEP_SetParamIndex_To_AD(void)
{
}

void EEP_SetParamIndex_To_Normal(void)
{
}

void EEP_SetParamIndex_To_Program(void)
{
}

int SetParamIndex(int idx)
{
	return 0;
}

void EEP_InitSavedParam(VCOM_PARAM *pSavedParam,int param_Number)
{
}

void InitSavedParam(void)
{
}

int _ReadParamFromEep_(unsigned param_idx, void *param)
{
	return 0;
}

int ReadParamFromEep(unsigned param_idx)
{
	return 0;
}

int _WriteParamToEep_(int param_idx,void *param)
{
	return 0;
}

int WriteParamToEep(int param_idx)
{
	return 0;
}

void EEP_ClearEepromParam(VCOM_PARAM *pSavedParam,int param_Number)
{
}

void ClearEepromParam(void)
{
}

int XS_EEP_Read(unsigned addr,unsigned size, void *data)
{
	return 0;
}

int XS_EEP_Write(unsigned addr,unsigned size, void *data)
{
	return 0;
}

void ReadMarkString(void)
{
}

void WriteMarkString(void)
{
}

int CheckMarkString(void)
{
	return 0;
}

int CheckEepWrite(void)
{
	return 0;
}

void ReadOpBatchTimes(void)
{
}
 
void WriteOpBatchTimes(void)
{
}

void ReadOpTotalTime(void)
{
}

void WriteOpTotalTime(void)
{
}

void ReadOpServiceTimes(void)
{
}

void WriteOpServiceTimes(void)
{
}

void ReadOpServiceHour(void)
{
}

void WriteOpServiceHour(void)
{
}

void AddDailyBatchTimes(void)
{
}

void ReadDailyBatchTimes(void)
{
}

void ReadPrintSatus(void)
{
}

int SavePrintSatus(void)
{
	return 0;
}

void ReadLanguageSatus(void)
{
}

void SaveLanguageSatus(void)
{
}

void ReadAuto_add_WaterSatus(void)
{
}

void SaveAuto_add_WaterSatus(void)
{
}

void Read_ErrorCodeItem_1(void)
{
}
 
void Save_ErrorCodeItem_1(void)
{
}

void Read_ErrorCodeItem_2(void)
{
}

void Save_ErrorCodeItem_2(void)
{
}

void Read_ErrorCodeItem_3(void)
{
}

void Save_ErrorCodeItem_3(void)
{
}

void ReadTC1QcValue(void)
{
}

void SaveTC1QcValue(void)
{
}

void ReadTC2QcValue(void)
{
}

void SaveTC2QcValue(void)
{
}

void ReadTC3QcValue(void)
{
}

void SaveTC3QcValue(void)
{
}

void ReadAirQcValue(void)
{
}

void SaveAirQcValue(void)
{
}

void Read4_12mAQcValue(void)
{
}

void Save4_12mAQcValue(void)
{
}

void Read_LCD_BackLight(void)
{
}

void Write_LCD_BackLight(void)
{
}

void Read_Password(void)
{
}

void Write_Password(void)
{
}

void Distributor_Read_Password(void)
{
}
 
void Distributor_Write_Password(void)
{
}

void Read_bRunProgram(void)
{
}
 
void Write_bRunProgram(void)
{
}

void Read_stProgramEnd(void)
{
}

void Write_stProgramEnd(void)
{
}

void Read_bUseDhcp(void)
{
}

int Read_bUseDhcp_0(void)
{
	return 0;
}

void Write_bUseDhcp(int value)
{
}
 
void Read_SerialNumber(void)
{
}
 
void Write_SerialNumber(void)
{
}

void ReadPT1QcValue(void)
{
}
 
void SavePT1QcValue(void)
{
}

void ReadPT2QcValue(void)
{
}
 
void SavePT2QcValue(void)
{
}

int ReadAllSavedParamFromEep(void)
{
	return 0;
}




