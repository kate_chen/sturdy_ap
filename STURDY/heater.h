


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HEATER_H
#define __HEATER_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup HEATER_Exported_Functions
  * @{
  */

/** @addtogroup HEATER_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
/**
  * @}
  */

/** @addtogroup HEATER_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
void SGHeater_Start(int temperature);
void SGHeater_Stop(void);
void SGHeater_Loop(void);
void BHeater_Start(int temperature);
void BHeater_Stop(void);
void BHeater_Loop(void);
void PreHeater_Start(int T1,int T2);
void PreHeater_Stop(void);
void PreHeater_Loop(void);

void Heater_Stop(void);
void Heater_Start(int temperature);
void StartHeater(void);
void ChangeHeater(void);
void Heater_Loop(void);
/**
  * @}
  */

/** @addtogroup HEATER_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __HEATER_H */




