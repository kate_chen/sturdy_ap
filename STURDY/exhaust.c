
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "uart_debug.h"
#include "exhaust.h"


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

void ExhaustPressure_Stop(void)
{
}

void ExhaustPressure_Start(unsigned ttt,int PPP)
{
}

void ExhaustPressure_Loop(void)
{
}

void ExhaustAirPressure_Stop(void)
{
}

void ExhaustAirPressure_Start(unsigned ttt,int PPP)
{
}

void ExhaustAirPressure_Loop(void)
{
}

void ExhaustPressureOneShot_Start(void)
{
}

void ExhaustPressureOneShot_Loop(void)
{
}




