
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "uart_debug.h"
#include "DIO.h"
#include "Prime.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/
void SSR1_ON(void)
{
	HAL_GPIO_WritePin(MCU_DO_16_GPIO_Port,MCU_DO_16_Pin,GPIO_PIN_SET);
  printf("[SET SSR1_ON]");
  bNewLine = 1;
}
//----------------------------
void SSR1_OFF(void)
{
	HAL_GPIO_WritePin(MCU_DO_16_GPIO_Port,MCU_DO_16_Pin,GPIO_PIN_RESET);
  printf("[SET SSR1_OFF]");
  bNewLine = 1;
}
//------------------------------------------------------------------------------
void SSR2_ON(void)
{
	HAL_GPIO_WritePin(MCU_DO_17_GPIO_Port,MCU_DO_17_Pin,GPIO_PIN_SET);
  printf("[SET SSR2_ON]");
  bNewLine = 1;
}
//----------------------------
void SSR2_OFF(void)
{
	HAL_GPIO_WritePin(MCU_DO_17_GPIO_Port,MCU_DO_17_Pin,GPIO_PIN_RESET);
  printf("[SET SSR2_OFF]");
  bNewLine = 1;
}
//------------------------------------------------------------------------------
void AddWaterSolenoid_ON(void)
{
	HAL_GPIO_WritePin(MCU_DO_01_GPIO_Port,MCU_DO_01_Pin,GPIO_PIN_SET);
  printf("[SET AddWaterSolenoid_ON]");
  bNewLine = 1;
}
//----------------------------
void AddWaterSolenoid_OFF(void)
{
	HAL_GPIO_WritePin(MCU_DO_01_GPIO_Port,MCU_DO_01_Pin,GPIO_PIN_RESET);
  printf("[SET AddWaterSolenoid_OFF]");
  bNewLine = 1;
}
//------------------------------------------------------------------------------
void AddSteamSolenoid_ON(void)
{
	HAL_GPIO_WritePin(MCU_DO_02_GPIO_Port,MCU_DO_02_Pin,GPIO_PIN_SET);
  printf("[SET AddSteamSolenoid_ON]");
  bNewLine = 1;
}
//----------------------------
void AddSteamSolenoid_OFF(void)
{
	HAL_GPIO_WritePin(MCU_DO_02_GPIO_Port,MCU_DO_02_Pin,GPIO_PIN_RESET);
  printf("[SET AddSteamSolenoid_OFF]");
  bNewLine = 1;
}
//------------------------------------------------------------------------------
void ExhaustAirSolenoid_ON(void)
{
	HAL_GPIO_WritePin(MCU_DO_03_GPIO_Port,MCU_DO_03_Pin,GPIO_PIN_SET);
  printf("[SET ExhaustAirSolenoid_ON]");
  bNewLine = 1;
}
//----------------------------
void ExhaustAirSolenoid_OFF(void)
{
	HAL_GPIO_WritePin(MCU_DO_03_GPIO_Port,MCU_DO_03_Pin,GPIO_PIN_RESET);
  printf("[SET ExhaustAirSolenoid_OFF]");
  bNewLine = 1;
}
//------------------------------------------------------------------------------
void ExhaustSolenoid_ON(void)
{
	HAL_GPIO_WritePin(MCU_DO_04_GPIO_Port,MCU_DO_04_Pin,GPIO_PIN_SET);
  printf("[SET ExhaustSolenoid_ON]");
  bNewLine = 1;
} 
//----------------------------
void ExhaustSolenoid_OFF(void)
{
	HAL_GPIO_WritePin(MCU_DO_04_GPIO_Port,MCU_DO_04_Pin,GPIO_PIN_RESET);
  printf("[SET ExhaustSolenoid_ON]");
  bNewLine = 1;
} 
//------------------------------------------------------------------------------     
void VacuumSolenoid_ON(void)
{
	HAL_GPIO_WritePin(MCU_DO_05_GPIO_Port,MCU_DO_05_Pin,GPIO_PIN_SET);
  printf("[SET VacuumSolenoid_ON]");
  bNewLine = 1;
}
//----------------------------
void VacuumSolenoid_OFF(void)
{
	HAL_GPIO_WritePin(MCU_DO_05_GPIO_Port,MCU_DO_05_Pin,GPIO_PIN_RESET);
  printf("[SET VacuumSolenoid_OFF]");
  bNewLine = 1;
}
//------------------------------------------------------------------------------
void VacuumRelease_ON(void)
{
	HAL_GPIO_WritePin(MCU_DO_06_GPIO_Port,MCU_DO_06_Pin,GPIO_PIN_SET);
  printf("[SET VacuumRelease_ON]");
  bNewLine = 1;
}
//----------------------------
void VacuumRelease_OFF(void)
{
	HAL_GPIO_WritePin(MCU_DO_06_GPIO_Port,MCU_DO_06_Pin,GPIO_PIN_RESET);
  printf("[SET VacuumRelease_OFF]");
  bNewLine = 1;
}
//------------------------------------------------------------------------------
void FastCoolSolenoid_ON(void)
{
	HAL_GPIO_WritePin(MCU_DO_07_GPIO_Port,MCU_DO_07_Pin,GPIO_PIN_SET);
  printf("[SET FastCoolSolenoid_ON]");
  bNewLine = 1;
}
//----------------------------
void FastCoolSolenoid_OFF(void)
{
	HAL_GPIO_WritePin(MCU_DO_07_GPIO_Port,MCU_DO_07_Pin,GPIO_PIN_RESET);
  printf("[SET FastCoolSolenoid_OFF]");
  bNewLine = 1;
}
//------------------------------------------------------------------------------
void Reserve_1_Solenoid_ON(void)
{
	HAL_GPIO_WritePin(MCU_DO_08_GPIO_Port,MCU_DO_08_Pin,GPIO_PIN_SET);
  printf("[SET FastCoolSolenoid_ON]");
  bNewLine = 1;
}
//----------------------------
void Reserve_1_Solenoid_OFF(void)
{
	HAL_GPIO_WritePin(MCU_DO_08_GPIO_Port,MCU_DO_08_Pin,GPIO_PIN_RESET);
  printf("[SET FastCoolSolenoid_OFF]");
  bNewLine = 1;
}
//------------------------------------------------------------------------------
void CoolFan1_ON(void)
{
	HAL_GPIO_WritePin(MCU_DO_18_GPIO_Port,MCU_DO_18_Pin,GPIO_PIN_SET);
  printf("[SET CoolFan1_ON]");
  bNewLine = 1;
}
//----------------------------
void CoolFan1_OFF(void)
{
	HAL_GPIO_WritePin(MCU_DO_18_GPIO_Port,MCU_DO_18_Pin,GPIO_PIN_RESET);
  printf("[SET CoolFan1_OFF]");
  bNewLine = 1;
}
//------------------------------------------------------------------------------
void CoolFan2_ON(void)
{
	HAL_GPIO_WritePin(MCU_DO_19_GPIO_Port,MCU_DO_19_Pin,GPIO_PIN_SET);
  printf("[SET CoolFan2_ON]");
  bNewLine = 1;
}
//----------------------------
void CoolFan2_OFF(void)
{
	HAL_GPIO_WritePin(MCU_DO_19_GPIO_Port,MCU_DO_19_Pin,GPIO_PIN_RESET);
  printf("[SET CoolFan2_OFF]");
  bNewLine = 1;
}
//------------------------------------------------------------------------------
void CoolFan3_ON(void)
{
	HAL_GPIO_WritePin(MCU_DO_20_GPIO_Port,MCU_DO_20_Pin,GPIO_PIN_SET);
  printf("[SET CoolFan3_ON]");
  bNewLine = 1;
}
//----------------------------
void CoolFan3_OFF(void)
{
	HAL_GPIO_WritePin(MCU_DO_20_GPIO_Port,MCU_DO_20_Pin,GPIO_PIN_RESET);
  printf("[SET CoolFan3_OFF]");
  bNewLine = 1;
}
//==============================================================================
void BZ_ON(void)
{
	HAL_GPIO_WritePin(BUZZER_CONTROL_GPIO_Port,BUZZER_CONTROL_Pin,GPIO_PIN_SET);
  printf("[SET BZ_ON]");
  bNewLine = 1;
}
//----------------------------
void BZ_OFF(void)
{
	HAL_GPIO_WritePin(BUZZER_CONTROL_GPIO_Port,BUZZER_CONTROL_Pin,GPIO_PIN_RESET);
  printf("[SET BZ_OFF]");
  bNewLine = 1;
}
//==============================================================================
void WaterSensor1_ON(void)
{
	HAL_GPIO_WritePin(WATER_LEVEL_DE1_GPIO_Port,WATER_LEVEL_DE1_Pin,GPIO_PIN_SET);
  printf("[SET WaterSensor1_ON]");
  bNewLine = 1;
}
//----------------------------
void WaterSensor1_OFF(void)
{
	HAL_GPIO_WritePin(WATER_LEVEL_DE1_GPIO_Port,WATER_LEVEL_DE1_Pin,GPIO_PIN_RESET);
  printf("[SET WaterSensor1_OFF]");
  bNewLine = 1;
}
//------------------------------------------------------------------------------
void WaterSensor2_ON(void)
{
	HAL_GPIO_WritePin(WATER_LEVEL_DE2_GPIO_Port,WATER_LEVEL_DE2_Pin,GPIO_PIN_SET);
  printf("[SET WaterSensor2_ON]");
  bNewLine = 1;
}
//----------------------------
void WaterSensor2_OFF(void)
{
	HAL_GPIO_WritePin(WATER_LEVEL_DE2_GPIO_Port,WATER_LEVEL_DE2_Pin,GPIO_PIN_RESET);
  printf("[SET WaterSensor2_OFF]");
  bNewLine = 1;
}
//------------------------------------------------------------------------------
void WaterSensor3_ON(void)
{
	HAL_GPIO_WritePin(WATER_LEVEL_DE3_GPIO_Port,WATER_LEVEL_DE3_Pin,GPIO_PIN_SET);
  printf("[SET WaterSensor3_ON]");
  bNewLine = 1;
}
//----------------------------
void WaterSensor3_OFF(void)
{
	HAL_GPIO_WritePin(WATER_LEVEL_DE3_GPIO_Port,WATER_LEVEL_DE3_Pin,GPIO_PIN_RESET);
  printf("[SET WaterSensor3_OFF]");
  bNewLine = 1;
}
//==============================================================================
void AC_Power_ON(void)
{
  HAL_GPIO_WritePin(AC_START_GPIO_Port,AC_START_Pin,GPIO_PIN_SET);
  printf("[SET AC_Power_ON]");
  bNewLine = 1;
}
//----------------------------
void AC_Power_OFF(void)
{
  HAL_GPIO_WritePin(AC_START_GPIO_Port,AC_START_Pin,GPIO_PIN_RESET);
  printf("[SET AC_Power_OFF]");
  bNewLine = 1;
}
//------------------------------------------------------------------------------
void VacuumPump_ON(void)
{
  HAL_GPIO_WritePin(MCU_ACO_01_GPIO_Port,MCU_ACO_01_Pin,GPIO_PIN_SET);
  printf("[SET VacuumPump_ON]");
  bNewLine = 1;
}
//----------------------------
void VacuumPump_OFF(void)
{
  HAL_GPIO_WritePin(MCU_ACO_01_GPIO_Port,MCU_ACO_01_Pin,GPIO_PIN_RESET);
  printf("[SET VacuumPump_OFF]");
  bNewLine = 1;
}
//------------------------------------------------------------------------------
void WaterPump_ON(void)
{
  HAL_GPIO_WritePin(MCU_ACO_03_GPIO_Port, MCU_ACO_03_Pin, GPIO_PIN_SET);
  printf("[SET WaterPump_ON]");
  bNewLine = 1;
}
//----------------------------
void WaterPump_OFF(void)
{
	HAL_GPIO_WritePin(MCU_ACO_03_GPIO_Port, MCU_ACO_03_Pin, GPIO_PIN_RESET);
  printf("[SET WaterPump_OFF]");
  bNewLine = 1;
}
//------------------------------------------------------------------------------
void ElectricLock_ON(void)
{
  HAL_GPIO_WritePin(MCU_ACO_02_GPIO_Port,MCU_ACO_02_Pin,GPIO_PIN_SET);
  printf("[SET ElectricLock_ON]");  
  bNewLine = 1;
}
void ElectricLock_OFF(void)
{
  HAL_GPIO_WritePin(MCU_ACO_02_GPIO_Port,MCU_ACO_02_Pin,GPIO_PIN_RESET);
  printf("[SET ElectricLock_OFF]");	
  bNewLine = 1;
}
//------------------------------------------------------------------------------
void PipeHeater_ON(void)
{
  HAL_GPIO_WritePin(MCU_ACO_04_GPIO_Port, MCU_ACO_04_Pin, GPIO_PIN_SET);
  printf("[SET PipeHeater_ON]");  
  bNewLine = 1;
}
//----------------------------
void PipeHeater_OFF(void)
{
	HAL_GPIO_WritePin(MCU_ACO_04_GPIO_Port, MCU_ACO_04_Pin, GPIO_PIN_RESET);
  printf("[SET PipeHeater_OFF]");	
  bNewLine = 1;
}

//------------------------------------------------------------------------------
void _ReadDI(void)
{
}

int DI_DoorClosed(void)
{
	return (HAL_GPIO_ReadPin(MCU_DI_01_GPIO_Port,MCU_DI_01_Pin));
}

int DI_DoorLocked(void)
{
	return (HAL_GPIO_ReadPin(MCU_DI_02_GPIO_Port,MCU_DI_02_Pin));
}

int DI_SG_WaterLevel(void)
{
	return (HAL_GPIO_ReadPin(MCU_DI_03_GPIO_Port,MCU_DI_03_Pin));
}

int DI_Reserve_WaterLevel(void)
{
	return (HAL_GPIO_ReadPin(MCU_DI_04_GPIO_Port,MCU_DI_04_Pin));
}

int DI_WaterSupplyTankHigh(void)
{
	return (HAL_GPIO_ReadPin(MCU_DI_06_GPIO_Port,MCU_DI_06_Pin));
}

int DI_WaterSupplyTankLow(void)
{
	return (HAL_GPIO_ReadPin(MCU_DI_07_GPIO_Port,MCU_DI_07_Pin));
}

int DI_WaterCollectionTankHigh(void)
{
	return (HAL_GPIO_ReadPin(MCU_DI_08_GPIO_Port,MCU_DI_08_Pin));
}

int DI_OverTemperature(void)
{
	return (HAL_GPIO_ReadPin(MCU_ACI_01_GPIO_Port,MCU_ACI_01_Pin));
}

int DI_OverPressure(void)
{
	return (HAL_GPIO_ReadPin(MCU_ACI_02_GPIO_Port,MCU_ACI_02_Pin));
}

int DI_WaterSupplyTankOk(void)
{
	return 0;
}
 
int DI_WaterCollectionTankOk(void)
{
	return 0;
}

int DI_WaterSupplyTank1High(void)
{
	return 0;
}

int DI_WaterSupplyTank2High(void)
{
	return 0;
}

int _GetDI(int idx)
{
	return 0;
}

int _GetDO(int idx)
{
	return 0;
}

void _SetDO(int idx,int value)
{
}

void DoPowerOn(void)
{
}

void PowerOff(void)
{
}




