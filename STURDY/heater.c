
/* Includes ------------------------------------------------------------------*/
#include "Prime.h"
#include "uart_debug.h"
#include "heater.h"


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
unsigned CheckSGHeaterMode = 0;
int CurSGHeaterFinalTemperature = 0;
int SGH_Temp_Low = 0;
int SGH_Temp_High = 0;

unsigned CheckBHeaterMode = 0;
int CurBHeaterFinalTemperature = 0;
int BH_Temp_Low = 0;
int BH_Temp_High = 0;

unsigned PreHeaterMode = 0;
int PreHeaterTemp1 = 0;
int PreHeaterTemp2 = 0;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern int K1Temperature;
extern int K2Temperature;

/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

void SGHeater_Start(int temperature)
{
	if(temperature > 230)
		temperature = 230;

	CurSGHeaterFinalTemperature = temperature*10;
	CheckSGHeaterMode = 0;
	ControlItem |= CTRL_SGHEATER;
}

void SGHeater_Stop(void)
{
	CheckSGHeaterMode = 10;
	ControlItem |= CTRL_SGHEATER;
}

void SGHeater_Loop(void)
{
	switch(CheckSGHeaterMode)
	{
 		case 0:
 			SGH_Temp_Low = CurSGHeaterFinalTemperature-5;
 			SGH_Temp_High = CurSGHeaterFinalTemperature;
			CheckSGHeaterMode++;
			break;
 		case 1:
			CheckSGHeaterMode++;
			break;
 		case 2:
 			SSR1_ON();
			CheckSGHeaterMode++;
			break;
 		case 3:
 			if(SG_Temperature_10 > SGH_Temp_High)
				CheckSGHeaterMode++;
			break;
 		case 4:
 		case 10:
 			SSR1_OFF();
			CheckSGHeaterMode++;
			break;
 		case 5:
 			if(SG_Temperature_10 < SGH_Temp_Low)
				CheckSGHeaterMode = 2;
			break;
 		case 11:
 			ControlItem &= (~CTRL_SGHEATER);
			CheckSGHeaterMode++;
			break;
		default:        
 			ControlItem &= (~CTRL_SGHEATER);
			break;
	}
}

void BHeater_Start(int temperature)
{
	if(temperature > 230)
		temperature = 230;

	CurBHeaterFinalTemperature = temperature*10;
	CheckBHeaterMode = 0;
	ControlItem |= CTRL_BHEATER;
}

void BHeater_Stop(void)
{
	CheckBHeaterMode = 10;
	ControlItem |= CTRL_BHEATER;
}

void BHeater_Loop(void)
{
	switch(CheckBHeaterMode)
	{
 		case 0:
 			BH_Temp_Low = CurBHeaterFinalTemperature-5;
 			BH_Temp_High = CurBHeaterFinalTemperature;
			CheckBHeaterMode++;
			break;
 		case 1:
			CheckBHeaterMode++;
			break;
 		case 2:
 			SSR2_ON();
			CheckBHeaterMode++;
			break;
 		case 3:
 			if(BH_Temperature_10 > BH_Temp_High)
				CheckBHeaterMode++;
			break;
 		case 4:
 		case 10:
 			SSR2_OFF();
			CheckBHeaterMode++;
			break;
 		case 5:
 			if(BH_Temperature_10 < BH_Temp_Low)
				CheckBHeaterMode = 2;
			break;
 		case 11:
 			ControlItem &= (~CTRL_BHEATER);
			CheckBHeaterMode++;
			break;
		default:        
 			ControlItem &= (~CTRL_BHEATER);
			break;
	}
}

void PreHeater_Start(int T1,int T2)
{
	PreHeaterTemp1 = T1;
	PreHeaterTemp2 = T2;
	PreHeaterMode = 0;
	ProgramItem |= PROGRAM_PRE_HEATER;
}
//----------------------------
void PreHeater_Stop(void)
{
	PreHeaterMode = 10;
	ProgramItem |= PROGRAM_PRE_HEATER;
}
//----------------------------
void PreHeater_Loop(void)
{
	switch(PreHeaterMode)
	{
 		case 0:
 			SGHeater_Start(PreHeaterTemp1);
			PreHeaterMode++;
			break;
 		case 1:
 			BHeater_Start(PreHeaterTemp2);
			PreHeaterMode++;
			break;
 		case 2:
 			if((K1Temperature > 800) && (K2Temperature > 800))
				PreHeaterMode++;
			break;
 		case 3:
 		case 12:
 			ProgramItem &= (~PROGRAM_PRE_HEATER);
			PreHeaterMode++;
			break;
 		case 10:
 			SGHeater_Stop();
			PreHeaterMode++;
			break;
 		case 11:
 			BHeater_Stop();
			PreHeaterMode++;
			break;
		default:        
 			ProgramItem &= (~PROGRAM_PRE_HEATER);
			break;
	}
}

void Heater_Stop(void)
{
}

void Heater_Start(int temperature)
{
}

void StartHeater(void)
{
}

void ChangeHeater(void)
{
}

void Heater_Loop(void)
{
}




