
/* Includes ------------------------------------------------------------------*/
#include "Prime.h"
#include "uart_debug.h"
#include "steam.h"


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
unsigned SterilizeMode = 0;
int TemperatureLow = 0;
int TemperatureHigh = 0;
int TargetTemperature_10 = 0;
uint32_t tSterilize = 0;
uint32_t stSterilize = 0;

unsigned SteamMode = 0;
int CurSteamFinalTemp = 0;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

void AddSteamOneShot_Start(void)
{
}

void AddSteamOneShot_Stop_1(void)
{
}

void AddSteamOneShot_Stop_0(void)
{
}
  
void AddSteamOneShot_Loop(void)
{
}
void Steam_Start(int temperature)
{
	if(temperature >135)
		temperature = 135;
	CurSteamFinalTemp = temperature *10;
	SteamMode = 0;
	ProgramItem |= PROGRAM_STEAM;
}

void Steam_Stop(void)
{
	SteamMode = 2;
	ProgramItem |= PROGRAM_STEAM;
}
 
void ResetCurrentTemperatureRec(void)
{
}

void SaveCurrentInnerTemperature(void)
{
}

int GetdT(unsigned dt)
{
	return 0;
}	

void Steam_Loop(void)
{
	switch(SteamMode)
	{
 		case 0:
 			AddSteamSolenoid_ON();
			SteamMode++;
			break;
 		case 1:
 			if(ChamberTemperature_10 >= CurSteamFinalTemp)
				SteamMode++;
			break;
 		case 2:
 			AddSteamSolenoid_OFF();
			SteamMode++;
			break;
 		case 3:
 			ProgramItem &= (~PROGRAM_STEAM);
			SteamMode++;
			break;
		default:        
 			ProgramItem &= (~PROGRAM_STEAM);
			break;
	}
}

void Sterilize_Start(int Temperature,unsigned ttt)
{
	if(Temperature >135)
		Temperature = 135;
	TargetTemperature_10 = Temperature*10;
	tSterilize = ttt*1000;
	SterilizeMode = 0;
	ProgramItem |= PROGRAM_STERILIZE;
}

void Sterilize_Stop(void)
{
	SterilizeMode = 10;
	ProgramItem |= PROGRAM_STERILIZE;
}

void Sterilize_Loop(void)
{
	switch(SterilizeMode)
	{
 		case 0:
 			TemperatureLow = TargetTemperature_10+12;
 			TemperatureHigh = TargetTemperature_10+15;
 			stSterilize = CurrentLoopTimeTicks;
			SterilizeMode++;
			break;
 		case 1:
			SterilizeMode++;
			break;
 		case 2:
 		case 4:
 			AddSteamSolenoid_ON();
			SterilizeMode++;
			break;
 		case 3:
 			if((CurrentLoopTimeTicks - stSterilize) >= tSterilize)
				SterilizeMode = 10;
 			else if(ChamberTemperature_10 >= TemperatureHigh)
				SterilizeMode++;
			break;
 		case 5:
 			if((CurrentLoopTimeTicks - stSterilize) >= tSterilize)
				SterilizeMode = 10;
 			else if(ChamberTemperature_10 <= TemperatureLow)
				SterilizeMode = 2;
			break;
 		case 10:
 			AddSteamSolenoid_OFF();
			SterilizeMode++;
			break;
 		case 11:
 			ControlItem &= (~PROGRAM_STERILIZE);
			SterilizeMode++;
			break;
		default:        
 			ControlItem &= (~PROGRAM_STERILIZE);
			break;
	}
}

void Temperature_balance_Start(int TTTA,unsigned ttt)
{
}

void Temperature_balance_Stop(void)
{
}

void Temperature_balance_Loop(void)
{
}

void Test_Stop(void)
{
}

void Test_Start(int targetT_10,unsigned t1 ,unsigned period)
{
}

void Test_Loop(void)
{
}




