


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_STURDY_H
#define __MAIN_STURDY_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup MAIN_STURDY_Exported_Functions
  * @{
  */

/** @addtogroup MAIN_STURDY_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
/**
  * @}
  */

/** @addtogroup MAIN_STURDY_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
char *GetAliasName(void);
char *GetModuleName(void);
void CommandInput(int key);
void SetConsoleInputFun(void (*fun)(int));
//static void near DoConsole(void);
static void DoConsole(void);
void User_Loop(void);
void DoOption(int argc,char *argv[]);
void SetTZ(void);
long OSToEpochTime(void);
void StartDhcp(void);
void XS_UserInit(int argc,char *argv[]);
void XS_UserEnd(void);
void SystemEnd(void);
void DoInitSystem(void);
/**
  * @}
  */

/** @addtogroup MAIN_STURDY_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __MAIN_STURDY_H */




