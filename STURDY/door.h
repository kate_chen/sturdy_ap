


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DOOR_H
#define __DOOR_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup DOOR_Exported_Functions
  * @{
  */

/** @addtogroup DOOR_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
/**
  * @}
  */

/** @addtogroup DOOR_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
void UnlockDoor_Start(void);
void UnlockDoor_Stop(void);
void LockDoor_Stop(void);
void LockDoor_Start(void);
void LockDoor_Loop(void);
void Do_UnlockDoor(void);
/**
  * @}
  */

/** @addtogroup DOOR_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __DOOR_H */


