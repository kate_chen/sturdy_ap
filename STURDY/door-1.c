#include <stdio.h>
//#include <ctype.h>
//#include <stdlib.h>
//#include <string.h>
#include "main.h"
//#include "command.h"
//#include "uart_debug.h"

static int DoorMode=0;
void UnlockDoor_Start(void)
{
	// 啟動開門程序
	ProgramItem |= PROGRAM_LOCK_DOOR;
	DoorMode=0;
//	_Puts("[Unlock Door:Start]");
}
//------------------------------------------------------------------------------
void UnlockDoor_Stop(void)
{
	// 停止開門程序
	ProgramItem |= PROGRAM_LOCK_DOOR;
	DoorMode=8;
//	_Puts("[Unlock Door:Stop]");
}

//------------------------------------------------------------------------------
//int bDoUnLockDoor=0;
void Do_UnlockDoor(void) //door.c
{ 
static uint32_t UnlockTime_1;
static uint32_t UnlockTime_2;
 // << 20190910V1 JASON 新增
    printf("[DoorMode=%d]\r\n",DoorMode);
	switch(DoorMode){ //X= 0 or 1 or 2......或是文字

		case 0:
      if(!DI_DoorIsClosed() && !DI_DoorIsLocked()){
        printf("Door unlocking\r\n");
        DoorMode = 1;
      }
      else{
        printf("Door is unlock\r\n");
        DoorMode = 5;
      }  
			break;
 		case 1:
      Power220V_ON();
      DoorMode++;
			break;        
 		case 2:
      DoorLockMotor_ON();
      UnlockTime_1 = tickstart;
      DoorMode++;
			break;
 		case 3:
      if(DI_DoorIsClosed())
      {    
        UnlockTime_2 = tickstart;
        DoorMode = 4;
      }  
      
      if(tickstart - UnlockTime_1 >= 10000)
      {   
        DoorMode = 5;
      }      
			break;    
		case 4:
      if(tickstart - UnlockTime_2 >= 2000)
      {    
        DoorMode++;
      }    
			break;
 		case 5:
      DoorLockMotor_OFF();  
      printf("Door unlock\r\n");
      DoorMode++;
			ProgramItem &= ~PROGRAM_LOCK_DOOR;
			break;            
		default:         //當所有選項都不成立,執行default內描述

			break;
	}
// >> 20190910V1 JASON 新增
}

//------------------------------------------------------------------------------