


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CHKWATER_H
#define __CHKWATER_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup CHKWATER_Exported_Functions
  * @{
  */

/** @addtogroup CHKWATER_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
/**
  * @}
  */

/** @addtogroup CHKWATER_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
//void SetLed_2(int idx,int mode);
void CheckWater_Stop(void);
void CheckWater_Start_mode(int mode);
void CheckWater_Start(void);
void CheckWater_Loop(void);
void CheckWater2_Stop(void);
void CheckWater2_Start(void);
void CheckWater2_Loop(void);
void AddWater_Loop(void);
void AddWaterBox_Stop(void);
void AddWaterBox_Start(void);
void AddWaterBox_Loop(void);
/**
  * @}
  */

/** @addtogroup CHKWATER_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __CHKWATER_H */




