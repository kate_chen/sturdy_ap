
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "uart_debug.h"
#include "main_sturdy.h"


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
void (*DoConsoleInput)(int key)=CommandInput;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

char *GetAliasName(void)
{
	return 0;
}

char *GetModuleName(void)
{
	return 0;
}

void CommandInput(int key)
{
#if 0
static int quit=0;

static int LastCmdSource=0;

 

        // 判斷是否有輸入Hot Key，有就直接處理。不是就等待輸入一行命令。

        //連續輸入 3 次 ESC(27)結束程式

        if(key==27){

                quit++;

                if(quit >= 3) QuitMain=1; //連續輸入 3 次結束程式

                return;

        }

        else {

                quit=0;

        }

        switch(key){

                case '^': //測試LCD 背光亮度。

                        IncLedBackLight();

                        break;

                case '}':

                        PlaySound("C4-C4-C4D4E4F4G4A4B4C5 C4 D4 E4 F4 G4 A4 B4 C5");

                        break;

                case '*':

                        LCD_ClrScrn_G();

                        LCD_Rectangle(10,10,50,50,1);

                        LCD_UpdateGraphic();

                        break;

                default:

                        if(bEcho){

                                if(key=='\r') puts("\r\n");

                                else if(key=='\b'){

                                        if(CmdIdx){

                                                puts("\b \b");

                                        }

                                }      

                                else _Putch(key);

                        }

                        if(key=='\r') {

GetNewCmd:

                                Command[CmdIdx]=0;

                                if(!CmdIdx){// 重複上一個命令

                                        strcpy(Command,LastCommand);

                                }

                                else {

                                        // 有新命令就備份

                                        strcpy(LastCommand,Command);

                                }

                                if(ttSource0){

                                        if(LastCmdSource){

                                                LastCmdSource=0;

                                                SetPutch(0);

                                        }

                                }

                                else if(!LastCmdSource){

                                        LastCmdSource=1;

                                        SetPutch(TcpPutch);

                                }

                                DoCommand(Command);

                                CmdIdx=0;

                                if(bEchoPrompt) EchoPrompt();

                        }

                        else if(key=='\b'){

                                if(CmdIdx){

                                        CmdIdx--;

                                }

                        }      

                        else {

                                Command[CmdIdx++]=key;

                                if(CmdIdx>=MAX_CMD_LENGTH) goto GetNewCmd;

                        }

                        break;

        }
#endif
}

void SetConsoleInputFun(void (*fun)(int))
{
		if(fun) DoConsoleInput=fun;
		else DoConsoleInput=CommandInput; // fun 給 0 表示 restore (還原)
}


static void DoConsole(void)
{
}


void User_Loop(void)
{
}

void DoOption(int argc,char *argv[])
{
}

void SetTZ(void)
{
}

long OSToEpochTime(void)
{
	return 0;
}

void XS_UserInit(int argc,char *argv[])
{
}

void XS_UserEnd(void)
{
}

void SystemEnd(void)
{
}

void DoInitSystem(void)
{
}




