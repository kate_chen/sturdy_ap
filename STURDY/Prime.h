


/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __PRIME_H
#define __PRIME_H

#ifdef __cplusplus
extern "C" {
#endif



/* Includes ------------------------------------------------------------------*/
#include "main.h"
/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup PRIME_Exported_Functions
  * @{
  */

/** @addtogroup PRIME_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
/**
  * @}
  */

/** @addtogroup PRIME_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
//------------------------------------------------------------------------------
// main.C 裡頭的副程式/變數
//------------------------------------------------------------------------------
// 底下定義跟 ProgramItem 有關的項目.
#define PROGRAM_CHECK 0x00000001
#define PROGRAM_PRE_HEATER 0x00000002
#define PROGRAM_CHECK_WATER 0x00000004
#define PROGRAM_VACUUM 0x00000008
#define PROGRAM_Temperature_balance 0x00000010
#define PROGRAM_EXHAUST 0x00000020
#define PROGRAM_VACUUM_RELEASE 0x00000040
#define PROGRAM_LEAKAGE_TEST 0x00000080
#define PROGRAM_STEAM 0x00000100
#define PROGRAM_STERILIZE 0x00000200
#define PROGRAM_PAUSE 0x00000400
#define PROGRAM_STOP 0x00000800
#define PROGRAM_LEAKAGE 0x00001000
#define PROGRAM_DRY 0x00002000
#define PROGRAM_EXHAUSTAIR 0x00004000
#define PROGRAM_WAIT 0x00008000

// 底下定義跟 ControlItem 有關的項目 .
#define CTRL_POWER_ON 0x00000001
#define CTRL_POWER_OFF 0x00000002
#define CTRL_LOCK_DOOR 0x00000004
#define CTRL_BHEATER 0x00000008
#define CTRL_SGHEATER 0x00000010
#define CTRL_EXHAUST_PRESSURE_1SHOT 0x00000020
#define CTRL_UNLOCK_DOOR 0x00000040
#define CTRL_CHECK_WATER2 0x00000080
#define CTRL_AddWaterBox 0x00000100
#define CTRL_Prescan 0x00000200
//#define 0x00000400
#define CTRL_PAUSE 0x00000800
#define CTRL_STERILIZE 0x00001000
#define CTRL_DRY 0x00002000
#define CTRL_STEAM 0x00004000
#define CTRL_TEST 0x00008000



extern unsigned ProgramItem;
extern unsigned ControlItem;
extern uint32_t tickstart;

extern uint32_t CurrentLoopTimeTicks;	//ms
extern int ChamberTemperature_10;//0.01℃
extern int SG_Temperature_10;//0.01℃
extern int BH_Temperature_10;	//0.01℃

//------------------------------------------------------------------------------
// DIO.C 裡頭的副程式/變數
//------------------------------------------------------------------------------
void SSR1_ON(void);
void SSR1_OFF(void);
void SSR2_ON(void);
void SSR2_OFF(void);
void AddWaterSolenoid_ON(void);
void AddWaterSolenoid_OFF(void);
void AddSteamSolenoid_ON(void);
void AddSteamSolenoid_OFF(void);
void ExhaustAirSolenoid_ON(void);
void ExhaustAirSolenoid_OFF(void);
void ExhaustSolenoid_ON(void); 
void ExhaustSolenoid_OFF(void);      
void VacuumSolenoid_ON(void);
void VacuumSolenoid_OFF(void);
void VacuumRelease_ON(void);
void VacuumRelease_OFF(void);
void FastCoolSolenoid_ON(void);
void FastCoolSolenoid_OFF(void);
void Reserve_1_Solenoid_ON(void);
void Reserve_1_Solenoid_OFF(void);
void CoolFan1_ON(void);
void CoolFan1_OFF(void);
void CoolFan2_ON(void);
void CoolFan2_OFF(void);
void CoolFan3_ON(void);
void CoolFan3_OFF(void);
void BZ_ON(void);
void BZ_OFF(void);
void WaterSensor1_ON(void);
void WaterSensor1_OFF(void);
void WaterSensor2_ON(void);
void WaterSensor2_OFF(void);
void WaterSensor3_ON(void);
void WaterSensor3_OFF(void);

void VacuumPump_ON(void);
void VacuumPump_OFF(void);
void WaterPump_ON(void);
void WaterPump_OFF(void);
void ElectricLock_ON(void);
void ElectricLock_OFF(void);
void PipeHeater_ON(void);
void PipeHeater_OFF(void);
void AC_Power_ON(void);
void AC_Power_OFF(void);
void _ReadDI(void);
int DI_DoorClosed(void);
int DI_DoorLocked(void);
int DI_OverTemperature(void);
int DI_OverPressure(void);
int DI_WaterSupplyTankOk(void); 
int DI_WaterCollectionTankOk(void);
int DI_WaterSupplyTank1High(void);
int DI_WaterSupplyTank2High(void);
int _GetDI(int idx);
int _GetDO(int idx);
void _SetDO(int idx,int value);
void DoPowerOn(void);
void PowerOff(void);


//------------------------------------------------------------------------------
// control.C 裡頭的副程式/變數
//------------------------------------------------------------------------------
extern int bNewLine;
//<< 20191003V1 JASON Precsan需求新增
extern int DO01,DO02,DO03,DO04,DO05,DO06,DO07,DO08,DO09,DO10;
extern int DO11,DO12,DO13,DO14,DO15,DO16,DO17;
extern int RO01,RO02,RO03,RO04,RO05;
//>> 20191003V1 JASON Precsan需求新增


void Prescan_Start(void);
void Prescan_Stop(void);
void Prescan_Loop(void);

//------------------------------------------------------------------------------
// door.C 裡頭的副程式/變數
//------------------------------------------------------------------------------
void Do_UnlockDoor(void);


#ifdef __cplusplus
}
#endif

#endif /* __PRIME_H */



