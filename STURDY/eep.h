


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __EEP_H
#define __EEP_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/
typedef struct {
	int PARAM_NAME;
	int size;	// 由 type 可以決定 size。
	void *value;
	int offset;
} VCOM_PARAM;

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup EEP_Exported_Functions
  * @{
  */

/** @addtogroup EEP_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
/**
  * @}
  */

/** @addtogroup EEP_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
int EEP_SetParamIndex(VCOM_PARAM *pSavedParam,int idx);
void EEP_SetParamIndex_To_AD_QC(void);
void EEP_SetParamIndex_To_AD(void);
void EEP_SetParamIndex_To_Normal(void);
void EEP_SetParamIndex_To_Program(void);
int SetParamIndex(int idx);
void EEP_InitSavedParam(VCOM_PARAM *pSavedParam,int param_Number);
void InitSavedParam(void);
int _ReadParamFromEep_(unsigned param_idx, void *param);
int ReadParamFromEep(unsigned param_idx);
int _WriteParamToEep_(int param_idx,void *param);
int WriteParamToEep(int param_idx);
void EEP_ClearEepromParam(VCOM_PARAM *pSavedParam,int param_Number);
void ClearEepromParam(void);
int XS_EEP_Read(unsigned addr,unsigned size, void *data);
int XS_EEP_Write(unsigned addr,unsigned size, void *data);
void ReadMarkString(void);
void WriteMarkString(void);
int CheckMarkString(void);
int CheckEepWrite(void);
void ReadOpBatchTimes(void); 
void WriteOpBatchTimes(void);
void ReadOpTotalTime(void);
void WriteOpTotalTime(void);
void ReadOpServiceTimes(void);
void WriteOpServiceTimes(void);
void ReadOpServiceHour(void);
void WriteOpServiceHour(void);
void AddDailyBatchTimes(void);
void ReadDailyBatchTimes(void);
void ReadPrintSatus(void);
int SavePrintSatus(void);
void ReadLanguageSatus(void);
void SaveLanguageSatus(void);
void ReadAuto_add_WaterSatus(void);
void SaveAuto_add_WaterSatus(void);
void Read_ErrorCodeItem_1(void); 
void Save_ErrorCodeItem_1(void);
void Read_ErrorCodeItem_2(void);
void Save_ErrorCodeItem_2(void);
void Read_ErrorCodeItem_3(void);
void Save_ErrorCodeItem_3(void);
void ReadTC1QcValue(void);
void SaveTC1QcValue(void);
void ReadTC2QcValue(void);
void SaveTC2QcValue(void);
void ReadTC3QcValue(void);
void SaveTC3QcValue(void);
void ReadAirQcValue(void);
void SaveAirQcValue(void);
void Read4_12mAQcValue(void);
void Save4_12mAQcValue(void);
void Read_LCD_BackLight(void);
void Write_LCD_BackLight(void);
void Read_Password(void);
void Write_Password(void);
void Distributor_Read_Password(void); 
void Distributor_Write_Password(void);
void Read_bRunProgram(void); 
void Write_bRunProgram(void);
void Read_stProgramEnd(void);
void Write_stProgramEnd(void);
void Read_bUseDhcp(void);
int Read_bUseDhcp_0(void);
void Write_bUseDhcp(int value); 
void Read_SerialNumber(void); 
void Write_SerialNumber(void);
void ReadPT1QcValue(void); 
void SavePT1QcValue(void);
void ReadPT2QcValue(void); 
void SavePT2QcValue(void);
int ReadAllSavedParamFromEep(void);
/**
  * @}
  */

/** @addtogroup EEP_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __EEP_H */




