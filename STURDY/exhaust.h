


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __EXHAUST_H
#define __EXHAUST_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup EXHAUST_Exported_Functions
  * @{
  */

/** @addtogroup EXHAUST_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
/**
  * @}
  */

/** @addtogroup EXHAUST_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
void ExhaustPressure_Stop(void);
void ExhaustPressure_Start(unsigned ttt,int PPP);
void ExhaustPressure_Loop(void);
void ExhaustAirPressure_Stop(void);
void ExhaustAirPressure_Start(unsigned ttt,int PPP);
void ExhaustAirPressure_Loop(void);
void ExhaustPressureOneShot_Start(void);
void ExhaustPressureOneShot_Loop(void);
/**
  * @}
  */

/** @addtogroup EXHAUST_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __EXHAUST_H */




