


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __VACUUM_H
#define __VACUUM_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup VACUUM_Exported_Functions
  * @{
  */

/** @addtogroup VACUUM_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
/**
  * @}
  */

/** @addtogroup VACUUM_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
void Vacuum_Stop(void);
void Vacuum_Start_New(unsigned t1,int p1,int dp2,unsigned t2,unsigned drytime,unsigned t3,int Mode);
void Vacuum_Loop(void);
void VacuumRelease_Stop(void);
void VacuumRelease_Start(unsigned ttt,int PPP,int VR_Print);
void VacuumRelease_Loop(void);
/**
  * @}
  */

/** @addtogroup VACUUM_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __VACUUM_H */




