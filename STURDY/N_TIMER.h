


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __N_TIMER_H
#define __N_TIMER_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup N_TIMER_Exported_Functions
  * @{
  */

/** @addtogroup N_TIMER_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
/**
  * @}
  */

/** @addtogroup N_TIMER_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
void SD_LedOn(void);
void SD_LedOff(void);
int IsKey(void);
int ReadKey(void);
//static int near PushKey(int key);
static int PushKey(int key);
unsigned char IsKeyEvent(void);
unsigned char ReadKeyEvent(void);
//static int near PushKeyEvent(int key);
static int PushKeyEvent(int key);
void OutWave1(unsigned long freq,unsigned high,unsigned low);
void StopOutWave1(void);
void _Sound(unsigned freq);
void Sound(unsigned freq,unsigned period);
int ReadKeyStatus(unsigned key);
void ScanKeyPad(void);
void PlayCurrentSoundCmd(void);
int PlaySound(char *SoundCmd);
//static void near KeySoundLcdBacklight(void);
static void KeySoundLcdBacklight(void);
//static void interrupt TimerIsr_0(void);
static void TimerIsr_0(void);

void RestoreNewTimer(void);
void InstallNewTimer(void);
/**
  * @}
  */

/** @addtogroup N_TIMER_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __N_TIMER_H */




