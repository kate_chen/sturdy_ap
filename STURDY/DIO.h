


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DIO_H
#define __DIO_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup DIO_Exported_Functions
  * @{
  */

/** @addtogroup DIO_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
/**
  * @}
  */

/** @addtogroup DIO_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/

/**
  * @}
  */

/** @addtogroup DIO_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __DIO_H */


