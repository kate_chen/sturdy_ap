


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __AI_H
#define __AI_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Private includes ----------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions prototypes ---------------------------------------------*/
/** @addtogroup AI_Exported_Functions
  * @{
  */

/** @addtogroup AI_Exported_Functions_Group1
  * @{
  */
/* Initialization/de-initialization functions  ********************************/
/**
  * @}
  */

/** @addtogroup AI_Exported_Functions_Group2
  * @{
  */
/* I/O operation functions  ***************************************************/
int Read_AI_Hex(void);
int Read_AI_Hex_C(void);
//static unsigned near _ReadAi_10(void);
static unsigned _ReadAi_10(void);
//static unsigned near _ReadAi_100(void);
static unsigned _ReadAi_100(void);
void SetAiChannel(unsigned channel);
void ResetAiChannel(void);
unsigned _GetAI(int idx);
void StartReadAI(void);
void _ReadAI(void);
int GetTc1Temperature(void);
int GetTc2Temperature(void);
int GetTc3Temperature(void);
int GetPT1Temperature(void);
int GetPT2Temperature(void);
void PT_AI_Reset(void);
int GetRelativePressure(void);
int GetRelativePressure_10(void);
//static void near CjcHLH(void);
static void CjcHLH(void);
//static void near CjcLHL(void);
static void CjcLHL(void);
void SetCjcAverageNo(int no);
//static int near Tx100Averag(int newTx100);
static int Tx100Averag(int newTx100);
void GetCjc(void);
long CALC_CJC_V1_mV(int cjc100);
long CALC_CJC_V2_mV(int cjc100);
long CALC_CJC_V3_mV(int cjc100);
int CvtmV2T(int mV);
int CvtmR2P(int mV);
int GetTempTC1(int ad,int cjc);
int GetTempTC2(int ad,int cjc);
int GetTempTC3(int ad,int cjc);
int GetTempPT1(int ad,int cjc);
int GetTempPT2(int ad,int cjc);
int GetAirPressure(int ad);
void TestTC2(void);
/**
  * @}
  */

/** @addtogroup AI_Exported_Functions_Group3
  * @{
  */
/* Peripheral State and Error functions ***************************************/
/**
  * @}
  */

/**
  * @}
  */



#ifdef __cplusplus
}
#endif

#endif /* __AI_H */




