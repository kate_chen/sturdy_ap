
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "uart_debug.h"
#include "MENU.h"


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

int StartProgramCheck(int mode)
{
	return 0;
}

int StartLeakage(int mode)
{
	return 0;
}

int StartHelix(int mode)
{
	return 0;
}

int StartBD(int mode)
{
	return 0;
}

int CheckPassword(void)
{
	return 0;
}

void ResetCodeInput(void)
{
}

int DistributorCheckPassword(void)
{
	return 0;
}

void DistributorResetInputCode(void)
{
}

int SaveNewServiceTimes(void)
{
	return 0;
}

void DistributorCopyOldId(void)
{
}

int DistributorCheckNewId(void)
{
	return 0;
}

void Calibrate_Tc1(void)
{
}

void Calibrate_Tc2(void)
{
}

void Calibrate_Tc3(void)
{
}

void Calibrate_P1(void)
{
}

void ToggleHeater(void *tmpdata)
{
}

void ToggleVacuumRelease(void *tmpdata)
{
}

void ToggleFan1(void *tmpdata)
{
}

void ToggleFan2(void *tmpdata)
{
}

void ToggleAddWater(void *tmpdata)
{
}

void ToggleVacuumSolenoid(void *tmpdata)
{
}

void ToggleExhaustSolenoid(void *tmpdata)
{
}

void ToggleWaterSensor(void *tmpdata)
{
}

void ToggleVacuumPump(void *tmpdata)
{
}

void ToggleDoorLockMotor(void *tmpdata)
{
}

void ToggleWaterPump(void *tmpdata)
{
}

void TogglePower220V(void *tmpdata)
{
}

void Page_Read_DO(void)
{
}

void SetFor_Program_135_Unwrapped(void)
{
}

void SetFor_135_Unwrapped(void)
{
}

void SetFor_Program_135_Wrapped(void)
{
}

void SetFor_135_Wrapped(void)
{
}

void SetFor_Program_126_Unwrapped(void)
{
}

void SetFor_126_Unwrapped(void)
{
}

void SetFor_Program_126_Wrapped(void)
{
}

void SetFor_126_Wrapped(void)
{
}

void SetFor_Program_121_Unwrapped(void)
{
}

void SetFor_121_Unwrapped(void)
{
}

void SetFor_Program_121_Wrapped(void)
{
}

void SetFor_121_Wrapped(void)
{
}

void SetFor_Program_Prion(void)
{
}

void SetFor_Prion(void)
{
}

void SetFor_Program_Liquid(void)
{
}

void SetFor_Liquid(void)
{
}

void SetFor_Program_DryOnly(void)
{
}

void SetFor_DryOnly(void)
{
}

void SetFor_Program_Customization(void)
{
}

void SetFor_Customization(void)
{
}

void SetFor_Program_Leakage(void)
{
}

void SetFor_Leakage(void)
{
}

void SetFor_Program_Helix(void)
{
}

void SetFor_Helix(void)
{
}

void SetFor_Program_Helix_134(void)
{
}

void SetFor_Helix_134(void)
{
}

void SetFor_Program_Helix_121(void)
{
}

void SetFor_Helix_121(void)
{
}

void SetFor_Program_BD(void)
{
}

void SetFor_BD(void)
{
}

void SetFor_Program_BD_134(void)
{
}

void SetFor_BD_134(void)
{
}

void SetFor_Program_BD_121(void)
{
}

void SetFor_BD_121(void)
{
}

void SetFor_Program(int ProgramIdx)
{
}

int DoData(pDATA_EDIT data,int key)
{
	return 0;
}

void GetCurrentTimeDate(void)
{
}

void SetNewTimeDate(void)
{
}

int SaveUnitToEeprom(void)
{
	return 0;
}

int Fun_Print(int mode)
{
	return 0;
}

void LiquidPreSet(void)
{
}

void StartDryOnly(void)
{
}

void ChangeCustomize(void)
{
}

void ShowTime(void)
{
}

void ShowCjc(void)
{
}

void UpdateScr_00(void)
{
}

void PowerOn(void)
{
}

void UserId(void)
{
}

void DoKeyEvent(void)
{
}

void AlarmInverse(void)
{
}

void CheckAlarm(void)
{
}
  
int ShowAlarm(int idx)
{
	return 0;
}

void PlayAlarmSound(void)
{
}

void SetAlarmKeyAndTime(void)
{
}

void SetAlarmSpecialMode(void)
{
}

void SetAlarmSoundMode(void)
{
}

void StartAlarm(int err)
{
}

void Test_ShowComplete_End(void)
{
}

void CheckInnerPressure(void)
{
}

void Cmd_Alarm(void)
{
}

void UpdateDTTP(void)
{
}

void UpdateData(void)
{
}

void StartProgram(int idx)
{
}

int UpdateProgramPage(int mode,int idx)
{
	return 0;
}

void Program(void)
{
}

int GetDataString(pDATA_EDIT data,unsigned char *buf)
{
	return 0;
}

void DoHMI(void)
{
}

int _IsKey(void)
{
	return 0;
}

int _ReadKey(void)
{
	return 0;
}




