
/* Includes ------------------------------------------------------------------*/
#include "Prime.h"
#include "control.h"
#include "uart_debug.h"
#include "command.h"
#include "heater.h"
#include "steam.h"
#include "uart_hmi.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
unsigned ProgramItem=0;
unsigned ControlItem=0;

int bNewLine=0;

uint32_t CurrentLoopTimeTicks;	//ms
int ChamberTemperature_10;//0.01℃
int SG_Temperature_10;//0.01℃
int BH_Temperature_10;	//0.01℃
int ChamberPressure;	//0.001bar = 10kPa
int AirPressure;		//0.001bar = 10kPa

unsigned CheckMode = 0;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern int PT1Temperature;
extern int PT2Temperature;
extern int PT3Temperature;
extern int K1Temperature;
extern int K2Temperature;
extern int K3Temperature;
extern int K4Temperature;
extern int P1Pressure;
extern int P2Pressure;

extern __IO uint32_t uwUpdateItem;
extern __IO uint8_t hUpdateCounter;
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/
// << 20191002V1 JASON 新增
#ifdef USE_PRESCAN
static int PrescanMode=0;
uint32_t PrescaTime;
uint32_t PrescanT1=10000;
uint32_t PrescanT2=13000;
int DO01=1;
int DO02=1;
int DO03=1;
int DO04=1;
int DO05=1;
int DO06=1;
int DO07=1;
int DO08=1;
int DO09=1;
int DO10=1;
int DO11=1;
int DO12=1;
int DO13=1;
int DO14=1;
int DO15=1;
int DO16=1;
int DO17=1;
int RO01=1;
int RO02=1;
int RO03=1;
int RO04=1;
int RO05=1;

//==============================================================================
void Prescan_Start(void)
{
	ControlItem |= CTRL_Prescan;
	PrescanMode = 0;
}
//----------------------------
void Prescan_Stop(void)
{
	ControlItem |= CTRL_Prescan;
	PrescanMode = 89;
}
//----------------------------
void Prescan_Loop(void)
{
	switch(PrescanMode){ //X= 0 or 1 or 2......或是文字

		case 0:
      if(DO01){
        SSR1_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 1:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 2:
      SSR1_OFF();
      PrescanMode++; 
			break;
 		case 3:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;   
//------------------------------------------------------------------------------
		case 4:
      if(DO02){
        SSR2_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 5:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 6:
      SSR2_OFF();
      PrescanMode++; 
			break;
 		case 7:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;   
//------------------------------------------------------------------------------
		case 8:
      if(DO03){
        AddWaterSolenoid_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 9:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 10:
      AddWaterSolenoid_OFF();
      PrescanMode++; 
			break;
 		case 11:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;   
//------------------------------------------------------------------------------
		case 12:
      if(DO04){
        AddSteamSolenoid_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 13:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 14:
      AddSteamSolenoid_OFF();
      PrescanMode++; 
			break;
 		case 15:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;   
//------------------------------------------------------------------------------
		case 16:
      if(DO05){
        ExhaustAirSolenoid_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 17:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 18:
      ExhaustAirSolenoid_OFF();
      PrescanMode++; 
			break;
 		case 19:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;   
//------------------------------------------------------------------------------
		case 20:
      if(DO06){
        ExhaustSolenoid_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 21:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 22:
      ExhaustSolenoid_OFF();
      PrescanMode++; 
			break;
 		case 23:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;   
//------------------------------------------------------------------------------
		case 24:
      if(DO07){
        VacuumSolenoid_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 25:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 26:
      VacuumSolenoid_OFF();
      PrescanMode++; 
			break;
 		case 27:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;   
//------------------------------------------------------------------------------
		case 28:
      if(DO08){
        VacuumRelease_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 29:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 30:
      VacuumRelease_OFF();
      PrescanMode++; 
			break;
 		case 31:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;   
//------------------------------------------------------------------------------
		case 32:
      if(DO09){
        FastCoolSolenoid_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 33:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 34:
      FastCoolSolenoid_OFF();
      PrescanMode++; 
			break;
 		case 35:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;   
//------------------------------------------------------------------------------
		case 36:
      if(DO10){
        Reserve_1_Solenoid_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 37:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 38:
      Reserve_1_Solenoid_OFF();
      PrescanMode++; 
			break;
 		case 39:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;   
//------------------------------------------------------------------------------
		case 40:
      if(DO11){
        CoolFan1_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 41:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 42:
      CoolFan1_OFF();
      PrescanMode++; 
			break;
 		case 43:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;   
//------------------------------------------------------------------------------
		case 44:
      if(DO12){
        CoolFan2_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 45:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 46:
      CoolFan2_OFF();
      PrescanMode++; 
			break;
 		case 47:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;   
//------------------------------------------------------------------------------
		case 48:
      if(DO13){
        CoolFan3_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 49:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 50:
      CoolFan3_OFF();
      PrescanMode++; 
			break;
 		case 51:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;   
//------------------------------------------------------------------------------
		case 52:
      if(DO14){
        WaterSensor1_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 53:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 54:
      WaterSensor1_OFF();
      PrescanMode++; 
			break;
 		case 55:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;  			
//------------------------------------------------------------------------------
		case 56:
      if(DO15){
        WaterSensor2_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 57:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 58:
      WaterSensor2_OFF();
      PrescanMode++; 
			break;
 		case 59:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;  			
//------------------------------------------------------------------------------        
		case 60:
      if(DO16){
        WaterSensor3_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 61:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 62:
      WaterSensor3_OFF();
      PrescanMode++; 
			break;
 		case 63:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;  			
//------------------------------------------------------------------------------
		case 64:
      if(RO05){
        AC_Power_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 65:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 66:
//      AC_Power_OFF();
      PrescanMode++; 
			break;
 		case 67:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;  			
//------------------------------------------------------------------------------
 		case 68:
      if(RO01){
        VacuumPump_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 69:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 70:
      VacuumPump_OFF();
      PrescanMode++; 
			break;
 		case 71:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;  			
//------------------------------------------------------------------------------
		case 72:
      if(RO02){
        ElectricLock_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 73:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 74:
      ElectricLock_OFF();
      PrescanMode++; 
			break;
 		case 75:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;  
//------------------------------------------------------------------------------
		case 76:
      if(RO03){
        WaterPump_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 77:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 78:
      WaterPump_OFF();
      PrescanMode++; 
			break;
 		case 79:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;  						
//------------------------------------------------------------------------------   
		case 80:
      if(RO04){
        PipeHeater_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode+=4;
      } 
      break;
 		case 81:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT1){
        PrescanMode++;    
      }
			break;        
 		case 82:
      PipeHeater_OFF();
      PrescanMode++; 
			break;
 		case 83:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break;  			
//------------------------------------------------------------------------------   
		case 84:
      if(DO17){
        BZ_ON();
        PrescaTime= CurrentLoopTimeTicks;
        PrescanMode++;       
      }
      else{
        PrescanMode = 0;
      } 
      break;
 		case 85:
      if(CurrentLoopTimeTicks-PrescaTime >= 1000){
        PrescanMode++;    
      }
			break;        
 		case 86:
      BZ_OFF();
      PrescanMode++; 
			break;
 		case 87:
      if(CurrentLoopTimeTicks-PrescaTime >= PrescanT2){
        PrescanMode++;    
      }
			break; 
//------------------------------------------------------------------------------
		case 88:
      AC_Power_OFF();
      PrescanMode = 0;       
      break;
//------------------------------------------------------------------------------
		case 89:
      SSR1_OFF();
      PrescanMode++;       
      break;
 		case 90:
      SSR2_OFF();
      PrescanMode++;       
      break;       
 		case 91:
      AddWaterSolenoid_OFF();
      PrescanMode++;       
      break;
 		case 92:
      AddSteamSolenoid_OFF();
      PrescanMode++;       
      break;
		case 93:
      ExhaustAirSolenoid_OFF();
      PrescanMode++;       
      break;
 		case 94:
      ExhaustSolenoid_OFF();
      PrescanMode++;       
      break;       
 		case 95:
      VacuumSolenoid_OFF();
      PrescanMode++;       
      break;
 		case 96:
      VacuumRelease_OFF();
      PrescanMode++;       
      break; 
		case 97:
      FastCoolSolenoid_OFF();
      PrescanMode++;       
      break;
 		case 98:
      Reserve_1_Solenoid_OFF();
      PrescanMode++;       
      break;       
 		case 99:
      CoolFan1_OFF();
      PrescanMode++;       
      break;
 		case 100:
      CoolFan2_OFF();
      PrescanMode++;       
      break;
		case 101:
      CoolFan3_OFF();
      PrescanMode++;       
      break;
 		case 102:
      WaterSensor1_OFF();
      PrescanMode++;       
      break;       
 		case 103:
      WaterSensor2_OFF();
      PrescanMode++;       
      break;
 		case 104:
      WaterSensor3_OFF();
      PrescanMode++;       
      break;               			
//------------------------------------------------------------------------------
 		case 105:
      VacuumPump_OFF();
      PrescanMode++;       
      break;
 		case 106:
      WaterPump_OFF();
      PrescanMode++;       
      break;
		case 107:
      ElectricLock_OFF();
      PrescanMode++;       
      break;
 		case 108:
      PipeHeater_OFF();
      PrescanMode++;       
      break;       
 		case 109:
      AC_Power_OFF();
      PrescanMode++;       
      break;
//------------------------------------------------------------------------------
 		case 110:
      ControlItem &= ~CTRL_Prescan;
      PrescanMode++;       
      break;               			
//------------------------------------------------------------------------------
		default:         //當所有選項都不成立,執行default內描述
      PrescanMode=0;
			break;
	}

}
#endif

void Check_Start(void)
{
	CheckMode = 0;
	ProgramItem |= PROGRAM_CHECK;
}
//----------------------------
void Check_Stop(void)
{
	CheckMode = 14;
	ProgramItem |= PROGRAM_CHECK;
}
//----------------------------
void Check_Loop(void)
{
	switch(CheckMode)
	{
 		case 0:
 			if(GPIO_PIN_RESET == DI_DoorClosed())
 				CheckMode=15;
 			else
 				CheckMode++;
			break;
 		case 1:
 			if(GPIO_PIN_RESET == DI_DoorLocked())
 				CheckMode=15;
 			else
 				CheckMode++;
			break;
 		case 2:
 			ElectricLock_OFF();
			CheckMode++;
			break;
 		case 3:
 			VacuumPump_OFF();
			CheckMode++;
			break;
 		case 4:
 			WaterPump_OFF();
			CheckMode++;
			break;
 		case 5:
 			PipeHeater_OFF();
			CheckMode++;
			break;
 		case 6:
 			AddWaterSolenoid_OFF();
			CheckMode++;
			break;
 		case 7:
 			AddSteamSolenoid_OFF();
			CheckMode++;
			break;
 		case 8:
 			ExhaustAirSolenoid_ON();
			CheckMode++;
			break;
 		case 9:
 			ExhaustSolenoid_OFF();
			CheckMode++;
			break;
 		case 10:
 			VacuumSolenoid_OFF();
			CheckMode++;
			break;
 		case 11:
 			VacuumRelease_OFF();
			CheckMode++;
			break;
 		case 12:
 			FastCoolSolenoid_OFF();
			CheckMode++;
			break;
 		case 13:
 			Reserve_1_Solenoid_OFF();
			CheckMode++;
			break;
 		case 14:
 			ProgramItem &= (~PROGRAM_CHECK);
			CheckMode++;
			break;
 		case 15:
 			ProgramItem &= (~PROGRAM_CHECK);
			CheckMode++;
			//Error_Start();
			break;
		default:       
 			ProgramItem &= (~PROGRAM_CHECK);
			break;
	}
}

// >> 20191002V1 JASON 新增
// << 20190909V1 JASON 新增
uint32_t Testtime;//
int TestMode=0;


void IO_TimeTest(void){

  if(tickstart >= 1000){
  	switch(TestMode){ //X= 0 or 1 or 2......或是文字
  
  		case 0:
        SSR1_ON();
        Testtime = tickstart;
        printf("\r\n[Start time=%d]\r\n",Testtime);
        TestMode++;
  			break;
   		case 1:
        SSR1_OFF();
        TestMode++;
  			break;        
   		case 2:
          printf("\r\n[tickstart=%d,Testtime=%d]\r\n",tickstart,Testtime); 
        if(tickstart - Testtime >= 360000){
          SSR1_ON();
          printf("\r\n[End time=%d]\r\n",Testtime);
          TestMode++;
        }
  			break;
   		case 3:
        SSR1_OFF();
        TestMode++;
  			break;           
   		case 4:
  			break;  
  		default:         //當所有選項都不成立,執行default內描述
        TestMode = 0;
  			break;
  	}    
  }
}
// >> 20190909V1 JASON 新增

void IO_Control(void)
{

	UART_DEBUG_PrintSysTime();
	printf(bNewLine ? "\r\n[%lu]":"\r[%lu]",CurrentLoopTimeTicks);
	bNewLine=0;

	ChamberTemperature_10 = PT1Temperature;
	SG_Temperature_10 = K1Temperature;
	BH_Temperature_10 = K2Temperature;
	ChamberPressure = P2Pressure;
	AirPressure = P1Pressure;

  if(ControlItem & CTRL_BHEATER) BHeater_Loop();
  if(ControlItem & CTRL_SGHEATER) SGHeater_Loop();
  
  if(ProgramItem & PROGRAM_CHECK) Check_Loop();
  if(ProgramItem & PROGRAM_PRE_HEATER) PreHeater_Loop();
  if(ProgramItem & PROGRAM_STERILIZE) Sterilize_Loop();

  if(ControlItem & CTRL_LOCK_DOOR) Do_UnlockDoor();
  
  #ifdef USE_PRESCAN
  if(ControlItem & CTRL_Prescan) Prescan_Loop();
  #endif
}




