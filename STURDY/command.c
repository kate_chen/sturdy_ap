

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "command.h"
#include "Prime.h"
#include "uart_debug.h"
#include "main_sturdy.h"  // 20190916 十方新增
#include "TDS.h"          // 20190916 十方新增
#include "door.h"
#include "heater.h"
#include "uart_hmi.h"



#define _DWORD_	 1
#define _WORD_	 2
#define _INT_		 3
#define _BYTE_	 4
#define _STRING_ 5
/*
For Prime系列  輸入開關訊號 
指令範例 DI 1   --> 顯示DI01讀入狀態 0表讀入低準位 1表讀入高準位 
"DI01:門叩感應器",
"DI02:門鎖感應器",
"DI03:清水高水位",
"DI04:清水低水位",
"DI05:SG/Chamber滿水位",
"DI06:備用水位",
"DI07:廢水滿水位",

"AI01:SG/in Heater超溫(AC)",
"AI02:鍋內超壓(AC)",


*/
//------------------------------------------------------------------------------
#define MAX_CMD_NO 50

/*

use the *Delimiter=" ,\r\t"; to seperate the args.

 

for example:

 

char *test_str="/A1 test2 3rd /end";

 

after call CmdToArg(test_str);

Argc=4,

Argv[0]="/A1";

Argv[1]="test2";

Argv[2]="3rd";

Argv[3]="/end";

*/
int InputItem=0;
//char far InputBuffer[81];
char InputBuffer[81];
char LastInputBuffer[82] = {0};
int InputIdx=0;
INPUT *pCurInput;
int CurInputItem=0;

int bEchoPrompt=1;
char Main_Model[20]="SA-260MB";
char Software_version[20]="PC-260MB_A1V2.10";
char SerialNumber[14]="110805202-005";
__IO int Argc=0;
char *Argv[MAX_CMD_NO];

//static long ttSource0=0L;
//static long ttSource1=0L;


//待刪void (*DoConsoleInput)(int key)=CommandInput;


void Cmd_PowerOn(void)
{
}

int CmdToArg(char *cmd)
{
  char *tok_space = " ,\r\t";
  char * pArg;
  
	Argc=0;

  //printf ("Splitting string \"%s\" into tok_spaces:\n",cmd);
  pArg = strtok(cmd,tok_space);
  while (pArg != NULL)
  {
    //printf ("%d:%s\n",Argc,pArg);
    Argv[Argc++] = pArg;
    pArg = strtok (NULL, tok_space);
  }      
	return 0;
}
#if 0
void CommandInput(int key)
{
#if 0
static int quit=0;
static int LastCmdSource=0;

  // 判斷是否有輸入Hot Key，有就直接處理。不是就等待輸入一行命令。
  //連續輸入 3 次 ESC(27)結束程式
  if(key==27){
    quit++;
    if(quit >= 3) QuitMain=1; //連續輸入 3 次結束程式
    return;
  }
  else {
    quit=0;
  }
  switch(key){
    case '^': //測試LCD 背光亮度。
      IncLedBackLight();
      break;
    case '}':
      PlaySound("C4-C4-C4D4E4F4G4A4B4C5 C4 D4 E4 F4 G4 A4 B4 C5");
      break;
    case '*':
      LCD_ClrScrn_G();
      LCD_Rectangle(10,10,50,50,1);
      LCD_UpdateGraphic();
      break;
    default:
      if(bEcho){
        if(key=='\r') puts("\r\n");
        else if(key=='\b'){
          if(CmdIdx){
            puts("\b \b");
          }
        }      
        else _Putch(key);
      }
      if(key=='\r') {
GetNewCmd:
        Command[CmdIdx]=0;
        if(!CmdIdx){// 重複上一個命令
          strcpy(Command,LastCommand);
        }
        else {
                   // 有新命令就備份
          strcpy(LastCommand,Command);
        }
        if(ttSource0){
          if(LastCmdSource){
            LastCmdSource=0;
            SetPutch(0);
          }
        }
        else if(!LastCmdSource){
          LastCmdSource=1;
          SetPutch(TcpPutch);
        }
        DoCommand(Command);
        CmdIdx=0;
        if(bEchoPrompt) EchoPrompt();
      }
      else if(key=='\b'){
        if(CmdIdx){
          CmdIdx--;
        }
      }      
      else {
        Command[CmdIdx++]=key;
        if(CmdIdx>=MAX_CMD_LENGTH) goto GetNewCmd;
      }
      break;
  }
#endif
}

#endif

/*  //待刪
void SetConsoleInputFun(void (*fun)(int))
{

        if(fun) DoConsoleInput=fun;

        else DoConsoleInput=CommandInput; // fun 給 0 表示 restore (還原)

}
*/


//------------------------------------------------------------------------------
void InputShowMsg(void)
{
	printf("\r\n%s",pCurInput[CurInputItem].msg);
	switch(pCurInput[CurInputItem].type){
		case _DWORD_:
			printf("(%ld):",*(unsigned long *)pCurInput[CurInputItem].data);
			break;
		case _WORD_:
			printf("(%u):",*(unsigned *)pCurInput[CurInputItem].data);
			break;
		case _INT_:
			printf("(%d):",*(int *)pCurInput[CurInputItem].data);
			break;
		case _BYTE_:
			printf("(%u):",*(unsigned char *)pCurInput[CurInputItem].data);
			break;
		case _STRING_:
			printf("(%s):",(unsigned char *)pCurInput[CurInputItem].data);
			break;
	}
}

//------------------------------------------------------------------------------
void InputShowAllMsg(INPUT *pInput,int n)
{
	pCurInput=pInput;
	for(CurInputItem=0; CurInputItem < n; CurInputItem++){
		InputShowMsg();
	}
}

//------------------------------------------------------------------------------
#if 1
void ParamInput(int key)
{
	if(pCurInput[CurInputItem].type==_STRING_){
		// 目前不限制輸入的字元
		if(key=='\b'){
			if(InputIdx){
				InputIdx--;
				printf("\b \b");
			}
		}
		else if(key=='\r'){
			if(!InputIdx){
				// 跳過這一項輸入
				goto Skip;
			}
			goto EndInput; 
		}
		else if(key==27){
			goto EndOfInput; 
		}	
    else {
    	goto Add2Buf;
		}
		return;
	}
	if(isdigit(key)){
Add2Buf:
		InputBuffer[InputIdx++]=key;
		putchar(key);
		if(InputIdx >= pCurInput[CurInputItem].max_size){
			goto EndInput;
		}
	}
	else if(key=='\b'){
		if(InputIdx){
			InputIdx--;
			puts("\b \b");
		}
	}
	else if(InputIdx==0 && (key=='-' || key=='+')){
		InputBuffer[InputIdx++]=key;
		putchar(key);
	}
	else if(key=='\r'){
		if(!InputIdx){
			// 跳過這一項輸入
			goto Skip;
		}
EndInput:
		InputBuffer[InputIdx]=0;
		switch(pCurInput[CurInputItem].type){
			case _DWORD_:
				*(unsigned long *)pCurInput[CurInputItem].data=atol(InputBuffer);
				break;
			case _WORD_:
				*(unsigned *)pCurInput[CurInputItem].data=atoi(InputBuffer);
				break;
			case _INT_:
				*(int *)pCurInput[CurInputItem].data=atoi(InputBuffer);
				break;
			case _BYTE_:
				*(unsigned char *)pCurInput[CurInputItem].data=atoi(InputBuffer);
				break;
			case _STRING_:
				strcpy((char *)pCurInput[CurInputItem].data,InputBuffer);
				break;
		}
Skip:
		InputIdx=0;
		CurInputItem++;
		if(CurInputItem >= InputItem){
			// 已經全部輸入完成
			goto EndOfInput;
		}
		else {
			InputShowMsg();
		}
	}
	else if(key==27){
EndOfInput:	
		SetConsoleInputFun(0); // 還原。
		bEchoPrompt=1;
		//EchoPrompt();
	}
}
#endif
//------------------------------------------------------------------------------
void StartParamInput(INPUT *pInput,int n)
{
	SetConsoleInputFun(ParamInput);
	InputItem=n;
	InputIdx=0;
	pCurInput=pInput;
	CurInputItem=0;
	InputShowMsg();
	bEchoPrompt=0;
}
//==============================================================================

// Cmd_Ver() 顯示版本編號
//char FirmwareVer[]="V0.0.1\r\n";
//char FirmwareVer[]="V0.0.2\r\n";   //20190830 JASON 一十方提供新版本，進版
//char FirmwareVer[]="V0.0.3\r\n";   //20191022 Kate 一Add AP 1.1~1.6, 3.1, 3.2, 3.6, 3.10, 3.11, chapter 4 & 5.
char FirmwareVer[]="V0.0.4\r\n";   //20191024 Kate 一Add update sensor function & some HMI UART protocol.
void Cmd_Ver(void)
{
//	puts(FirmwareVer);
    printf("Model:%s\r\n",Main_Model);
    printf("Ver:%s\r\n",Software_version);
    printf("SN:%s\r\n",SerialNumber);
    printf("FW:%s\r\n",FirmwareVer);
}

char *DO_Name[]={
"<>",
"DO01:SSR1",
"DO02:SSR2",
"DO03:V1 Auto add water Sol",
"DO04:V2 Add steam Sol",
"DO05:V3 Exhaust Air Sol",
"DO06:V4 Exhaust Sol",
"DO07:V5 Vacuum Sol",
"DO08:V6 Vacuum Release Sol",
"DO09:V7 Flash Cool Sol",
"DO10:V8 Reserve Sol",
"DO11:FAN-1",
"DO12:FAN-2",
"DO13:FAN-3",
"DO14:Water sensor 1",
"DO15:Water sensor 2",
"DO16:Water sensor 3",
};


/*
For Prime系列   輸出DO訊號
指令範例 DO 1 1 -->開SSR1 ；例 DO 1 0 -->關SSR1 
"DO01:SSR1",
"DO02:SSR2",
"DO03:水箱/鍋內(60S)自動加水",
"DO04:內鍋進氣/排氣(60S)(常開)",
"DO05:內鍋排氣/排氣(常開)",
"DO06:SG&內鍋排水",
"DO07:抽真空",
"DO08:破真空",
"DO09:快速冷卻(液滅用)",
"DO10:備用-1",
"DO11:FAN1",
"DO12:FAN2",
"DO13:FAN3",
"DO14:SG/Chamber水位偵測開關",
"DO15:清水水位偵測開關",
"DO16:廢水水位偵測開關",

*/


struct _DO_ {
	unsigned DoAddr;
	GPIO_TypeDef* DoVal;
	uint16_t DoBit;  
};
struct _DO_ DoAddrData[]={
{0,0,0},                                                                        //DO00 不使用
{1,MCU_DO_16_GPIO_Port,MCU_DO_16_Pin},                                                  //DO01:SSR1
{2,MCU_DO_17_GPIO_Port,MCU_DO_17_Pin},                                                  //DO02:SSR2
{3,MCU_DO_01_GPIO_Port,MCU_DO_01_Pin},                                                  //DO03:水箱/鍋內(60S)自動加水
{4,MCU_DO_02_GPIO_Port,MCU_DO_02_Pin},                                                  //DO04:內鍋進氣/排氣(60S)(常開)
{5,MCU_DO_03_GPIO_Port,MCU_DO_03_Pin},                                                  //DO05:內鍋排氣/排氣(常開)
{6,MCU_DO_04_GPIO_Port,MCU_DO_04_Pin},                                                  //DO06:SG&內鍋排水
{7,MCU_DO_05_GPIO_Port,MCU_DO_05_Pin},                                                  //DO07:抽真空
{8,MCU_DO_06_GPIO_Port,MCU_DO_06_Pin},                                                  //DO08:破真空
{9,MCU_DO_07_GPIO_Port,MCU_DO_07_Pin},                                                  //DO09:快速冷卻(液滅用)
{10,MCU_DO_08_GPIO_Port,MCU_DO_08_Pin},                                                  //DO10:備用-1
{11,MCU_DO_18_GPIO_Port,MCU_DO_18_Pin},                                                  //DO11:FAN1
{12,MCU_DO_19_GPIO_Port,MCU_DO_19_Pin},                                                  //DO12:FAN2
{13,MCU_DO_20_GPIO_Port,MCU_DO_20_Pin},                                                  //DO13:FAN3
{14,WATER_LEVEL_DE1_GPIO_Port,WATER_LEVEL_DE1_Pin},                                                  //DO14:SG/Chamber水位偵測開關
{15,WATER_LEVEL_DE2_GPIO_Port,WATER_LEVEL_DE2_Pin},                                                  //DO15:清水水位偵測開關
{16,WATER_LEVEL_DE3_GPIO_Port,WATER_LEVEL_DE3_Pin},                                                  //DO16:廢水水位偵測開關
//{,,}, //11:
};
//------------------------------------------------------------------------------
char FmtDo[]="Set [%s] %s\r\n";
void Cmd_DO(void)
{ 
  int DoIdx=0;
  int OnOff=0;
  	
	if(Argc > 1){
		if(!strcasecmp(Argv[1],"reset")){
			// reset DO output.
			Cmd_PowerOn();
			return;
		}
		DoIdx=atoi(Argv[1]);
		if(DoIdx >16 || DoIdx < 1) DoIdx=1;      //13
		if(Argc > 2){
			OnOff=atoi(Argv[2]);
			if(OnOff){
				HAL_GPIO_WritePin(DoAddrData[DoIdx].DoVal, DoAddrData[DoIdx].DoBit, GPIO_PIN_SET);
			}
			else {
				HAL_GPIO_WritePin(DoAddrData[DoIdx].DoVal, DoAddrData[DoIdx].DoBit, GPIO_PIN_RESET);
			}
		}
		else {
			OnOff = HAL_GPIO_ReadPin(DoAddrData[DoIdx].DoVal, DoAddrData[DoIdx].DoBit);
		}
		printf(FmtDo,DO_Name[DoIdx],OnOff?"ON":"OFF");
	}
}

//------------------------------------------------------------------------------
char *RO_Name[]={
"<RO>",
"RO01:Vacuum pump",
"RO02:Water pump",
"RO03:Door lock motor",
"RO04:PH Heater",
"RO05:AC output",
"RO06:TDS",                // 20190916 十方新增
};
/*
For Prime系列   輸出AC訊號
指令範例 RO 1 1 -->開真空幫浦 ；例 RO 1 0 -->關真空幫浦 
"RO01:真空幫浦",
"RO02:加水幫浦",
"RO03:門鎖",
"RO04:管路電熱片",
"RO05:AC輸出開關",
};


*/
//------------------------------------------------------------------------------
void Cmd_RO(void)
{
  int DoIdx=0;
  int OnOff=0;
  	
	if(Argc > 1){
		DoIdx=atoi(Argv[1]);
		if(DoIdx > 6 || DoIdx < 1) DoIdx=1;
		if(Argc > 2){
			OnOff=atoi(Argv[2]);
			switch(DoIdx){
				case 1: //O 2 2
					if(OnOff){
						HAL_GPIO_WritePin(MCU_ACO_01_GPIO_Port, MCU_ACO_01_Pin, GPIO_PIN_SET);
					}
					else {
						HAL_GPIO_WritePin(MCU_ACO_01_GPIO_Port, MCU_ACO_01_Pin, GPIO_PIN_RESET);
					}
					break;
				case 2: //O 2 4
					if(OnOff){
						HAL_GPIO_WritePin(MCU_ACO_02_GPIO_Port, MCU_ACO_02_Pin, GPIO_PIN_SET);
					}
					else {
						HAL_GPIO_WritePin(MCU_ACO_02_GPIO_Port, MCU_ACO_02_Pin, GPIO_PIN_RESET);
					}
					break;
				case 3: //O 2 8
					if(OnOff){
						HAL_GPIO_WritePin(MCU_ACO_03_GPIO_Port, MCU_ACO_03_Pin, GPIO_PIN_SET);
					}
					else {
						HAL_GPIO_WritePin(MCU_ACO_03_GPIO_Port, MCU_ACO_03_Pin, GPIO_PIN_RESET);
					}
					break;
				case 4: //O 2 20
					if(OnOff){
						HAL_GPIO_WritePin(MCU_ACO_04_GPIO_Port, MCU_ACO_04_Pin, GPIO_PIN_SET);
					}
					else {
						HAL_GPIO_WritePin(MCU_ACO_04_GPIO_Port, MCU_ACO_04_Pin, GPIO_PIN_RESET);
					}
					break;
				case 5: //O 2 80
					if(OnOff){
						HAL_GPIO_WritePin(AC_START_GPIO_Port, AC_START_Pin, GPIO_PIN_SET);
					}
					else {
						HAL_GPIO_WritePin(AC_START_GPIO_Port, AC_START_Pin, GPIO_PIN_RESET);
					}
					break;
			}
		}
		else {
			switch(DoIdx){
				case 1: //O 2 2
					OnOff = HAL_GPIO_ReadPin(MCU_ACO_01_GPIO_Port, MCU_ACO_01_Pin);
					break;
				case 2: //O 2 4
					OnOff = HAL_GPIO_ReadPin(MCU_ACO_02_GPIO_Port, MCU_ACO_02_Pin);
					break;
				case 3: //O 2 8
					OnOff = HAL_GPIO_ReadPin(MCU_ACO_03_GPIO_Port, MCU_ACO_03_Pin);
					break;
				case 4: //O 2 20
					OnOff = HAL_GPIO_ReadPin(MCU_ACO_04_GPIO_Port, MCU_ACO_04_Pin);
					break;
				case 5: //O 2 80
					OnOff = HAL_GPIO_ReadPin(AC_START_GPIO_Port, AC_START_Pin);
					break;
				case 6:         // 20190916 十方新增
					OnOff = ~(TDS_ConvStart());
					break;
			}
		}		
		printf(FmtDo,RO_Name[DoIdx],OnOff?"ON":"OFF");
	}
}

// >> 20190827V1 JASON 新增

struct _DI_ {
	unsigned DoAddr;
	GPIO_TypeDef* DoVal;
	uint16_t DoBit;  
};

struct _DI_ DiAddrData[]={
{0,0,0},                                                                        //DO00 
{1,MCU_DI_01_GPIO_Port,MCU_DI_01_Pin},                                                  //DI01:門扣感應器
{2,MCU_DI_02_GPIO_Port,MCU_DI_02_Pin},                                                  //DI02:門鎖感應器
{3,MCU_DI_03_GPIO_Port,MCU_DI_03_Pin},                                                  //DI03:SG / chamber 滿水位
{4,MCU_DI_04_GPIO_Port,MCU_DI_04_Pin},                                                  //DI04:備用水位
{5,MCU_DI_06_GPIO_Port,MCU_DI_06_Pin},                                                  //DI06:清水高水位
{6,MCU_DI_07_GPIO_Port,MCU_DI_07_Pin},                                                  //DI07:清水低水位
{7,MCU_DI_08_GPIO_Port,MCU_DI_08_Pin},                                                  //DI08:廢水滿水位
{6,MCU_ACI_01_GPIO_Port,MCU_ACI_01_Pin},                                                //DI09:SG / in heater 超溫(AC)
{7,MCU_ACI_02_GPIO_Port,MCU_ACI_02_Pin},                                                //DI010:鍋內超壓(AC)
//{,,}, //11:
};

char *DI_Name[]={
"<>",
"DI01:Door Closed SW",      //門扣感應器
"DI02:Door lock SW",        //門鎖感應器
"DI03:SG water level",      //SG / chamber 滿水位
"DI04:Reserve water level", //備用水位
"DI06:Water high level",    //清水高水位
"DI07:Water low level",     //清水低水位
"DI08:Waste water level",   //廢水滿水位
"DI07:Temperature over",     //SG / in heater 超溫(AC)
"DI08:Pressure over",   //DI010:鍋內超壓(AC)
};

char FmtDi[]="[%s:%s]\r\n";
void Cmd_DI(void)
{ 
//  int DoIdx=0;
  int OnOff=0;
	printf("\r\n");
  OnOff = HAL_GPIO_ReadPin(DiAddrData[1].DoVal, DiAddrData[1].DoBit);
	printf(FmtDi,DI_Name[1],OnOff?"H":"L");
  OnOff = HAL_GPIO_ReadPin(DiAddrData[2].DoVal, DiAddrData[2].DoBit);
	printf(FmtDi,DI_Name[2],OnOff?"H":"L");
  OnOff = HAL_GPIO_ReadPin(DiAddrData[3].DoVal, DiAddrData[3].DoBit);
	printf(FmtDi,DI_Name[3],OnOff?"H":"L");	
  OnOff = HAL_GPIO_ReadPin(DiAddrData[4].DoVal, DiAddrData[4].DoBit);
	printf(FmtDi,DI_Name[4],OnOff?"H":"L");
  OnOff = HAL_GPIO_ReadPin(DiAddrData[5].DoVal, DiAddrData[5].DoBit);
	printf(FmtDi,DI_Name[5],OnOff?"H":"L");
  OnOff = HAL_GPIO_ReadPin(DiAddrData[6].DoVal, DiAddrData[6].DoBit);
	printf(FmtDi,DI_Name[6],OnOff?"H":"L");
  OnOff = HAL_GPIO_ReadPin(DiAddrData[7].DoVal, DiAddrData[7].DoBit);
	printf(FmtDi,DI_Name[7],OnOff?"H":"L");
  OnOff = HAL_GPIO_ReadPin(DiAddrData[8].DoVal, DiAddrData[8].DoBit);
	printf(FmtDi,DI_Name[8],OnOff?"H":"L");
  OnOff = HAL_GPIO_ReadPin(DiAddrData[9].DoVal, DiAddrData[9].DoBit);
	printf(FmtDi,DI_Name[9],OnOff?"H":"L");
	printf("*************************");
}
// >> 20190827V1 JASON 新增

// << 20190828V1 JASON 新增

void Cmd_BZ(void)
{ 
//  int DoIdx=0;
  int OnOff=0;


	printf("\r\n");
		if(Argc > 1){
			if(atoi(Argv[1])){
				HAL_GPIO_WritePin(BUZZER_CONTROL_GPIO_Port,BUZZER_CONTROL_Pin,GPIO_PIN_SET);
			}
			else {
				HAL_GPIO_WritePin(BUZZER_CONTROL_GPIO_Port,BUZZER_CONTROL_Pin,GPIO_PIN_RESET);
			}
		}
		else {
			OnOff = HAL_GPIO_ReadPin(BUZZER_CONTROL_GPIO_Port,BUZZER_CONTROL_Pin);
		}
		printf("[BUZZER:%s\r\n",OnOff?"ON":"OFF");
}

// >> 20190828V1 JASON 新增
typedef struct {
  char *cmd;
  void (*fun)(void);
} CmdTable;

void Cmd_Door(void)
{
  UnlockDoor_Start();
}

void Cmd_SGHeater(void)
{
  int temp=0;
  	
	if(Argc > 1){
		temp= atoi(Argv[1]);
		if(temp == 0)
			SGHeater_Stop();
		else if((temp >= 0) && (temp <= 200))
			SGHeater_Start(temp);
	}
}

void Cmd_BHeater(void)
{
  int temp=0;
  	
	if(Argc > 1){
		temp= atoi(Argv[1]);
		if(temp == 0)
			BHeater_Stop();
		else if((temp >= 0) && (temp <= 200))
			BHeater_Start(temp);
	}
}

//<< 20191003V1 JASON Precsan需求新增
#ifdef USE_PRESCAN
void Cmd_Prescan(void)
{
	if(atoi(Argv[1])){
		Prescan_Start();
	}
	else {
		Prescan_Stop();
	}
}
#endif
//----------------------------
void Cmd_DO01(void)
{
	if(atoi(Argv[1])){
		DO01 = 1;
	}
	else {
		DO01 = 0;
	}
  printf("[DO01=%d]",DO01);
  bNewLine = 1;
}
//----------------------------
void Cmd_DO02(void)
{
	if(atoi(Argv[1])){
		DO02 = 1;
	}
	else {
		DO02 = 0;
	}
  printf("[DO02=%d]",DO02);
  bNewLine = 1;
}
//----------------------------
void Cmd_DO03(void)
{
	if(atoi(Argv[1])){
		DO03 = 1;
	}
	else {
		DO03 = 0;
	}
  printf("[DO03=%d]",DO03);
  bNewLine = 1;
}
//----------------------------
void Cmd_DO04(void)
{
	if(atoi(Argv[1])){
		DO04 = 1;
	}
	else {
		DO04 = 0;
	}
  printf("[DO04=%d]",DO04);
  bNewLine = 1;
}
//----------------------------
void Cmd_DO05(void)
{
	if(atoi(Argv[1])){
		DO05 = 1;
	}
	else {
		DO05 = 0;
	}
  printf("[DO05=%d]",DO05);
  bNewLine = 1;
}
//----------------------------
void Cmd_DO06(void)
{
	if(atoi(Argv[1])){
		DO06 = 1;
	}
	else {
		DO06 = 0;
	}
  printf("[DO06=%d]",DO06);
  bNewLine = 1;
}
//----------------------------
void Cmd_DO07(void)
{
	if(atoi(Argv[1])){
		DO07 = 1;
	}
	else {
		DO07 = 0;
	}
  printf("[DO07=%d]",DO07);
  bNewLine = 1;
}
//----------------------------
void Cmd_DO08(void)
{
	if(atoi(Argv[1])){
		DO08 = 1;
	}
	else {
		DO08 = 0;
	}
  printf("[DO08=%d]",DO08);
  bNewLine = 1;
}
//----------------------------
void Cmd_DO09(void)
{
	if(atoi(Argv[1])){
		DO09 = 1;
	}
	else {
		DO09 = 0;
	}
  printf("[DO09=%d]",DO09);
  bNewLine = 1;
}
//----------------------------
void Cmd_DO10(void)
{
	if(atoi(Argv[1])){
		DO10 = 1;
	}
	else {
		DO10 = 0;
	}
  printf("[DO10=%d]",DO10);
  bNewLine = 1;
}
//----------------------------
void Cmd_DO11(void)
{
	if(atoi(Argv[1])){
		DO11 = 1;
	}
	else {
		DO11 = 0;
	}
  printf("[DO11=%d]",DO11);
  bNewLine = 1;
}
//----------------------------
void Cmd_DO12(void)
{
	if(atoi(Argv[1])){
		DO12 = 1;
	}
	else {
		DO12 = 0;
	}
  printf("[DO12=%d]",DO12);
  bNewLine = 1;
}
//----------------------------
void Cmd_DO13(void)
{
	if(atoi(Argv[1])){
		DO13 = 1;
	}
	else {
		DO13 = 0;
	}
  printf("[DO13=%d]",DO13);
  bNewLine = 1;
}
//----------------------------
void Cmd_DO14(void)
{
	if(atoi(Argv[1])){
		DO14 = 1;
	}
	else {
		DO14 = 0;
	}
  printf("[DO14=%d]",DO14);
  bNewLine = 1;
}
//----------------------------
void Cmd_DO15(void)
{
	if(atoi(Argv[1])){
		DO15 = 1;
	}
	else {
		DO15 = 0;
	}
  printf("[DO15=%d]",DO15);
  bNewLine = 1;
}
//----------------------------
void Cmd_DO16(void)
{
	if(atoi(Argv[1])){
		DO16 = 1;
	}
	else {
		DO16 = 0;
	}
  printf("[DO16=%d]",DO16);
  bNewLine = 1;
}
//----------------------------
void Cmd_DO17(void)
{
	if(atoi(Argv[1])){
		DO17 = 1;
	}
	else {
		DO17 = 0;
	}
  printf("[DO17=%d]",DO17);
  bNewLine = 1;
}
//----------------------------
void Cmd_RO01(void)
{
	if(atoi(Argv[1])){
		RO01 = 1;
	}
	else {
		RO01 = 0;
	}
  printf("[RO01=%d]",RO01);
  bNewLine = 1;
}
//----------------------------
void Cmd_RO02(void)
{
	if(atoi(Argv[1])){
		RO02 = 1;
	}
	else {
		RO02 = 0;
	}
  printf("[RO02=%d]",RO02);
  bNewLine = 1;
}
//----------------------------
void Cmd_RO03(void)
{
	if(atoi(Argv[1])){
		RO03 = 1;
	}
	else {
		RO03 = 0;
	}
  printf("[RO03=%d]",RO03);
  bNewLine = 1;
}
//----------------------------
void Cmd_RO04(void)
{
	if(atoi(Argv[1])){
		RO04 = 1;
	}
	else {
		RO04 = 0;
	}
  printf("[RO04=%d]",RO04);
  bNewLine = 1;
}
//----------------------------
void Cmd_RO05(void)
{
	if(atoi(Argv[1])){
		RO05 = 1;
	}
	else {
		RO05 = 0;
	}
  printf("[RO05=%d]",RO05);
  bNewLine = 1;
}
//>> 20191003V1 JASON Precsan需求新增
#if 1
//static far CmdTable CT[]={
static CmdTable CT[]={
	{"di",Cmd_DI },
	{"do",Cmd_DO },
	{"BZ",Cmd_BZ },
	{"RO",Cmd_RO },
	{"ver",Cmd_Ver},
	{"door",Cmd_Door},
	{"Sgheater",Cmd_SGHeater},
	{"bheater",Cmd_BHeater},

//<< 20191003V1 JASON Precsan需求新增
	#ifdef USE_PRESCAN
	{"prescan",Cmd_Prescan},
	{"DO01",Cmd_DO01},  	
	{"DO02",Cmd_DO02},  
	{"DO03",Cmd_DO03},  
	{"DO04",Cmd_DO04},  
	{"DO05",Cmd_DO05},      	
	{"DO06",Cmd_DO06},  	
	{"DO07",Cmd_DO07},  
	{"DO08",Cmd_DO08},  
	{"DO09",Cmd_DO09},  
	{"DO10",Cmd_DO10},
	{"DO11",Cmd_DO11},  	
	{"DO12",Cmd_DO12},  
	{"DO13",Cmd_DO13},  
	{"DO14",Cmd_DO14},  
	{"DO15",Cmd_DO15},	
	{"DO16",Cmd_DO16},  	
	{"RO01",Cmd_RO01},  
	{"RO02",Cmd_RO02},  
	{"RO03",Cmd_RO03},  
	{"RO04",Cmd_RO04},
	{"RO05",Cmd_RO05},
	#endif
//>> 20191003V1 JASON Precsan需求新增
};
#else
static far CmdTable CT[]={
//	{"load",Cmd_Load },
	{"di",Cmd_DI },
	{"do1",Cmd_DO1 },
	{"do2",Cmd_DO2 },
	{"do3",Cmd_DO3 },
	{"do",Cmd_DO },
	{"RO",Cmd_RO },
	{"AI",Cmd_AD},
	{"ver",Cmd_Ver},
	{"LED",Cmd_Led},
	{"prn",Cmd_Printer},
	{"i",Cmd_Input},
	{"o",Cmd_Output},
	{"cjc",Cmd_CJC},
	{"door",Cmd_Door},
	{"water",Cmd_Water},
	{"heater",Cmd_Heater},
	{"vacuum",Cmd_Vacuum},
	{"v",Cmd_Vacuum_New},
	{"exhaust",Cmd_ExhaustPressure},
	{"e",Cmd_ExhaustPressure},
	{"2",Cmd_ExhaustPressure1Shot},
	{"1",Cmd_AddSteam1Shot},
//	{"steam",Cmd_AddSteam},
	{"h",Cmd_Steam},
	{"p",Cmd_Pause},
	{"R",Cmd_VacuumRelease},
	{"s",Cmd_Sterilize},
	{"d",Cmd_Dry},
	{"scanai",Cmd_ScanAi},

//	{"tc1offset",Cmd_Tc1Offset},
	{"tc1table",Cmd_Tc1Table},
	{"tc2table",Cmd_Tc2Table}, //[2010/09/08]
//	{"tc3offset",Cmd_Tc3Offset},
	{"tc3table",Cmd_Tc3Table},
  {"pt1table",Cmd_Pt1Table},
  {"pt2table",Cmd_Pt2Table},  
	{"airtable",Cmd_AirTable},//[2011/06/24]

	{"calitc1",Cmd_CaliTc1},
	{"calitc2",Cmd_CaliTc2},
	{"calitc3",Cmd_CaliTc3},
  {"calipt1",Cmd_CaliPt1},
	{"calipt2",Cmd_CaliPt2}, 
	{"caliair",Cmd_CaliAir},  //[2011/06/24]

  {"tc1offset",Cmd_Tc1Offset},
	{"tc2offset",Cmd_Tc2Offset},
  {"tc3offset",Cmd_Tc3Offset},
  {"pt1offset",Cmd_Pt1Offset},
  {"pt2offset",Cmd_Pt2Offset},

	{"poffset",Cmd_PressureOffset},
	{"ptable",Cmd_PressureTable},
	{"printer",Cmd_TestPrinter},
	{"auto",Cmd_Auto},
	{"start",Cmd_Start},
	{"program",Cmd_Program},
	{"stop",Cmd_StopProgram},
	{"time",Cmd_Time},
	{"date",Cmd_Date},
	{"sd",Cmd_SD},
	{"newsd",Cmd_NewSD},
	{"cd",Cmd_CD},
	{"dir",Cmd_DIR},
	{"type",Cmd_Type},
	{"load",Cmd_SD_Load},
	{"get",Cmd_Get},
	{"alarm",Cmd_Alarm},
	{"leakage",Cmd_LeakageTest},
	{"test",Cmd_Test},
	{"disp",Cmd_Display},
	{"next",Cmd_NextStep},
	{"quit",Cmd_Quit},
	{"ip",Cmd_Ip},
	{"mask",Cmd_Mask},
	{"gateway",Cmd_Gateway},
	{"setexhaust",Cmd_SetExhaust},
	{"sfixed",Cmd_SteamFixed},
	{"sound",Cmd_PlaySound},
	{"font",TestFont}, // 測試字型用
	{"eep",WriteMarkString}, // 測試字型用
	{"chkerr",Cmd_CheckErrorMode}, // 測試字型用
	{"errorcode",Cmd_ErrorCode}, // 20130702 JASON 新增，控制錯誤碼用
	{"lang",Cmd_Language}, // 20160516 JASON 新增，控制顯示語言用    
  
	{"arp",Cmd_Arp}, // 測試用
	{"table",Cmd_Table}, // 測試用
	{"ctrl",Cmd_Ctrl}, // 測試用
	{"counter",Cmd_Counter}, //出貨將 counter reset 用。
	{"serial",Cmd_SerialNumber}, //出貨前設定 serial number 用。
};
#endif

static int CmdNo=sizeof(CT)/sizeof(CmdTable);
/*
目前可用的測試命令：
"di",讀 D/I
"do n xx",設定 D/O 輸出。n:1 ~ 3, xx :"00"~"FF"
"AI", 讀 A/D channel 0
"ver",詢問版本
"LED" 設定第二組 LED
"i" 跟 OS 的命令一樣
"o" 跟 OS 的命令一樣

結束程式請連續按 ESC 3 次。
*/
void DoCommand(char *Command)
{ int i;

	CmdToArg(Command);
	if(!Argc) return;
	for(i=0;i<CmdNo;i++){
		if(!strcasecmp(Argv[0],CT[i].cmd)) {
			CT[i].fun();
			return;
		}
	}
	printf("Unknown command \"%s\"\r\n",Argv[0]);
}

//------------------------------------------------------------------------------
extern char Prompt[];
void DoTcpCommand(int skt,char *Command)
{ int i;

	CmdToArg(Command);
	if(!Argc) return;
	for(i=0;i<CmdNo;i++){
		if(!strcasecmp(Argv[0],CT[i].cmd)) {
			//TcpCmdSkt=skt;
			CT[i].fun();
			//TcpCmdSkt=-1;

			//Tcp_Flush();

			break;
		}
	}
	//if(i==CmdNo) TcpPrint(skt,0,"Unknown command \"%s\"\r\n",Argv[0]);
	//if(bEchoPrompt) TcpPrint(skt,1,Prompt);
}

//For HMI command
void DoHmiCommand(uint8_t *Command)
{ 
	switch(Command[1])
	{
		case HMI_OP_STOP_EMERGENCY:
			if(Command[0] == HMI_SIZE_CMD_STOP_EMERGENCY)
			{
				//Activate emergency stop function.
				
				//If success, send ACK
				UART_HMI_send_one_data(Command[1],HMI_DATA_ACK);
			}
			break;
		case HMI_OP_OPEN_DOOR:
			if(Command[0] == HMI_SIZE_CMD_OPEN_DOOR)
			{
				//Activate open door function.
				
				//If success, send 2
				UART_HMI_send_one_data(Command[1],2);
			}
			break;
		case HMI_OP_START:
			if(Command[0] == HMI_SIZE_CMD_START)
			{
				uint8_t steps[20] ={1,2,3,4,3,4,3,4,5,6,7,8,0,0,0,0,0,0,0,0};
				
				switch(Command[2])
				{
					case 1:
						//Activate sterlize function.
						
						//If success, send start data
						UART_HMI_start_data(Command[2],12,0,0,0,0,0);
						HAL_Delay(5);
						//send all steps information
						UART_HMI_all_steps(steps);
						break;
					default :
						break;
				}
				
			}
			break;
			
		#ifdef USE_PRESCAN
		case HMI_OP_PRESCAN:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					Prescan_Start();
				}
				else {
					Prescan_Stop();
				}
			}
			break;
		case HMI_OP_DO01:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO01 = 1;
				}
				else {
					DO01 = 0;
				}
			  printf("[DO01=%d]",DO01);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_DO02:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO02 = 1;
				}
				else {
					DO02 = 0;
				}
			  printf("[DO02=%d]",DO02);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_DO03:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO03 = 1;
				}
				else {
					DO03 = 0;
				}
			  printf("[DO03=%d]",DO03);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_DO04:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO04 = 1;
				}
				else {
					DO04 = 0;
				}
			  printf("[DO04=%d]",DO04);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_DO05:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO05 = 1;
				}
				else {
					DO05 = 0;
				}
			  printf("[DO05=%d]",DO05);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_DO06:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO06 = 1;
				}
				else {
					DO06 = 0;
				}
			  printf("[DO06=%d]",DO06);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_DO07:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO07 = 1;
				}
				else {
					DO07 = 0;
				}
			  printf("[DO07=%d]",DO07);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_DO08:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO08 = 1;
				}
				else {
					DO08 = 0;
				}
			  printf("[DO08=%d]",DO08);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_DO09:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO09 = 1;
				}
				else {
					DO09 = 0;
				}
			  printf("[DO09=%d]",DO09);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_DO10:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO10 = 1;
				}
				else {
					DO10 = 0;
				}
			  printf("[DO10=%d]",DO10);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_DO11:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO11 = 1;
				}
				else {
					DO11 = 0;
				}
			  printf("[DO11=%d]",DO11);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_DO12:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO12 = 1;
				}
				else {
					DO12 = 0;
				}
			  printf("[DO12=%d]",DO12);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_DO13:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO13 = 1;
				}
				else {
					DO13 = 0;
				}
			  printf("[DO13=%d]",DO13);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_DO14:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO14 = 1;
				}
				else {
					DO14 = 0;
				}
			  printf("[DO14=%d]",DO14);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_DO15:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO15 = 1;
				}
				else {
					DO15 = 0;
				}
			  printf("[DO15=%d]",DO15);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_DO16:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO16 = 1;
				}
				else {
					DO16 = 0;
				}
			  printf("[DO16=%d]",DO16);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_DO17:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO17 = 1;
				}
				else {
					DO17 = 0;
				}
			  printf("[DO17=%d]",DO17);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_RO01:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					RO01 = 1;
				}
				else {
					RO01 = 0;
				}
			  printf("[RO01=%d]",RO01);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_RO02:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					RO02 = 1;
				}
				else {
					RO02 = 0;
				}
			  printf("[RO02=%d]",RO02);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_RO03:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					RO03 = 1;
				}
				else {
					RO03 = 0;
				}
			  printf("[RO03=%d]",RO03);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_RO04:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					RO04 = 1;
				}
				else {
					RO04 = 0;
				}
			  printf("[RO04=%d]",RO04);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_RO05:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					RO05 = 1;
				}
				else {
					RO05 = 0;
				}
			  printf("[RO05=%d]",RO05);
			  bNewLine = 1;
			}
			break;
		case HMI_OP_ALL_ON:
		case HMI_OP_ALL_OFF:
			if(Command[0] == HMI_SIZE_CMD_PRESCAN)
			{
				if(Command[2]){
					DO01	 = 1;
					DO02	 = 1;
					DO03	 = 1;
					DO04	 = 1;
					DO05	 = 1;
					DO06	 = 1;
					DO07	 = 1;
					DO08	 = 1;
					DO09	 = 1;
					DO10	 = 1;
					DO11	 = 1;
					DO12	 = 1;
					DO13	 = 1;
					DO14	 = 1;
					DO15	 = 1;
					DO16	 = 1;
					DO17	 = 1;
					RO01	 = 1;
					RO02	 = 1;
					RO03	 = 1;
					RO04	 = 1;
					RO05	 = 1;
				}
				else {
					DO01	 = 0;
					DO02	 = 0;
					DO03	 = 0;
					DO04	 = 0;
					DO05	 = 0;
					DO06	 = 0;
					DO07	 = 0;
					DO08	 = 0;
					DO09	 = 0;
					DO10	 = 0;
					DO11	 = 0;
					DO12	 = 0;
					DO13	 = 0;
					DO14	 = 0;
					DO15	 = 0;
					DO16	 = 0;
					DO17	 = 0;
					RO01	 = 0;
					RO02	 = 0;
					RO03	 = 0;
					RO04	 = 0;
					RO05	 = 0;
				}
			  printf("[All =%d]",Command[2]);
			  bNewLine = 1;
			}
			break;
		#endif
		default :
			printf("Unknown command \"%d\"\r\n",Command[1]);
			break;
	}
}

//------------------------------------------------------------------------------
