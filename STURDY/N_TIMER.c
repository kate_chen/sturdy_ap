
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "uart_debug.h"
#include "N_TIMER.h"


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

void SD_LedOn(void)
{
}

void SD_LedOff(void)
{
}

int IsKey(void)
{
	return 0;
}

int ReadKey(void)
{
	return 0;
}

static int PushKey(int key)
{
	return 0;
}

unsigned char IsKeyEvent(void)
{
	return 0;
}

unsigned char ReadKeyEvent(void)
{
	return 0;
}

static int PushKeyEvent(int key)
{
	return 0;
}

void OutWave1(unsigned long freq,unsigned high,unsigned low)
{
}

void StopOutWave1(void)
{
}

void _Sound(unsigned freq)
{
}

void Sound(unsigned freq,unsigned period)
{
}

int ReadKeyStatus(unsigned key)
{
	return 0;
}

void ScanKeyPad(void)
{
}

void PlayCurrentSoundCmd(void)
{
}

int PlaySound(char *SoundCmd)
{
	return 0;
}

static void KeySoundLcdBacklight(void)
{
}

void TimerIsr_0(void)
{
}

void RestoreNewTimer(void)
{
}

void InstallNewTimer(void)
{
}




