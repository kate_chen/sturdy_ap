
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "chkwater.h"
#include "uart_debug.h"


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private functions ---------------------------------------------------------*/

void CheckWater_Stop(void)
{
}

void CheckWater_Start_mode(int mode)
{
}

void CheckWater_Start(void)
{
}

void CheckWater_Loop(void)
{
}

void CheckWater2_Stop(void)
{
}

void CheckWater2_Start(void)
{
}

void CheckWater2_Loop(void)
{
}

void AddWater_Loop(void)
{
}

void AddWaterBox_Stop(void)
{
}

void AddWaterBox_Start(void)
{
}

void AddWaterBox_Loop(void)
{
}




