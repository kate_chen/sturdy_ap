/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "uart_debug.h"
#include "command.h"
#include "control.h"           // 20190830 十方_kate 新增
#include "Prime.h"
#include "uart_hmi.h"
#include "sensor.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
//TIM_HandleTypeDef htim2;            // 20190830 十方_kate 新增
TIM_HandleTypeDef htim14;             // 20190916 十方_kate 新增
__IO uint8_t ufIoControl = RESET;     // 20190916 十方_kate 新增

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
//static void MX_TIM2_Init(void);     // 20190830 十方_kate 新增
static void MX_TIM14_Init(void);      // 20190916 十方_kate 新增
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* External variables --------------------------------------------------------*/
extern UART_HandleTypeDef huart_debug;
extern __IO uint32_t uwBufferReadyIndication;
extern uint8_t *pBufferReadyForUser;
extern __IO uint32_t uwReceiveError;

//uart_hmi.c
extern __IO uint32_t uwBufferReadyIndicationHMI;
extern uint8_t *pBufferReadyForCB;
extern __IO uint32_t uwErrorHMI;
extern __IO uint32_t uwReceiveErrorHMI;
extern __IO uint32_t uwTransmitErrorHMI;

/* USER CODE BEGIN EV */

/* USER CODE END EV */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */


/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  UART_HMI_Init();
  UART_DEBUG_Init();
  MX_TIM14_Init();   //  20190916 十方_kate 新增
  SENSOR_Init();  
  
// << 20190830 十方_kate 新增
//  MX_TIM2_Init();          // 20190916 十方變更
//  if (HAL_TIM_Base_Start_IT(&htim2) != HAL_OK)      
  if (HAL_TIM_Base_Start_IT(&htim14) != HAL_OK)       // 20190916 十方變更
  {
    /* Starting Error */
    Error_Handler();
  }
// >> 20190830 十方_kate 新增

  /* USER CODE BEGIN 2 */
  printf("\r\n[start]\r\n");

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    //HMI UART re-enable RX interrupt
    if(uwReceiveErrorHMI)
    {
      UART_HMI_ReceiveCMD();
    }
    //HMI UART re-transmit data
    if(uwTransmitErrorHMI)
    {
      UART_HMI_TransmitData();
    }
    if(uwBufferReadyIndicationHMI != 0)
    {
				//printf("\nHMI %d/%d:%X/%X,%X\r\n",uwBufferReadyIndicationHMI,pBufferReadyForCB[0],pBufferReadyForCB[1],pBufferReadyForCB[2],pBufferReadyForCB[3]);
				DoHmiCommand(pBufferReadyForCB);

      /* Reset indication */
      	memset(pBufferReadyForCB,'\0',uwBufferReadyIndicationHMI);
				uwBufferReadyIndicationHMI = 0;
      UART_HMI_ReceiveCMD();
    }


// << command輸入處理    
    if(uwReceiveError)
    {
      UART_DEBUG_ReceiveCMD();
    }
// << 20190916 十方新增
    if(ufIoControl)
   	{
			ufIoControl = RESET;
		  IO_Control();
   	}    
    SENSOR_update();
// >> 20190916 十方新增
    /* Checks if Buffer full indication has been set */
    if (uwBufferReadyIndication != 0)
    {
			//Add end symbol
			if(uwBufferReadyIndication == DEBUGBUFFERSIZE)
				pBufferReadyForUser[DEBUGBUFFERSIZE-1] = '\0';
			else
				pBufferReadyForUser[uwBufferReadyIndication] = '\0';

			//printf("RX:%s\r\n",(uint8_t*)pBufferReadyForUser);
			DoCommand((char *)pBufferReadyForUser);

      /* Reset indication */
      	memset(pBufferReadyForUser,'\0',uwBufferReadyIndication);
      uwBufferReadyIndication = 0;
      UART_DEBUG_ReceiveCMD();
    }
// >> command輸入處理  
 //   ParamInput(pBufferReadyForUser);
    /* Manage temporisation between TX buffer sendings */
 //   HAL_Delay(500);      // 20190917 十方變更
   
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
// << 20190830 十方_kate 變更後
void SystemClock_Config(void)
{
  
	#ifdef USE_HSI
  printf("\r\n[USE_HSI = false]\r\n");
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48;       //內部震盪器
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI48;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL2;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
#else
  printf("\r\n[USE_HSI = true]\r\n");
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;           //外部震盪器
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL4;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    printf("\r\n[Error_Handler_1]\r\n");
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    printf("\r\n[Error_Handler_2]\r\n");
    Error_Handler();
  }
  
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    printf("\r\n[Error_Handler_3]\r\n");
    Error_Handler();
  }
  #endif
}
// >> 20190830 十方_kate 變更後
#if 0     // << 20190830 十方_kate 變更前
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI48;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL2;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }

  #if 1
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  #elif 0
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  #else
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  #endif
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

#endif     // >> 20190830 十方_kate 變更前

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, AC_START_Pin|MCU_ACO_01_Pin|MCU_ACO_02_Pin|MCU_ACO_03_Pin 
                          |MCU_ACO_04_Pin|WATER_LEVEL_DE2_Pin|WATER_LEVEL_DE3_Pin|BUZZER_CONTROL_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(WATER_LEVEL_DE1_GPIO_Port, WATER_LEVEL_DE1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(PT100_NSS_GPIO_Port, PT100_NSS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, KTYPE_NSS_Pin|PRESSURE_I2C_OE_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, MCU_DO_01_Pin|MCU_DO_02_Pin|MCU_DO_03_Pin|MCU_DO_04_Pin 
                          |MCU_DO_05_Pin|MCU_DO_06_Pin|MCU_DO_20_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, MCU_DO_07_Pin|MCU_DO_08_Pin|MCU_DO_16_Pin|MCU_DO_17_Pin 
                          |MCU_DO_18_Pin|MCU_DO_19_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin input Level */
  HAL_GPIO_ReadPin(GPIOB,MCU_DI_01_Pin|MCU_DI_02_Pin|MCU_ACI_01_Pin);
  /*Configure GPIO pin input Level */
  HAL_GPIO_ReadPin(GPIOE,MCU_DI_03_Pin|MCU_DI_04_Pin|MCU_DI_06_Pin
                        |MCU_DI_07_Pin|MCU_DI_08_Pin|MCU_ACI_02_Pin);
                        
  /*Configure GPIO pins : AC_START_Pin MCU_ACO_01_Pin MCU_ACO_02_Pin MCU_ACO_03_Pin 
                           MCU_ACO_04_Pin WATER_LEVEL_DE2_Pin WATER_LEVEL_DE3_Pin */
  GPIO_InitStruct.Pin = AC_START_Pin|MCU_ACO_01_Pin|MCU_ACO_02_Pin|MCU_ACO_03_Pin 
                          |MCU_ACO_04_Pin|WATER_LEVEL_DE2_Pin|WATER_LEVEL_DE3_Pin|BUZZER_CONTROL_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : WATER_LEVEL_DE1_Pin PRESSURE_I2C_OE_Pin */
  GPIO_InitStruct.Pin = WATER_LEVEL_DE1_Pin|PRESSURE_I2C_OE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : MCU_DO_01_Pin MCU_DO_02_Pin MCU_DO_03_Pin MCU_DO_04_Pin 
                           MCU_DO_05_Pin MCU_DO_06_Pin MCU_DO_20_Pin */
  GPIO_InitStruct.Pin = MCU_DO_01_Pin|MCU_DO_02_Pin|MCU_DO_03_Pin|MCU_DO_04_Pin 
                          |MCU_DO_05_Pin|MCU_DO_06_Pin|MCU_DO_20_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : MCU_DO_07_Pin MCU_DO_08_Pin MCU_DO_16_Pin MCU_DO_17_Pin 
                           MCU_DO_18_Pin MCU_DO_19_Pin */
  GPIO_InitStruct.Pin = MCU_DO_07_Pin|MCU_DO_08_Pin|MCU_DO_16_Pin|MCU_DO_17_Pin 
                          |MCU_DO_18_Pin|MCU_DO_19_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
  
  GPIO_InitStruct.Pin = MCU_DI_01_Pin|MCU_DI_02_Pin|MCU_ACI_01_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);  

  GPIO_InitStruct.Pin = MCU_DI_03_Pin|MCU_DI_04_Pin|MCU_DI_06_Pin
                       |MCU_DI_07_Pin|MCU_DI_08_Pin|MCU_ACI_02_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct); 

  /*Configure GPIO pin : PT100_NSS_Pin */
  GPIO_InitStruct.Pin = PT100_NSS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(PT100_NSS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : KTYPE_NSS_Pin */
  GPIO_InitStruct.Pin = KTYPE_NSS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(KTYPE_NSS_GPIO_Port, &GPIO_InitStruct);
  
}



#if 0    // << 20190917 十方改用其他方式
// << 20190830 十方_kate 新增
/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 1599;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 999;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}
// >> 20190830 十方_kate 新增
#endif   // >> 20190917 十方改用其他方式


/**
  * @brief TIM14 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM14_Init(void)
{

  /* USER CODE BEGIN TIM14_Init 0 */

  /* USER CODE END TIM14_Init 0 */

  /* USER CODE BEGIN TIM14_Init 1 */

  /* USER CODE END TIM14_Init 1 */
  htim14.Instance = TIM14;
  htim14.Init.Prescaler = 1599;
  htim14.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim14.Init.Period = 999;
  htim14.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim14.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim14) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM14_Init 2 */

  /* USER CODE END TIM14_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel2_3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);

  /* DMA1_Channel4_5_6_7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel4_5_6_7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel4_5_6_7_IRQn);
}


/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  Tx Transfer completed callback
  * @param  UartHandle: UART handle. 
  * @note   This example shows a simple way to report end of DMA Tx transfer, and 
  *         you can add your own implementation. 
  * @retval None
  */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *UartHandle)
{
  UART_HMI_TxCpltCallback(UartHandle);
}

/**
  * @brief  Rx Transfer completed callback
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report end of DMA Rx transfer, and 
  *         you can add your own implementation.
  * @retval None
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
	UART_HMI_RxCpltCallback(UartHandle);
	UART_DEBUG_RxCpltCallback(UartHandle);
}

/**
  * @brief  UART error callbacks
  * @param  UartHandle: UART handle
  * @note   This example shows a simple way to report transfer error, and you can
  *         add your own implementation.
  * @retval None
  */
void HAL_UART_ErrorCallback(UART_HandleTypeDef *UartHandle)
{
	UART_HMI_ErrorCallback(UartHandle);
	UART_DEBUG_ErrorCallback(UartHandle);
}

// << 20190830 十方_kate 新增
/**
  * @brief  Period elapsed callback in non blocking mode
  * @param  htim : TIM handle
  * @retval None
  */

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
//  IO_Control();     //  20190917 十方改用其他方式
	if(htim->Instance == TIM14)
	{
		ufIoControl = SET;
	}
	else 
	{
		SENSOR_PeriodElapsedCallback(htim);
	}
}
// >> 20190830 十方_kate 新增

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
//    printf("\r\n[Error_Handler]\r\n");


  while (1){
//    printf("\r\n[Error_Handler]\r\n");
  
  }



  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
